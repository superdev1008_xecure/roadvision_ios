//
//  AppDelegate.m
//  MV084
//
//  Created by macvision on 16/8/29.
//  Copyright © 2016年 MV084. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "VideoViewController.h"
#import "VLCMovieViewController.h"
#import "SDFileViewController.h"
#import "SettingsViewController.h"
#import "AITCameraCommand.h"
#import "AITUtil.h"
#import "Toast+UIView.h"
#import "XMLReader.h"
#import "AppXMlTemplate.h"

typedef enum
{
    CAMERA_CMD_GET_SettingXML,
    CAMERA_CMD_QUERY_RECORDING,
    CAMERA_CMD_SDQUERY_RECORDING,
    CAMERA_START_VIDEO
    
} Camera_cmd_t;

@interface AppDelegate (){
    
    UINavigationController *navigationController ;
    NSTimer *recordTimer;
    NSTimer *JudeWifi;
    BOOL is_Down;
    BOOL JudeQuery;
    BOOL APPSuspend;
    BOOL JudeRecording;
    Camera_cmd_t camera_cmd;
    int TimerNum;
    NSString *AppVersion1;
    NSString *appFullVer;
    
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    if (notification)
    {
        
        application.applicationIconBadgeNumber = 0;
    }
    
    
    AppVersion1 = [[NSUserDefaults standardUserDefaults]stringForKey:@"AppVersion"];
    
    NSString *appVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *appBuildVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    appFullVer = [[appVer stringByAppendingString:@"."] stringByAppendingString:appBuildVer];
    //对比保存版本号和当前版本号
    if (AppVersion1 == nil ||  ![appFullVer isEqualToString:AppVersion1]){
        
        //保存最新
        [[NSUserDefaults standardUserDefaults]setObject:appFullVer forKey:@"AppVersion"];
        //清除wifi
        NSString * WifiIsYN = [NSString stringWithFormat:@"Wifi=NO"];
        [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
        
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"HardwareVersion"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"VideoPlayUrl"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SettingsViewListArray"];
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"RearCameraLens"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
//        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(GetCameraXML) userInfo:nil repeats:NO];
        
    }
    
    navigationController = [[UINavigationController alloc] initWithRootViewController:[[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil]] ;
    
    [self.window setRootViewController:navigationController] ;
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navigation.png"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    return YES;
}

//程序暂停
- (void)applicationWillResignActive:(UIApplication *)application {
    
    APPSuspend = YES;
    
    UIViewController* visibleViewController1 = [(UINavigationController*)self.window.rootViewController visibleViewController];
    
    NSString *JudeWifi1 = [[NSUserDefaults standardUserDefaults]stringForKey:@"JudeWifi"];
    if ([JudeWifi1 isEqualToString:@"Wifi=YES"]){
        
        if ([visibleViewController1 isKindOfClass:[SDFileViewController class]]){
            
            SDFileViewController* SDViewController = (SDFileViewController *)visibleViewController1;
            if ( SDViewController->dlCount == 0) {
                
                if (SDViewController->CountNum <= 10) {
                    
                    
                    SDViewController->CountNum = 1;
                }
                
            }
        }else if([visibleViewController1 isKindOfClass:[SettingsViewController class]]){
            
            SettingsViewController *settingview = (SettingsViewController *)visibleViewController1;
            if (settingview->SettingsCountNum <= 10) {
                
                
                settingview->SettingsCountNum = 1;
                
            }
            
        }else if ([visibleViewController1 isKindOfClass:[VideoViewController class]]){
            
            [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(QueryCameraStatus) userInfo:nil repeats:NO];
            
        }else{
            
            [self QueryCameraStatus];
            
            
        }
    }
}
//进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    
    if (JudeWifi) {
        
        [JudeWifi invalidate];
        JudeWifi = nil;
    }
    
    [self checkViews:application.windows] ;
    
    UIViewController* visibleViewController1 = [(UINavigationController*)self.window.rootViewController visibleViewController];
    
    if ([visibleViewController1 isKindOfClass:[SDFileViewController class]])
    {
        
        SDFileViewController* cameraBrowserViewController = (SDFileViewController *)visibleViewController1;
        is_Down = YES;
        if(!cameraBrowserViewController->dlCount)
        {
            
            is_Down = NO;
            [navigationController popToRootViewControllerAnimated:NO] ;
            NSString * WifiIsYN = [NSString stringWithFormat:@"Wifi=NO"];
            [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
        
    }else{
        
        is_Down = NO;
        [navigationController popToRootViewControllerAnimated:NO] ;
        NSString * WifiIsYN = [NSString stringWithFormat:@"Wifi=NO"];
        [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
    
    
    self.Encryption = NO;
    self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        
        //清除wifi
        NSString * WifiIsYN = [NSString stringWithFormat:@"Wifi=NO"];
        [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    }];
}
//程序进入前台
- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    APPSuspend = NO;
    
    if(recordTimer)
    {
        [recordTimer invalidate];
        recordTimer = nil;
    }
    
}
//程序被激活
- (void)applicationDidBecomeActive:(UIApplication *)application {

    
    if ([[NSUserDefaults standardUserDefaults]stringForKey:@"VideoPlayUrl"].length < 10) {
        
        [self GetCameraXML];
    }
    self.Encryption = NO;
    APPSuspend = NO;
    if (!is_Down) {
        
        TimerNum = 0;
        [self AppDelegateJudeWifi];
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    APPSuspend = YES;
}

- (UIViewController *)appRootViewController{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

#pragma mark - GetDate

- (void)startRecord:(NSTimer *)timer{
    
    if (APPSuspend) {
        
        [navigationController popToRootViewControllerAnimated:NO];
        
        (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStartRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
        
        
    }
}

-(void) requestFinished:(NSString*) result{
    
    switch (camera_cmd) {
            
        case CAMERA_CMD_GET_SettingXML:
        {
            if (result.length > 100) {
                
                NSError *parseError = nil;
                NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:result error:&parseError];
                NSDictionary *camera = [xmlDictionary objectForKey:@"camera"];
                
                if (xmlDictionary == nil && camera == nil) {
                    
                    self.Encryption = NO;
                    self.RearCameraLens = NO;
                    return ;
                }
                
                
                //UI
                NSString *cameraUI = [[camera objectForKey:@"isdualcamera"]objectForKey:@"id"];
                
                if ([cameraUI integerValue] == 0) {
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"RearCameraLens"];
                    self.RearCameraLens = NO;
                    
                }else{
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"RearCameraLens"];
                    self.RearCameraLens = YES;
                    
                }
                
                NSArray *tem = [[NSUserDefaults standardUserDefaults]arrayForKey:@"SettingsViewListArray"];
                if (tem == nil || tem.count < 1) {
                    //这些必须要显示设置界面上
                    NSDictionary *DictSet = [camera objectForKey:@"setting"];
                    NSDictionary *menu = [DictSet objectForKey:@"menu"];
                    
                    NSDictionary *streamingaddr = [camera objectForKey:@"streamingaddr"];
                    NSString *straddr = [streamingaddr objectForKey:@"id"];                    
                    [[NSUserDefaults standardUserDefaults]setObject:straddr forKey:@"VideoPlayUrl"];
                    
                    
                    NSMutableArray *SettingViewArray = [NSMutableArray array];
                    
                    for (NSDictionary *temDitc in menu) {
                        
                        AppXMlTemplate *AppXML = [[AppXMlTemplate alloc]init];
                        NSString *title = [temDitc objectForKey:@"title"];
                        NSString *get = [temDitc objectForKey:@"get"];
                        NSString *set = [temDitc objectForKey:@"set"];
                        NSString *Switch = [temDitc objectForKey:@"switch"];
                        
                        NSDictionary *itemditc = [temDitc objectForKey:@"item"];
                        AppXML.XMlTitle = title;
                        AppXML.XMlGet = get;
                        AppXML.XMlSet = set;
                        AppXML.XMlSwitch = Switch;
                        
                        AppXML.XMlText = [NSMutableDictionary dictionary];;
                        AppXML.XMlID  = [NSMutableDictionary dictionary];
                        AppXML.XMlArray = [NSMutableArray array];
                        
                        
                        for (NSDictionary *item in itemditc) {
                            
                            NSString *strid = [item objectForKey:@"id"];
                            NSString *strtext = [item objectForKey:@"text"];
                            
                            
                            strtext =  [strtext stringByReplacingOccurrencesOfString:@"\t" withString:@""];
                            strtext =  [strtext stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                            [AppXML.XMlID   setObject:strid forKey:strtext];
                            [AppXML.XMlText setObject:strtext forKey:strid];
                            [AppXML.XMlArray addObject:strid];
                            
                            
                        }
                        
                        NSData *HistorData  = [NSKeyedArchiver archivedDataWithRootObject:AppXML];
                        [SettingViewArray addObject:HistorData];
                        
                    }
                    
                    [[NSUserDefaults standardUserDefaults]setObject:SettingViewArray forKey:@"SettingsViewListArray"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                
            }
            break;
        }
        case CAMERA_CMD_QUERY_RECORDING:
        {
            if(result){
                /* Check recording status */
                NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
                
                if(range.location != NSNotFound)
                {
                    NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    if([camearRecordStatus rangeOfString:@"Recording"].location == NSNotFound)
                    {
                        
                        
                        recordTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(startRecord:) userInfo:nil repeats:NO];
                    }
                }
            }
            break;
        }
        case CAMERA_CMD_SDQUERY_RECORDING:
        {
            NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
            if(range.location != NSNotFound){
                NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                if([camearRecordStatus rangeOfString:@"Recording"].location == NSNotFound)
                {
                    
                    camera_cmd = CAMERA_START_VIDEO;
                    (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStartRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
                }
            }
            break;
        }
        case CAMERA_START_VIDEO:
        {
            if([result rangeOfString:@"OK"].location != NSNotFound) {
                
                UIViewController* visibleViewController1 = [self appRootViewController];
                NSValue *centerValue = [NSValue valueWithCGPoint:CGPointMake(visibleViewController1.view.bounds.size.width/2, visibleViewController1.view.bounds.size.height - 55)];
                if (zhSP) {
                    
                    [visibleViewController1.view makeToast:NSLocalizedString(@"Video has been opened.", nil) duration:1.5 position:centerValue];
                }else{
                    
                    [visibleViewController1.view makeToast:NSLocalizedStringFromTable(@"Video has been opened.", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                    
                }
                
            }
            break;
        }
        default:
            break;
    }
    
}

- (void)checkViews:(NSArray *)subviews{
    
    Class AVClass = [UIAlertView class];
    Class ASClass = [UIActionSheet class];
    for (UIView * subview in subviews){
        
        if ([subview isKindOfClass:AVClass]){
            
            [(UIAlertView *)subview dismissWithClickedButtonIndex:[(UIAlertView *)subview cancelButtonIndex] animated:NO];
            
        } else if ([subview isKindOfClass:ASClass]){
            
            [(UIActionSheet *)subview dismissWithClickedButtonIndex:[(UIActionSheet *)subview cancelButtonIndex] animated:NO];
        }else{
            [self checkViews:subview.subviews];
        }
    }
}


#pragma mark - 根据返回值控制所有页面不允许横屏

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if (self.allowRotation) {
        return UIInterfaceOrientationMaskAll;
    }
    return UIInterfaceOrientationMaskPortrait;
}

-(void)AppDelegateJudeWifi{
    
    NSString *WifiIsYN;
    BOOL boolWiFiEnabled = [AITUtil isWiFiEnabled];
    BOOL boolConnectedFirmware ;
    
    int valu = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (valu >= 10) {
        
        boolWiFiEnabled = 1;
    }
    
    if (boolWiFiEnabled)
    {
        NSURL* url = [AITCameraCommand commandQueryCameraStatusUrl];
        NSURLRequest * request = [NSURLRequest requestWithURL:url] ;
        NSURLResponse* urlResponse = nil;
        NSError* error = nil;
        NSData* resultData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if (error)
        {
            
            boolConnectedFirmware = NO;
        }
        NSString *result = [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding] ;
        
        if (result)
        {
            
            NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
            if(range.location != NSNotFound)
            {
                boolConnectedFirmware = YES;
            }else{
                
                boolConnectedFirmware = NO;
            }
        }else{
            boolConnectedFirmware = NO;
        }
    }else{
        boolConnectedFirmware = NO;
    }
    
    
    UIViewController* visibleViewController1 = [self appRootViewController];
    
    if (boolConnectedFirmware) {
        
        if (JudeWifi) {
            [JudeWifi invalidate];
            JudeWifi = nil;
        }
        
        self.Encryption = YES;
        
        NSString *rcl = [[NSUserDefaults standardUserDefaults]stringForKey:@"RearCameraLens"];
        if (rcl != nil) {
            if ([rcl integerValue] == 0) {
                self.RearCameraLens = NO;
            }else{
                self.RearCameraLens = YES;
            }
        }
        
        WifiIsYN = [NSString stringWithFormat:@"Wifi=YES"];
        [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if (zhSP) {
            
            [visibleViewController1.view makeToast:NSLocalizedString(@"Connected to CarDV", nil) duration:1.0 position:nil];
            
        }else{
            
            [visibleViewController1.view makeToast:NSLocalizedStringFromTable(@"Connected to CarDV", @"DefLocalizable", nil) duration:1.0 position:nil];
            
        }
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MainViewControllerCloseAerltView" object:nil];
        
        
    }else{
        
        self.Encryption = NO;
        
        if (!JudeWifi) {
            
            JudeWifi = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(AppDelegateJudeWifi) userInfo:nil repeats:YES];
        }
        TimerNum++;
        WifiIsYN = [NSString stringWithFormat:@"Wifi=NO"];
        [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        if (zhSP) {
            
            [visibleViewController1.view makeToast:NSLocalizedString(@"Connect your mobile phone to CarDV by Wi-Fi",nil) duration:1.0 position:nil];
        }else{
            
            [visibleViewController1.view makeToast:NSLocalizedStringFromTable(@"Connect your mobile phone to CarDV by Wi-Fi", @"DefLocalizable", nil) duration:1.0 position:nil];
            
        }
        
        
    }
    
    if (TimerNum == 6) {
        if (JudeWifi) {
            
            [JudeWifi invalidate];
            JudeWifi = nil;
        }
        
    }
    
    
}

-(void)GetCameraXML{
    
    camera_cmd = CAMERA_CMD_GET_SettingXML;
    (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandGetCamMenu] Delegate:(id<AITCameraRequestDelegate>)self] ;
    
}

-(void)QueryCameraStatus{
    
    camera_cmd = CAMERA_CMD_QUERY_RECORDING;
    (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandQueryCameraStatusUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
}

@end
