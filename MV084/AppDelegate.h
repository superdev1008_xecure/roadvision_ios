//
//  AppDelegate.h
//  MV084
//
//  Created by macvision on 16/8/29.
//  Copyright © 2016年 MV084. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic) UIBackgroundTaskIdentifier backgroundTask;

/***  是否允许横屏的标记 */
@property (nonatomic,assign)BOOL allowRotation;
//标记加密是否正确
@property (nonatomic,assign)BOOL Encryption;
@property (nonatomic,assign)BOOL RearCameraLens;


@end

