//
//  SettingWIFIViewController.h
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/2/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingWIFIViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property (weak, nonatomic) IBOutlet UIButton *BTNUpDate;

- (IBAction)UpWIFIDate:(id)sender;

@end
