//
//  SettingWIFIViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/2/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "SettingWIFIViewController.h"
#import "AITCameraCommand.h"
#import "Toast+UIView.h"


typedef enum
{
    CAMERA_CMD_QUERY_SETTINGS,
    CAMERA_CMD_SET_SETTINGS,
    CAMERA_CMD_INVALID
} Camera_cmd_t;

@interface SettingWIFIViewController (){
    
    UITextField *WIFISSID;
    UITextField *Phrase;
    NSString *WIFIName;
    NSString *PhraseName;
    BOOL UpDateIsSuccess;
    
    //动画
    UIView *parentView;
    UIView *AnimationsView;
    Camera_cmd_t camera_cmd;
    
}

@end



@implementation SettingWIFIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 30)];
    if (zhSP) {
         titleLabel.text = NSLocalizedString(@"Network Settings", nil);
    }else{
         titleLabel.text = NSLocalizedStringFromTable(@"Network Settings", @"DefLocalizable", nil);
        
    }
   
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    self.navigationItem.titleView = titleLabel;

    self.tableview.layer.cornerRadius = 5;
    self.tableview.layer.masksToBounds = YES;
    self.tableview.scrollEnabled = NO;
    
    self.tableview.layer.borderWidth = 1;
    self.tableview.layer.borderColor = [[UIColor clearColor] CGColor];//设置列表边框
    self.tableview.separatorColor = [UIColor clearColor];//设置行间隔边框
    [self.tableview setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewDidAppear:animated] ;
    

    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    WIFIName = [[NSString alloc]init];
    PhraseName = [[NSString alloc]init];
    WIFISSID = [[UITextField alloc]init];
    Phrase = [[UITextField alloc]init];
    
    WIFISSID.enabled = NO;
    Phrase.enabled = NO;
    UpDateIsSuccess = NO;
    
    if (zhSP) {
        
        WIFISSID.placeholder = NSLocalizedString(@"Set the Wi-Fi SSID", nil);
        Phrase.placeholder = NSLocalizedString(@"Please set a password", nil) ;
        [self.BTNUpDate setTitle: NSLocalizedString(@"Reset", nil) forState:UIControlStateNormal];
    }else{
        
        WIFISSID.placeholder = NSLocalizedStringFromTable(@"Set the Wi-Fi SSID", @"DefLocalizable", nil);
        Phrase.placeholder = NSLocalizedStringFromTable(@"Please set a password", @"DefLocalizable", nil);
        [self.BTNUpDate setTitle: NSLocalizedStringFromTable(@"Reset", @"DefLocalizable", nil) forState:UIControlStateNormal];
        
    }
    
    
    
    
    [self RoutingDirective:2];

}

#pragma mark - 命令

-(void)RoutingDirective:(NSInteger)Num{
    
    switch (Num) {
        case 0://重启wifi
        {
            UpDateIsSuccess = NO;
            camera_cmd = CAMERA_CMD_SET_SETTINGS;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandReactivateUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 1://更新接口
        {
            UpDateIsSuccess = YES;
            camera_cmd = CAMERA_CMD_SET_SETTINGS;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandUpdateUrl:WIFIName EncryptionKey:PhraseName] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 2:
        {
            camera_cmd = CAMERA_CMD_QUERY_SETTINGS;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandWifiInfoUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        default:
            break;
    }
    
}

-(void) requestFinished:(NSString*) result{
    
    if (result != nil){
        
        NSRange range;
        
        if(camera_cmd == CAMERA_CMD_QUERY_SETTINGS){
            
            NSArray *lines = [result componentsSeparatedByString:@"\n"];
            for (NSString *line in lines)
            {
                
                if ([line hasPrefix:[AITCameraCommand PROPERTY_SSID]]){
                    
                    NSArray *properties = [line componentsSeparatedByString:@"="] ;
                    
                    if ([properties count] == 2){

                        WIFIName = [NSString stringWithFormat:@"%@",[properties objectAtIndex:1]];
                    }
                    
                }else if ([line hasPrefix:[AITCameraCommand PROPERTY_ENCRYPTION_KEY]]){
                    
                    NSArray *properties = [line componentsSeparatedByString:@"="] ;
                    
                    if ([properties count] == 2)
                    {
                        //                        self.encryptionKeyText.text = [properties objectAtIndex:1] ;
                        
                        PhraseName = [NSString stringWithFormat:@"%@",[properties objectAtIndex:1]];
                    }
                }
                
                [self.tableview reloadData];
            }
        }else if(camera_cmd == CAMERA_CMD_SET_SETTINGS){
            
            range= [result rangeOfString:@"OK"];
            if(range.location != NSNotFound){
                
                if (UpDateIsSuccess) {
                    
                    [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(RestartEquipment) userInfo:nil repeats:NO];
                    
                    
                }else{
                    //Update success.
                    NSString *message = NSLocalizedString(@"Update success.", nil);
                    if (!zhSP) {
                        
                        message = NSLocalizedStringFromTable(@"Update success.", @"DefLocalizable", nil);
                        
                    }
                    [self CustomAnimationsSuccessImage];
                    [self ShowAlaertView:message Tag:1721];
                }
            }
        }
    }else{
        
        if (zhSP) {
            [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:nil];
        }else{
            
            [self.view makeToast: NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:nil];
            
        }
        
    }
    
    WIFISSID.enabled = YES;
    Phrase.enabled = YES;

}

-(void)RestartEquipment{
    
    [self RoutingDirective:0];
}

#pragma mark - IBAction

- (IBAction)UpWIFIDate:(id)sender {
    
    
    if ([self JudeUITextFieldIsNULL]) {
        
        [WIFISSID resignFirstResponder];
        [Phrase resignFirstResponder];
        [self LoaingCustomAnimationsView];
        [self RoutingDirective:1];
    }
}

#pragma mark - JudeUITextField

-(BOOL)JudeUITextFieldIsNULL{
    
    NSCharacterSet *printableAsciiCharSet = [NSCharacterSet characterSetWithCharactersInString:@"01234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~`!@#$%^&*()-_=+[{]}\\|;:'\",<.>/? "] ;
    
    WIFIName = WIFISSID.text;
    NSString *ssid = WIFISSID.text;
    
    NSData *data = [ssid dataUsingEncoding:NSUTF8StringEncoding] ;
    
    if ([data length] ==0){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Wrong SSID", nil) message:NSLocalizedString(@"SSID is required", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] ;
        if (!zhSP) {
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Wrong SSID", @"DefLocalizable", nil) message:NSLocalizedStringFromTable(@"SSID is required", @"DefLocalizable", nil) delegate:nil cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"DefLocalizable", nil) otherButtonTitles:nil] ;
            
        }
        [alert show] ;
        
        return NO;
        
    } else if ([data length] > 32){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Wrong SSID", nil) message:NSLocalizedString(@"SSID is too long", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] ;
        if (!zhSP) {
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Wrong SSID", @"DefLocalizable", nil) message:NSLocalizedStringFromTable(@"SSID is too long", @"DefLocalizable", nil) delegate:nil cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"DefLocalizable", nil) otherButtonTitles:nil] ;
            
        }
        
        [alert show] ;
        
        return NO;
    }
    
    PhraseName = Phrase.text;
    NSString *encryptionKey = Phrase.text;
    
    NSCharacterSet *invalidSet = [printableAsciiCharSet invertedSet] ;
    
    if ([encryptionKey rangeOfCharacterFromSet:invalidSet].location != NSNotFound) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Encryption Key is invalid", nil) message:NSLocalizedString(@"Please use ASCII characters", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] ;
        if (!zhSP) {
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Encryption Key is invalid", @"DefLocalizable", nil) message:NSLocalizedStringFromTable(@"Please use ASCII characters", @"DefLocalizable", nil) delegate:nil cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"DefLocalizable", nil) otherButtonTitles:nil] ;
            
        }
        
        [alert show] ;
        
        return NO;
    }
    
    if ([encryptionKey length] < 8){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Encryption Key is invalid", nil) message:NSLocalizedString(@"Encryption key is too short", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] ;
        if (!zhSP) {
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Encryption Key is invalid", @"DefLocalizable", nil) message:NSLocalizedStringFromTable(@"Encryption key is too short", @"DefLocalizable", nil) delegate:nil cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"DefLocalizable", nil) otherButtonTitles:nil] ;
        }
        
        [alert show] ;
        
        return NO;
    } else if ([encryptionKey length] > 63){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Encryption Key is invalid", nil) message:NSLocalizedString(@"Encryption key is too long", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] ;
        if (!zhSP) {
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Encryption Key is invalid", @"DefLocalizable", nil) message:NSLocalizedStringFromTable(@"Encryption key is too long", @"DefLocalizable", nil) delegate:nil cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"DefLocalizable", nil) otherButtonTitles:nil] ;
            
        }
        
        [alert show] ;
        
        return NO;
    }
    
    return YES;
    
}

#pragma mark - 动画

-(void)LoaingCustomAnimationsView{
    

    if (AnimationsView == nil) {
        AnimationsView = [[UIView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width /2 - 150 ,[[UIScreen mainScreen] bounds].size.height /2 - 200, 300, 300)];
    }
    
    AnimationsView.tag = 10013;
    
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 50, AnimationsView.frame.size.height/ 2 - 50, 100, 100)];
    UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake( AnimationsView.frame.size.width/ 2 - 75 , imageview.frame.size.height + imageview.frame.origin.y + 20, 150, 50)];
    message.backgroundColor = [UIColor clearColor];
    message.tag = 10012;
    message.font = [UIFont systemFontOfSize:20];
    message.textColor = [UIColor whiteColor];
    message.textAlignment = NSTextAlignmentCenter;
    
    if (zhSP) {
        
        message.text = NSLocalizedString(@"Loading...", nil);

    }else{
        
        message.text = NSLocalizedStringFromTable(@"Loading...", @"DefLocalizable", nil);
        
    }
    
    
    imageview.image = [UIImage imageNamed:@"Imageloading.png"];
    imageview = [self rotate360DegreeWithImageView:imageview];
    imageview.tag = 10011;
    
    [AnimationsView addSubview:imageview];
    [AnimationsView addSubview:message];
    AnimationsView.backgroundColor = [UIColor clearColor];
    AnimationsView.userInteractionEnabled = NO;
    
    //让页面暗下动画
    if (parentView == nil) {
        
        parentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height)];
    }
    
    parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    parentView.tag = 10010;
    [self.view addSubview:parentView];
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                     }
                     completion:NULL
     ];
    
    [self.view addSubview:AnimationsView];
    
}

-(void)CustomAnimationsSuccessImage{
    
    if (AnimationsView) {
        UIImageView *SuccessImage = [[UIImageView alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 50, AnimationsView.frame.size.height/ 2 - 50, 100, 100)];
        SuccessImage.image = [UIImage imageNamed:@"ImageloadingSuccess.png"];
        CGRect imageview;
        for (UIView *obj in [AnimationsView subviews]) {
            
            if (obj.tag == 10011 || obj.tag == 10012) {
                
                if (obj.tag == 10011) {
                    UIImageView *a = (UIImageView *)obj;
                    [a.layer removeAllAnimations];
                    imageview = a.frame;
                }
                [obj removeFromSuperview];
            }
            
        }
        UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 75 , imageview.size.height + imageview.origin.y + 20, 150, 50)];
        message.backgroundColor = [UIColor clearColor];
        message.font = [UIFont systemFontOfSize:20];
        message.textColor = [UIColor whiteColor];
        message.textAlignment = NSTextAlignmentCenter;
        message.text = NSLocalizedString(@"Consummation", nil);
        message.tag = 10012;
        SuccessImage.tag = 10011;
        
        [AnimationsView addSubview:message];
        [AnimationsView addSubview:SuccessImage];
        
        [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(StopCustomAnimationsView) userInfo:nil repeats:NO];
    }
    
}

-(void)StopCustomAnimationsView{
    
    for (UIView *del in [AnimationsView subviews]) {
        
        if (del.tag == 10011 || del.tag == 10012) {
           
            [del removeFromSuperview];
        }
        
    }
    
    for (UIView *obj in [self.view subviews]) {
        
        if (obj.tag == 10010 || obj.tag == 10013) {
            
            [obj removeFromSuperview];
        }
    }
    
    AnimationsView = nil;
}

- (UIImageView *)rotate360DegreeWithImageView:(UIImageView *)imageView{
    
    CABasicAnimation *animation = [ CABasicAnimation
                                   animationWithKeyPath: @"transform" ];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    
    //围绕Z轴旋转，垂直与屏幕
    animation.toValue = [NSValue valueWithCATransform3D:
                         
                         CATransform3DMakeRotation(M_PI, 0.0, 0.0, 1.0) ];
    
    animation.duration = 1.0;
    //旋转效果累计，先转180度，接着再旋转180度，从而实现360旋转
    animation.cumulative = YES;
    animation.repeatCount = 100;
    
    //在图片边缘添加一个像素的透明区域，去图片锯齿
    CGRect imageRrect = CGRectMake(0, 0,imageView.frame.size.width, imageView.frame.size.height);
    UIGraphicsBeginImageContext(imageRrect.size);
    [imageView.image drawInRect:CGRectMake(1,1,imageView.frame.size.width-2,imageView.frame.size.height-2)];
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [imageView.layer addAnimation:animation forKey:nil];
    return imageView;
}


#pragma  mark - 收键盘

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
//    [self SettingNumber];
    [WIFISSID resignFirstResponder];
    [Phrase resignFirstResponder];
    return YES;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    [self SettingNumber];
    [WIFISSID resignFirstResponder];
    [Phrase resignFirstResponder];
    
}

#pragma mark - Table view delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"SettingWIFIView";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(35, 10, 100, 30)];
    title.tintColor = [UIColor blackColor];
    title.font = [UIFont systemFontOfSize:17];
    
    UIImageView *backImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.tableview.frame.size.width, cell.frame.size.height)];
    backImage.image = [UIImage imageNamed:@"WIFIView_TextBackImage.png"];
    
    
    if (indexPath.row == 0) {
        
        title.text = @"Wi-Fi SSID:";
        
        WIFISSID.frame = CGRectMake(title.frame.origin.x + title.frame.size.width + 10, 10, self.tableview.frame.size.width - (title.frame.origin.x + title.frame.size.width + 20), 30);
        WIFISSID.delegate = self;
        
        if (WIFIName.length > 0) {
            WIFISSID.text = WIFIName;
        }
        
        title.textAlignment = NSTextAlignmentRight;
        
        [cell addSubview:backImage];
        [cell addSubview:WIFISSID];
        [cell addSubview:title];
        
    }else if (indexPath.row == 2){
        
        Phrase.frame = CGRectMake(title.frame.origin.x + title.frame.size.width + 10, 10, self.tableview.frame.size.width - (title.frame.origin.x + title.frame.size.width + 20), 30);
        
        
        if (PhraseName.length > 0) {
            Phrase.text = PhraseName;
        }
        
        if (zhSP) {
            title.text = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"Passphrase", nil)];
        }else{
            
            title.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTable(@"Passphrase", @"DefLocalizable", nil)];
            
        }
        
        title.textAlignment = NSTextAlignmentRight;
        
        [cell addSubview:backImage];
        [cell addSubview:Phrase];
        [cell addSubview:title];

    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

//设置cell的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 1) {
        
        return 10;
        
    }
    return 50;

}

#pragma mark - alertView

-(void)ShowAlaertView:(NSString *)Message Tag:(NSInteger)tag{
    
    UIAlertView *showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
    
    if (!zhSP) {
        
        showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
        
    }
    if (tag != 0) {
        showAlert.tag = tag;
    }
    
    [showAlert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 1721) {
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(BackToMainView) userInfo:nil repeats:NO];
        
    }
}

-(void)BackToMainView{
    
    NSString * WifiIsYN = [NSString stringWithFormat:@"Wifi=NO"];
    [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
