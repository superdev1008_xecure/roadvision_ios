//
//  SettingsViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "SettingsViewController.h"
#import "Toast+UIView.h"
#import "AITCameraCommand.h"
#import "SettingWIFIViewController.h"
#import "XMLReader.h"
#import "AppXMlTemplate.h"


typedef enum
{
    CAMERA_CMD_QUERY_SETTINGINFORMATION,
    CAMERA_CMD_GET_SettingXML,
    CAMERA_CMD_SET_FORMATSDCARD,
    CAMERA_CMD_SET_RESTOREFACTORY,
    CAMERA_CMD_SET_SettingXML,
    CAMERA_CMD_INVALID
    
} Camera_cmd_t;


@interface SettingsViewController (){
    
    NSMutableArray *_DataArray;
    NSString *FirmwareVersion;
    //动画
    UIView *parentView;
    UIView *AnimationsView;
    
    int VersionCount;
    Camera_cmd_t camera_cmd;
    BOOL JudeLoading;
    int GoToWIFIView;
    UIBarButtonItem *btnBack;

}

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 30)];
    if (zhSP) {
        titleLabel.text= NSLocalizedString(@"Settings", nil);
    }else{
        titleLabel.text= NSLocalizedStringFromTable(@"Settings", @"DefLocalizable", nil);
        
    }
    
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    self.navigationItem.titleView = titleLabel;
    VersionCount = 0;
    [self initDataSource];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    UIImage* imageBack = [UIImage imageNamed:@"BackImage.png"];
    CGRect frameimgBack = CGRectMake(100, 100, 15, 30);
    

        
    UIButton* backtBtn  = [[UIButton alloc] initWithFrame:frameimgBack];
    
    
    [backtBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [backtBtn addTarget:self action:@selector(SettingViewGoToMainView)
       forControlEvents:UIControlEventTouchUpInside];
    
    if (btnBack == nil) {
        btnBack = [[UIBarButtonItem alloc]initWithCustomView:backtBtn];
    }
    
    self.navigationItem.leftBarButtonItem = btnBack;
    
    if (GoToWIFIView != 1) {
        

        FirmwareVersion = [[NSString alloc]init];
        JudeLoading = NO;
        
        GoToWIFIView = 0;
        [self LoaingCustomAnimationsView];
        if ([[NSUserDefaults standardUserDefaults]arrayForKey:@"SettingsViewListArray"].count > 0) {
            [self CommandsSend:2];
        }else{
            [self CommandsSend:3];
        }
        
    }
    
    
}

-(void)SettingViewGoToMainView{
    

    [self.navigationController popViewControllerAnimated:YES];
}

-(void)GetSettingDate{
    
    [self CommandsSend:2];
}

#pragma mark - UIAlertView

-(void)ShowAlertView:(NSString *)info message:(NSString *)message tag:(NSInteger)tag{
    
    UIAlertView *showalert = [[UIAlertView alloc]init];
    
    if (info.length > 0) {
        
        
        if (zhSP) {
            
            showalert = [[UIAlertView alloc]initWithTitle:info message:message delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
        }else{
            
            showalert = [[UIAlertView alloc]initWithTitle:info message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil) otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
            
        }
       
    }else{
        
        if (zhSP) {
            
            showalert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
        }else{
            
            showalert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
            
        }
        
        
    }
    
    if (tag > 0) {
        showalert.tag = tag;
    }
    [showalert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    switch (buttonIndex) {
        case 0:
        {
            if (alertView.tag == 221) {
                
                [self CommandsSend:2];
            }
            break;
        }
        case 1:
        {
            if (alertView.tag == 201) {
                //格式化
                [self CommandsSend:0];
            }else if (alertView.tag == 80){
                [self CommandsSend:1];
            }else if (alertView.tag == 221){
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            break;
        }
            
        default:
            break;
    }
    
}

#pragma mark - Command

-(void)CommandsSend:(NSInteger)num{
    
    if (!JudeLoading) {
        [self LoaingCustomAnimationsView];
    }
    switch (num) {
 
        case 0://FormatSDCard
        {
            camera_cmd = CAMERA_CMD_SET_FORMATSDCARD;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraFormatSDCardUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 1:
        {
            camera_cmd = CAMERA_CMD_SET_RESTOREFACTORY;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraResetSettingUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 2://GET DATE
        {

            camera_cmd = CAMERA_CMD_QUERY_SETTINGINFORMATION;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandGetMenuSettingsValuesUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;            
            break;
        }
        case 3://查询XML
        {
            camera_cmd = CAMERA_CMD_GET_SettingXML;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandGetCamMenu] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        
        default:
            break;
    }
    
}

-(void) requestFinished:(NSString*) result{

    NSRange range;
    BOOL JudeFormatSD = NO;

    switch (camera_cmd) {
        //查询设置信息
        case CAMERA_CMD_QUERY_SETTINGINFORMATION:
        {
            if (result) {
                
                for (int i = 0 ; i < _DataArray.count; i++) {
                    
                    AppXMlTemplate *appxml = [[AppXMlTemplate alloc]init];
                    appxml = [_DataArray objectAtIndex:i];
                    NSString *strGet = [NSString stringWithFormat:@"%@",appxml.XMlGet];
                    NSRange strRange = [result rangeOfString:strGet];
                    if (strRange.location != NSNotFound) {
                        NSString *lines = [[result substringFromIndex:NSMaxRange(strRange)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        NSRange sRange = [lines rangeOfString:@"\n"];
                        appxml.XMlCurrentValue = [lines substringToIndex:sRange.location];
                    }
                    
                }
            }
            
            NSRange FWversion = [result rangeOfString:@"Camera.Menu.FWversion"];
            //版本信息
            if(FWversion.location != NSNotFound){
                
                FirmwareVersion = [[NSString alloc]init];
                FirmwareVersion = [[result substringFromIndex:NSMaxRange(FWversion)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                FirmwareVersion = [FirmwareVersion substringWithRange:NSMakeRange(0,4)];
                
            }
            break;
        }
        case CAMERA_CMD_GET_SettingXML:
        {
            if (result.length > 100) {
                
                NSError *parseError = nil;
                NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:result error:&parseError];
                NSDictionary *camera = [xmlDictionary objectForKey:@"camera"];
                
                if (xmlDictionary == nil && camera == nil) {
                    
                    if (zhSP) {
                        [self.view makeToast:NSLocalizedString(@"Setting failure", nil) duration:1.5 position:nil];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Setting failure", @"DefLocalizable", nil) duration:1.5 position:nil];
                        
                    }
                        
                    
                    return ;
                }
                
                NSString *cameraUI = [[camera objectForKey:@"isdualcamera"]objectForKey:@"id"];
                if ([cameraUI integerValue] == 0) {
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"RearCameraLens"];
                    
                }else{
                    
                    [[NSUserDefaults standardUserDefaults]setObject:@"1" forKey:@"RearCameraLens"];
                    
                }
                NSArray *tem = [[NSUserDefaults standardUserDefaults]arrayForKey:@"SettingsViewListArray"];
                if (tem == nil || tem.count < 1) {
                    //这些必须要显示设置界面上
                    NSDictionary *DictSet = [camera objectForKey:@"setting"];
                    NSDictionary *menu = [DictSet objectForKey:@"menu"];
                    
                    NSDictionary *streamingaddr = [camera objectForKey:@"streamingaddr"];
                    NSString *straddr = [streamingaddr objectForKey:@"id"];
               
                    
                    [[NSUserDefaults standardUserDefaults]setObject:straddr forKey:@"VideoPlayUrl"];
                    
                    
                    NSMutableArray *SettingViewArray = [NSMutableArray array];
                    
                    for (NSDictionary *temDitc in menu) {
                        
                        AppXMlTemplate *AppXML = [[AppXMlTemplate alloc]init];
                        NSString *title = [temDitc objectForKey:@"title"];
                        NSString *get = [temDitc objectForKey:@"get"];
                        NSString *set = [temDitc objectForKey:@"set"];
                        NSString *Switch = [temDitc objectForKey:@"switch"];
                        
                        NSDictionary *itemditc = [temDitc objectForKey:@"item"];
                        AppXML.XMlTitle = title;
                        AppXML.XMlGet = get;
                        AppXML.XMlSet = set;
                        AppXML.XMlSwitch = Switch;
                        
                        AppXML.XMlText = [NSMutableDictionary dictionary];;
                        AppXML.XMlID  = [NSMutableDictionary dictionary];
                        AppXML.XMlArray = [NSMutableArray array];
                        
                        
                        for (NSDictionary *item in itemditc) {
                            
                            NSString *strid = [item objectForKey:@"id"];
                            NSString *strtext = [item objectForKey:@"text"];
                            
                            
                            strtext =  [strtext stringByReplacingOccurrencesOfString:@"\t" withString:@""];
                            strtext =  [strtext stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                            [AppXML.XMlID   setObject:strid forKey:strtext];
                            [AppXML.XMlText setObject:strtext forKey:strid];
                            [AppXML.XMlArray addObject:strid];
                            
                            
                        }
                        
                        NSData *HistorData  = [NSKeyedArchiver archivedDataWithRootObject:AppXML];
                        [SettingViewArray addObject:HistorData];
                        
                    }
                    
                    [[NSUserDefaults standardUserDefaults]setObject:SettingViewArray forKey:@"SettingsViewListArray"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                }
                
            }
            
            [self initDataSource];
            break;
        }
        case CAMERA_CMD_SET_FORMATSDCARD:
        {
            range = [result rangeOfString:@"OK"];
            if(range.location != NSNotFound){
                
                JudeFormatSD = YES;
                
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Setting succeeds!", nil) duration:1.5 position:nil];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting succeeds!", @"DefLocalizable", nil) duration:1.5 position:nil];
                }
                
            }else{
                
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Setting failure", nil) duration:1.5 position:nil];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting failure", @"DefLocalizable", nil) duration:1.5 position:nil];
                    
                }
                
                
            }
            break;
        }
        case CAMERA_CMD_SET_RESTOREFACTORY:
        {
            range = [result rangeOfString:@"OK"];
            if(range.location != NSNotFound){
                JudeFormatSD = YES;
                NSString *info = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Info", nil)];
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Setting succeeds!", nil) duration:1.5 position:nil];
                    [self ShowAlertView:info  message:NSLocalizedString(@"Whether to return to the main interface?", nil) tag:221];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting succeeds!", @"DefLocalizable", nil) duration:1.5 position:nil];
                    
                    info = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil)];
                    
                    [self ShowAlertView:info  message:NSLocalizedStringFromTable(@"Whether to return to the main interface?", @"DefLocalizable", nil) tag:221];
                    
                }
                
                
               
                
                
                
            }else{
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Setting failure", nil) duration:1.5 position:nil];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting failure", @"DefLocalizable", nil) duration:1.5 position:nil];
                    
                }
                
                
            }
            break;
        }
        case CAMERA_CMD_SET_SettingXML:
        {

            [self.TableView reloadData];
            break;
        }
        case CAMERA_CMD_INVALID:
        {

            break;
        }
       
        default:
            break;
    }
    
    if (AnimationsView != nil) {
        
        if (camera_cmd == CAMERA_CMD_SET_RESTOREFACTORY || camera_cmd == CAMERA_CMD_SET_FORMATSDCARD ) {
            
            if (JudeFormatSD) {
                
                [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(CustomAnimationsSuccessImage) userInfo:nil repeats:NO];
            }else{
                if (AnimationsView) {
                    [self CustomAnimationsSuccessImage];
                }
                
            }
            
        
        }else{
            
            if (camera_cmd != CAMERA_CMD_QUERY_SETTINGINFORMATION) {
                
                [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(CustomAnimationsSuccessImage) userInfo:nil repeats:NO];
                
            }else{

                if (camera_cmd == CAMERA_CMD_SET_RESTOREFACTORY) {
                 
                }else{
                    
                    if (AnimationsView) {
                        [self CustomAnimationsSuccessImage];
                    }
                }
                
            }
            
        }
    }
    
    [self.TableView reloadData];
    camera_cmd = CAMERA_CMD_INVALID;
}

#pragma mark - 动画

-(void)LoaingCustomAnimationsView{
    
    JudeLoading = YES;
    btnBack.enabled = NO;
    self.TableView.userInteractionEnabled = NO;
    if (AnimationsView == nil) {
        AnimationsView = [[UIView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width /2 - 150 ,[[UIScreen mainScreen] bounds].size.height /2 - 200, 300, 300)];
    }
    
    AnimationsView.tag = 10013;
    
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 50, AnimationsView.frame.size.height/ 2 - 50, 100, 100)];
    UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake( AnimationsView.frame.size.width/ 2 - 75 , imageview.frame.size.height + imageview.frame.origin.y + 20, 150, 50)];
    message.backgroundColor = [UIColor clearColor];
    message.tag = 10012;
    message.font = [UIFont systemFontOfSize:20];
    message.textColor = [UIColor whiteColor];
    message.textAlignment = NSTextAlignmentCenter;
    if (zhSP) {
        message.text = NSLocalizedString(@"Loading...", nil);
    }else{
        message.text = NSLocalizedStringFromTable(@"Loading...", @"DefLocalizable", nil);
        
    }
    
    imageview.image = [UIImage imageNamed:@"Imageloading.png"];
    imageview = [self rotate360DegreeWithImageView:imageview];
    imageview.tag = 10011;
    
    [AnimationsView addSubview:imageview];
    [AnimationsView addSubview:message];
    AnimationsView.backgroundColor = [UIColor clearColor];
    AnimationsView.userInteractionEnabled = NO;
    
    //让页面暗下动画
    if (parentView == nil) {
        
        parentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height)];
    }
    
    parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    parentView.tag = 10010;
    [self.view addSubview:parentView];
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                     }
                     completion:NULL
     ];
    
    [self.view addSubview:AnimationsView];
    
}

-(void)CustomAnimationsSuccessImage{
    
    if (AnimationsView) {
        
        UIImageView *SuccessImage = [[UIImageView alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 50, AnimationsView.frame.size.height/ 2 - 50, 100, 100)];
        SuccessImage.image = [UIImage imageNamed:@"ImageloadingSuccess.png"];
        CGRect imageview;
        for (UIView *obj in [AnimationsView subviews]) {
            
            if (obj.tag == 10011 || obj.tag == 10012) {
                
                if (obj.tag == 10011) {
                    UIImageView *a = (UIImageView *)obj;
                    [a.layer removeAllAnimations];
                    imageview = a.frame;
                }
                [obj removeFromSuperview];
            }
            
        }
        UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 75 , imageview.size.height + imageview.origin.y + 20, 150, 50)];
        message.backgroundColor = [UIColor clearColor];
        message.font = [UIFont systemFontOfSize:20];
        message.textColor = [UIColor whiteColor];
        message.textAlignment = NSTextAlignmentCenter;
        message.text = NSLocalizedString(@"Consummation", nil);
        message.tag = 10012;
        SuccessImage.tag = 10011;
        
        [AnimationsView addSubview:message];
        [AnimationsView addSubview:SuccessImage];
        
        [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(StopCustomAnimationsView) userInfo:nil repeats:NO];
    }
    
}

-(void)StopCustomAnimationsView{
    
    self.TableView.userInteractionEnabled = YES;
    for (UIView *del in [AnimationsView subviews]) {
        
        if (del.tag == 10011 || del.tag == 10012) {

            [del removeFromSuperview];
        }
        
    }
    
    for (UIView *obj in [self.view subviews]) {
        
        if (obj.tag == 10010 || obj.tag == 10013) {
            
            [obj removeFromSuperview];
        }
    }
    AnimationsView = nil;
    JudeLoading = NO;
    btnBack.enabled = YES;
}

- (UIImageView *)rotate360DegreeWithImageView:(UIImageView *)imageView{
    
    CABasicAnimation *animation = [ CABasicAnimation
                                   animationWithKeyPath: @"transform" ];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    
    //围绕Z轴旋转，垂直与屏幕
    animation.toValue = [NSValue valueWithCATransform3D:
                         
                         CATransform3DMakeRotation(M_PI, 0.0, 0.0, 1.0) ];
    
    animation.duration = 1.0;
    //旋转效果累计，先转180度，接着再旋转180度，从而实现360旋转
    animation.cumulative = YES;
    animation.repeatCount = 100;
    
    //在图片边缘添加一个像素的透明区域，去图片锯齿
    CGRect imageRrect = CGRectMake(0, 0,imageView.frame.size.width, imageView.frame.size.height);
    UIGraphicsBeginImageContext(imageRrect.size);
    [imageView.image drawInRect:CGRectMake(1,1,imageView.frame.size.width-2,imageView.frame.size.height-2)];
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [imageView.layer addAnimation:animation forKey:nil];
    return imageView;
}

#pragma mark - Table view delegate

//初始化数据
- (void)initDataSource{

    _DataArray = [[NSMutableArray alloc] init];
    NSArray *tem = [[NSUserDefaults standardUserDefaults]arrayForKey:@"SettingsViewListArray"];
    for (int i = 0; i < tem.count; i++) {
        
        AppXMlTemplate *appxml = [[AppXMlTemplate alloc]init];
        appxml = [NSKeyedUnarchiver unarchiveObjectWithData:[tem objectAtIndex:i]];
        [_DataArray addObject:appxml];
        
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(GetListDate) userInfo:nil repeats:NO];
    }
}

-(void)GetListDate{
        
    [self CommandsSend:2];
}

#pragma mark -- UITableViewDataSource,UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _DataArray.count + 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *acell=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:acell];
    
//    if (!cell) {
        
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:acell];
    
//    }
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 200, 40)];
    title.textColor = [UIColor blackColor];
    title.textAlignment = NSTextAlignmentLeft;
    title.font = [UIFont systemFontOfSize:15];
    
    if (indexPath.row == 0) {
        
        
        if (zhSP) {
            title.text = NSLocalizedString(@"Network Settings", nil);
        }else{
            title.text = NSLocalizedStringFromTable(@"Network Settings", @"DefLocalizable", nil);
            
        }
        [cell addSubview:title];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else if(indexPath.row == _DataArray.count + 1){
        
        if (zhSP) {
            title.text = NSLocalizedString(@"SW Version", nil);
        }else{
            title.text = NSLocalizedStringFromTable(@"SW Version", @"DefLocalizable", nil);
            
        }
        
        
        UILabel *CurrentValue = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 210, 10, 200, 40)];
        CurrentValue.textColor = [UIColor blackColor];
        CurrentValue.textAlignment = NSTextAlignmentRight;
        CurrentValue.font = [UIFont systemFontOfSize:15];
        

        NSString *appVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString *appBuildVer = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        NSString *appFullVer = [[appVer stringByAppendingString:@"."] stringByAppendingString:appBuildVer];
        
        CurrentValue.text = [NSString stringWithFormat:@"%@/%@",FirmwareVersion,appFullVer];
        
        
        [cell addSubview:title];
        [cell addSubview:CurrentValue];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else if(indexPath.row == _DataArray.count + 2){
        
        if (zhSP) {
            title.text = NSLocalizedString(@"Format SD Card", nil);
        }else{
            
            title.text = NSLocalizedStringFromTable(@"Format SD Card", @"DefLocalizable", nil);
            
        }
        
        [cell addSubview:title];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else if(indexPath.row == _DataArray.count + 3){
        
        if (zhSP) {
            title.text = NSLocalizedString(@"Restore", nil);
        }else{
            
            title.text = NSLocalizedStringFromTable(@"Restore", @"DefLocalizable", nil);
            
        }
        
        [cell addSubview:title];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
       
    }
    
    AppXMlTemplate *appxml = [[AppXMlTemplate alloc]init];
    appxml = [_DataArray objectAtIndex:indexPath.row - 1];
    
    title.text = appxml.XMlTitle;
    
    if ([appxml.XMlSwitch isEqualToString:@"yes"]) {
        
        UIImageView *imageswitch = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 80, 15, 60, 30)];
        if ([appxml.XMlCurrentValue integerValue] == 0) {
            
            imageswitch.image = [UIImage imageNamed:@"SettingView_SwitchOff.png"];
            
        }else{
            imageswitch.image = [UIImage imageNamed:@"SettingView_SwitchOn.png"];
        }
        
        [cell addSubview:imageswitch];
        
    }else{
        
        UILabel *CurrentValue = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 210, 10, 200, 40)];
        CurrentValue.textColor = [UIColor blackColor];
        CurrentValue.textAlignment = NSTextAlignmentRight;
        CurrentValue.font = [UIFont systemFontOfSize:15];
        
        CurrentValue.text = [appxml.XMlText objectForKey:appxml.XMlCurrentValue];
        [cell addSubview:CurrentValue];
    }
    
    
    [cell addSubview:title];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
    
}

//设置cell的高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
}

#pragma mark - 选择
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
 
    
    if (indexPath.row == 0) {
        
        GoToWIFIView = 1;
        UIViewController *SWIFIVIEW = [[SettingWIFIViewController alloc]initWithNibName:@"SettingWIFIViewController" bundle:nil];
        [self.navigationController pushViewController:SWIFIVIEW animated:YES];
        return;
        
    }else if(indexPath.row == _DataArray.count + 1){
        
        VersionCount++;
        if (VersionCount == 10) {
            
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SettingsViewListArray"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            JudeLoading = NO;
            [self CommandsSend:3];
        }        
        return;
        
    }else if(indexPath.row == _DataArray.count + 2){
       
        NSString *info = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Admonition", nil)];
        
        if (zhSP) {
            [self ShowAlertView:info  message:NSLocalizedString(@"Please note formating SD card will erase all the data inside!", nil) tag:201];
        }else{
            
            info = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTable(@"Admonition", @"DefLocalizable", nil)];
            [self ShowAlertView:info  message:NSLocalizedStringFromTable(@"Please note formating SD card will erase all the data inside!", @"DefLocalizable", nil) tag:201];
            
        }
        
        return;
        
    }else if(indexPath.row == _DataArray.count + 3){
        
        NSString *info = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Admonition", nil)];
        NSString *message = NSLocalizedString(@"Factory reset will erase all of the logger configuration information", nil);
        if (!zhSP) {
            
            info = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTable(@"Admonition", @"DefLocalizable", nil)];
            message = NSLocalizedStringFromTable(@"Factory reset will erase all of the logger configuration information", @"DefLocalizable", nil);
            
        }
        
        [self ShowAlertView:info message:message tag:80];
        return;
        
    }else{
        
        AppXMlTemplate *appxml = [[AppXMlTemplate alloc]init];
        appxml = [_DataArray objectAtIndex:indexPath.row - 1];
        NSString *property = appxml.XMlSet;
        if ([appxml.XMlSwitch isEqualToString:@"yes"]) {
            
            camera_cmd = CAMERA_CMD_SET_SettingXML;
            [self LoaingCustomAnimationsView];
            if ([appxml.XMlCurrentValue integerValue] == 0) {
                
                appxml.XMlCurrentValue = @"1";
                (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandSettingsViewUrl:property value:@"1"] Delegate:(id<AITCameraRequestDelegate>)self] ;
                
            }else{
             
                appxml.XMlCurrentValue = @"0";
                (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandSettingsViewUrl:property value:@"0"] Delegate:(id<AITCameraRequestDelegate>)self] ;
            }
            
            
            
        }else{
            
            //初始化提示框
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            
            //按钮类型：UIAlertActionStyleDefault
            for (int i = 0; i < appxml.XMlArray.count; i++) {
                
                NSString *strid = [appxml.XMlArray objectAtIndex:i];
                NSString *Title = [appxml.XMlText objectForKey:strid];
                
                
                [alert addAction:[UIAlertAction actionWithTitle:Title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    
                    [self LoaingCustomAnimationsView];
                    appxml.XMlCurrentValue = strid;
                    camera_cmd = CAMERA_CMD_SET_SettingXML;
                    (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandSettingsViewUrl:property value:strid] Delegate:(id<AITCameraRequestDelegate>)self];
                    
                }]];
                
            }
            
            //取消，类型：UIAlertActionStyleCancel
            if (zhSP) {
                
                [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil]];
            }else{
                [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil) style:UIAlertActionStyleCancel handler:nil]];
                
            }
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    }
    
}


@end
