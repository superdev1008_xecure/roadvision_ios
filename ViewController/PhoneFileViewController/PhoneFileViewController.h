//
//  PhoneFileViewController.h
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickLook/QuickLook.h>
#import "MBProgressHUD.h"

@interface PhoneFileViewController : UIViewController<UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UIDocumentInteractionControllerDelegate,QLPreviewControllerDelegate,QLPreviewControllerDataSource,MBProgressHUDDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *fileTypeSwitch;
@property (weak, nonatomic) IBOutlet UIImageView *ImageVideo;
@property (weak, nonatomic) IBOutlet UIImageView *ImagePhoto;
@property (weak, nonatomic) IBOutlet UIImageView *BTNImageOpen;
@property (weak, nonatomic) IBOutlet UIImageView *BTNImageDelete;
@property (weak, nonatomic) IBOutlet UIImageView *BTNImageShare;
@property (weak, nonatomic) IBOutlet UIImageView *BTNImageSelect;





- (IBAction)fileTypeChanged:(id)sender;
- (IBAction)buttonOpen:(id)sender;
- (IBAction)buttonDelete:(id)sender;
- (IBAction)buttonShare:(id)sender;
- (IBAction)buttonSelect:(id)sender;





@end
