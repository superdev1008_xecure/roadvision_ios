//
//  PhoneFileViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "PhoneFileViewController.h"
#import "AITFileCell.h"
#import "AITFileNode.h"
#import "VLCMovieViewController.h"
#import "AppDelegate.h"
#import "AITCameraCommand.h"
#import <objc/message.h>

@interface PhoneFileViewController (){
    

    NSInteger SectionZero;//用于记录用户选择或取消哪个数据
    BOOL SelectAll;
    
    NSString *directory ;
    NSArray *fileList ;
    NSMutableArray *movFileList;
    NSMutableArray *jpegFileList;
    bool retFromOpen;
    
    UIDocumentInteractionController *documentInteractionController ;
    
    UIImage *PVideoImage;
    UIImage *PVideoTouchImage;
    UIImage *PPhotoImage;
    UIImage *PPhotoTouchImage;
    NSInteger OpenNum;
    
    NSURL *openURL;
    
    MBProgressHUD *_PhoneViewHUD;
    AppDelegate *appDelegate;
}

@end

@implementation PhoneFileViewController

-(NSUInteger) supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 30)];
    if (zhSP) {
        titleLabel.text = NSLocalizedString(@"Phone Files", nil);
    }else{
        titleLabel.text = NSLocalizedStringFromTable(@"Phone Files", @"DefLocalizable", nil);
        
    }
    
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    self.navigationItem.titleView = titleLabel;
    
    UINib *nib = [UINib nibWithNibName:@"AITFileCell" bundle:nil];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    if([paths count] > 0)
    {
        directory = [paths objectAtIndex:0] ;
    }

    [self.tableView registerNib:nib forCellReuseIdentifier:[AITFileCell reuseIdentifier]] ;    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    if (appDelegate == nil) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    
    UIImage* imageBack = [UIImage imageNamed:@"BackImage.png"];
    CGRect frameimgBack = CGRectMake(100, 100, 15, 30);
    
    UIButton* backtBtn = [[UIButton alloc] initWithFrame:frameimgBack];
    [backtBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [backtBtn addTarget:self action:@selector(PhoneFileGoToMainView)
       forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]initWithCustomView:backtBtn];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    if (OpenNum != 3) {
        
        if (OpenNum == 0) {
            
            if (movFileList.count > 0) {
                
                self.fileTypeSwitch.selectedSegmentIndex = 0;
                movFileList = [[movFileList sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                fileList = movFileList;
            }
            
            
        }else if (OpenNum == 1){
            
            if (jpegFileList.count > 0) {
                
                self.fileTypeSwitch.selectedSegmentIndex = 1;
                jpegFileList = [[jpegFileList sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                fileList = jpegFileList;
            }
        }
        [self.tableView reloadData];
        OpenNum = 3;
        
    }else{
        
        self.fileTypeSwitch.selectedSegmentIndex = 0;
        fileList = [self getFileList] ;
        [self ReloadTabelViewDate];
        [AITFileCell startThumbnailFetching] ;
        [self.tableView reloadData];
        
        
    }
    SelectAll = YES;
    [self ButtonSettings];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated] ;
    
    if(retFromOpen){
        
        retFromOpen = NO;
        
        for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++){
            
            NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView deselectRowAtIndexPath:p animated:YES];
        }
        
        SectionZero = 0;
        return;
    }
    
    fileList = [self getFileList] ;
    [self ReloadTabelViewDate];
    [AITFileCell startThumbnailFetching] ;
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self.navigationController setToolbarHidden:TRUE];
}

- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated] ;
    
    [AITFileCell stopThumbnailFetching] ;
}

-(void)PhoneFileGoToMainView{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)LoadingHUDView{
    
    _PhoneViewHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_PhoneViewHUD];
    
    _PhoneViewHUD.color=[UIColor grayColor];
    _PhoneViewHUD.delegate = self;
    
    if (zhSP) {
        
        _PhoneViewHUD.labelText = NSLocalizedString(@"Loading...", nil);
        
    }else{
        
        _PhoneViewHUD.labelText = NSLocalizedStringFromTable(@"Loading...", @"DefLocalizable", nil);
      
    }
    
    
    _PhoneViewHUD.labelColor = [UIColor whiteColor];
    [_PhoneViewHUD show:YES];
}

-(void)ButtonSettings{
    
    
    appDelegate.allowRotation = NO;
    
    self.ImageVideo.frame = CGRectMake(0, 0, (self.view.frame.size.width / 2) - 1 , 50);
    self.ImagePhoto.frame = CGRectMake((self.view.frame.size.width / 2) + 1, 0, (self.view.frame.size.width / 2) - 1, 50);
    
    self.fileTypeSwitch.backgroundColor = [UIColor clearColor];
    self.fileTypeSwitch.tintColor = [UIColor clearColor];
    
    if (zhSP) {
        
        PVideoImage = [UIImage imageNamed:@"PhoneVideo.png"];
        PPhotoImage = [UIImage imageNamed:@"PhonePhoto.png"];
        PVideoTouchImage = [UIImage imageNamed:@"PhoneVideo1.png"];
        PPhotoTouchImage  = [UIImage imageNamed:@"PhonePhoto1.png"];
        
        self.BTNImageOpen.image = [UIImage imageNamed:@"SDFile_Open.png"];
        self.BTNImageDelete.image = [UIImage imageNamed:@"SDFile_Delete.png"];
        self.BTNImageShare.image = [UIImage imageNamed:@"SDFile_Share.png"];
        self.BTNImageSelect.image  = [UIImage imageNamed:@"SDFile_Select.png"];
        
    }else{
        
        PVideoImage = [UIImage imageNamed:@"English_PhoneVideo.png"];
        PVideoTouchImage = [UIImage imageNamed:@"English_PhoneVideo1.png"];
        PPhotoImage = [UIImage imageNamed:@"English_PhonePhoto.png"];
        PPhotoTouchImage = [UIImage imageNamed:@"English_PhonePhoto1.png"];
        
        self.BTNImageOpen.image = [UIImage imageNamed:@"English_SDOpen.png"];
        self.BTNImageDelete.image = [UIImage imageNamed:@"English_Delete.png"];
        self.BTNImageShare.image = [UIImage imageNamed:@"English_Share.png"];
        self.BTNImageSelect.image  = [UIImage imageNamed:@"SDFile_SelectALL.png"];
    }
    
    
    
    
    if (self.fileTypeSwitch.selectedSegmentIndex == 0) {
        
        self.ImageVideo.image = PVideoTouchImage;
        self.ImagePhoto.image = PPhotoImage;
        
    }else{
        
        self.ImageVideo.image = PVideoImage;
        self.ImagePhoto.image = PPhotoTouchImage;
    }
    
}

#pragma mark - IBAction

- (IBAction)fileTypeChanged:(id)sender {
    
    [self ReloadTabelViewDate];
    
    switch ([sender selectedSegmentIndex]){
        case 0: //Video
        {
            self.ImageVideo.image = PVideoTouchImage;
            self.ImagePhoto.image = PPhotoImage;
            movFileList = [[movFileList sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            fileList = movFileList;
            
        }
            break;
            
        case 1: //Image
        {
            self.ImageVideo.image = PVideoImage;
            self.ImagePhoto.image = PPhotoTouchImage;
            jpegFileList = [[jpegFileList sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            fileList = jpegFileList;
            
        }
            break;
    }
    
    [self.tableView reloadData];
    
    /* Fred20150722: to refetech thumbnail after segment control switched */
    [AITFileCell stopThumbnailFetching];
    [AITFileCell startThumbnailFetching];
}

- (IBAction)buttonOpen:(id)sender {
    
    NSString *message = [[NSString alloc]init];
    
    if (zhSP) {
        
         message = NSLocalizedString(@"You can only open one file", nil);
    }else{
        
         message = NSLocalizedStringFromTable(@"You can only open one file", @"DefLocalizable", nil);
        
    }
    
   
    
    if ( SectionZero == 1) {
        
        for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++) {
            
            NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:p];
            AITFileNode *fileNode = [[AITFileNode alloc]init];
            fileNode = [fileList objectAtIndex:p.row];
            
            if (cell && fileNode.blSelected) {
                
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", directory, fileNode.name] ;
                NSURL *url = [NSURL fileURLWithPath: filePath] ;
                openURL = [NSURL fileURLWithPath: filePath] ;
                
                if (openURL) {
                    
                    retFromOpen = YES;
                    fileNode.blSelected = NO;
                    AITFileCell *cell = (AITFileCell*)[self.tableView cellForRowAtIndexPath:p];
                    cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                    NSString *extension = [fileNode.name pathExtension] ;
                    
                    if ([extension caseInsensitiveCompare:@"jpg"] == NSOrderedSame || [extension caseInsensitiveCompare:@"jpeg"] == NSOrderedSame) {
                        
                        [self open];
                        
                    }else {
                        
                        VLCMovieViewController *viewController = [[VLCMovieViewController alloc] initWithNibName:@"VLCMovieViewController" bundle:nil] ;
                        
                        viewController.url = url ;
                        
                        viewController.edgesForExtendedLayout = UIRectEdgeNone;
                        
                        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style: UIBarButtonItemStyleDone target:nil action:nil] ;
                        if (!zhSP) {
                            
                            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Back", @"DefLocalizable", nil) style: UIBarButtonItemStyleDone target:nil action:nil] ;
                            
                        }
                        appDelegate.allowRotation = YES;
                        [self.navigationController pushViewController:viewController animated:YES];
                    }
                }
                break;
            }
        }
        
        [self ReloadTabelViewDate];
        OpenNum = self.fileTypeSwitch.selectedSegmentIndex;
        
    }else{
        
        if (SectionZero < 1) {
            
            if (zhSP) {
                
                message = NSLocalizedString(@"Please select the file to be opened", nil);
            }else{
                
                message = NSLocalizedStringFromTable(@"Please select the file to be opened", @"DefLocalizable", nil);
                
            }
            
        }
        
        [self ShowAlertView:message];
        [self ReloadTabelViewDate];
        
    }
}

- (IBAction)buttonDelete:(id)sender {
    
    NSString *message = [[NSString alloc]init];
    
    if (SectionZero > 0) {
        
        if (zhSP) {
            message = NSLocalizedString(@"Delete the selected file?", nil) ;
        }else{
            
            message = NSLocalizedStringFromTable(@"Delete the selected file?", @"DefLocalizable", nil);
            
        }
        
        [self ShowAlertViewMessage:message AlertTag:1313];
        
    }else{
        
        if (zhSP) {
            message = NSLocalizedString(@"Please select the file to be deleted", nil) ;
        }else{
            
            message =  NSLocalizedStringFromTable(@"Please select the file to be deleted", @"DefLocalizable", nil) ;
           
        }
        
        
        [self ShowAlertView:message];
    }

}

- (IBAction)buttonShare:(id)sender {
    
    NSInteger ShareNum = SectionZero;
    NSString *message = [[NSString alloc]init];
    
    if (ShareNum > 1) {
        
        if (zhSP) {
            
            message = NSLocalizedString(@"You can only share a file.", nil) ;
        }else{
            message = NSLocalizedStringFromTable(@"You can only share a file.", @"DefLocalizable", nil) ;
            
        }
        
        
        [self ShowAlertView:message];
        [self ReloadTabelViewDate];
        
        
    }else if (ShareNum == 0){
        
        
        if (zhSP) {
             message = NSLocalizedString(@"Please select the file you want to share.", nil) ;
            
        }else{
             message = NSLocalizedStringFromTable(@"Please select the file you want to share.", @"DefLocalizable", nil) ;
            
        }
       
        
        [self ShowAlertView:message];
        [self ReloadTabelViewDate];
        
    }else if(ShareNum == 1){
        
        if (zhSP) {
             message = NSLocalizedString(@"Please note that switching network.", nil);
        }else{
            
             message = NSLocalizedStringFromTable(@"Please note that switching network.", @"DefLocalizable", nil);
            
        }
       
        [self ShowAlertViewMessage:message AlertTag:1200];
    }

    
}

- (IBAction)buttonSelect:(id)sender {
    
    if (SelectAll) {
        
        if (fileList.count > 0 ) {
            
            SelectAll = !SelectAll;
            for (int i = 0; i < fileList.count; i++) {
                
                SectionZero++;
                AITFileNode *file = [fileList objectAtIndex:i] ;
                file.blSelected = YES;
            }
            [self.tableView reloadData];
            
            for (int i = 0; i < fileList.count; i++) {
                
                NSIndexPath *ip=[NSIndexPath indexPathForRow:i inSection:0];
                [self.tableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];
                NSIndexPath *path=[NSIndexPath indexPathForRow:i inSection:0];
                [self tableView:self.tableView didSelectRowAtIndexPath:path];
            }
            NSIndexPath *p=[NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView selectRowAtIndexPath:p animated:YES scrollPosition:UITableViewScrollPositionBottom];
        }
        
        
    }else{

        if (fileList.count > 0 ) {
            
            SelectAll = !SelectAll;
            SectionZero = 0;
            for (int i = 0; i < fileList.count; i++) {
                
                
                AITFileNode *file = [fileList objectAtIndex:i] ;
                file.blSelected = NO;
            }
            [self.tableView reloadData];
            
            for (int i = 0; i < fileList.count; i++) {
                
                
                NSIndexPath *path=[NSIndexPath indexPathForRow:i inSection:0];
                [self tableView:self.tableView didDeselectRowAtIndexPath:path];
            }
            
        }

    }
        

}

-(void)ShareFile{
    
    NSInteger RowNum = 0;

    if (SectionZero > 0){
        
        NSMutableArray *temArray = [NSMutableArray array];
        for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++){
            
            NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
            AITFileNode *fileNode = [[AITFileNode alloc]init];
            
            
            fileNode = [fileList objectAtIndex:p.row];
            RowNum = fileList.count;
            temArray = [NSMutableArray arrayWithArray:fileList];
            
            if (fileNode.blSelected)
            {
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", directory, fileNode.name] ;
                NSURL *url = [NSURL fileURLWithPath: filePath] ;
                if (url)
                {
                    documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
                    
                    [documentInteractionController setDelegate:(id<UIDocumentInteractionControllerDelegate>)self];
                    
                    [documentInteractionController presentOpenInMenuFromRect:[self.tableView cellForRowAtIndexPath:p].frame inView:self.tableView animated:YES] ;
                    
                    
                    for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++)
                    {
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.tableView cellForRowAtIndexPath:p];
                        AITFileNode *fileNode = [temArray objectAtIndex:p.row] ;
                        
                        if(cell)
                        {
                            cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"] ;
                            [self.tableView deselectRowAtIndexPath:p animated:YES];
                        }
                        fileNode.blSelected = NO;
                    }
                }
                break;
            }
            
            if (RowNum - 1 == i) {
                RowNum = 0;
                break;
            }
        }
        
    }
    
    if(_PhoneViewHUD != nil){
        
        _PhoneViewHUD.hidden = YES;
        _PhoneViewHUD = nil;
    }
    [self ReloadTabelViewDate];
    
}

-(void)DeleteDate{
    
    BOOL blDeleted = NO;
    if (SectionZero > 0) {
        
        for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++){
            
            NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];

            AITFileNode *fileNode = [[AITFileNode alloc]init];
            
            fileNode = [fileList objectAtIndex:p.row];
            
            if (fileNode.blSelected){
                
                blDeleted = YES;
                NSString *filePath = [NSString stringWithFormat:@"%@/%@", directory, fileNode.name];
                [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil] ;
            }
            
            
        }
        
    }

    
    if(blDeleted){
        
        fileList = [self getFileList] ;
        [self.tableView reloadData];
    }
    
    SectionZero = 0;
    
}


#pragma mark - 清除用户已选择

-(void)ReloadTabelViewDate{

    for (int i = 0; i < fileList.count; i++) {
            
        AITFileNode *fileNode = [fileList objectAtIndex:i];
        fileNode.blSelected = NO;
    }
    for (int i = 0; i < [self.tableView numberOfRowsInSection:0]; i++){
            
        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
        [self.tableView deselectRowAtIndexPath:p animated:YES];
    }
    
    
    [self.tableView reloadData];
    SectionZero = 0;

}

#pragma mark - ShowAlertView

-(void)ShowAlertView:(NSString *)Message{
    
    UIAlertView *sectionAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:Message delegate:self cancelButtonTitle:NSLocalizedString(@"Confim", nil) otherButtonTitles: nil];
    if (!zhSP) {
        
        sectionAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:Message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil) otherButtonTitles: nil];
        
        
    }
    [sectionAlert show];
}

-(void)ShowAlertViewMessage:(NSString *)Message AlertTag:(NSInteger ) alertTag{
    
    UIAlertView *showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:Message delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
    
    if (!zhSP) {
        
        showAlert = [[UIAlertView alloc]initWithTitle: NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:Message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil) otherButtonTitles: NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];

    }
    if (alertTag > 0) {
        showAlert.tag = alertTag;
    }
    [showAlert show];
    
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{


    
    if (alertView.tag == 1200) {
        
        if (buttonIndex == 1) {
            
            [self LoadingHUDView];
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(ShareFile) userInfo:nil repeats:NO];
            
            
        }else{
            
            [self ReloadTabelViewDate];
        }
        
        
    }else if (alertView.tag == 1313){
        
        if (buttonIndex == 1) {
            
            [self DeleteDate];
            
        }else{
            
            [self ReloadTabelViewDate];
        }
        
        
    }else{
        
        [self ReloadTabelViewDate];
    }
}

#pragma mark - TableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger SctionNum = fileList.count;
    
    return SctionNum;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;{
    
    AITFileCell *cell = [tableView dequeueReusableCellWithIdentifier:[AITFileCell reuseIdentifier]];
    AITFileNode *fileNode =  [[AITFileNode alloc]init];
    
    fileNode = [fileList objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[AITFileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[AITFileCell reuseIdentifier]];
    }
    
    if (self.fileTypeSwitch.selectedSegmentIndex == 0) {
        cell.fileIcon.image = [UIImage imageNamed:@"logo.png"];
    }else{
        cell.fileIcon.image = [UIImage imageNamed:@"logoImage.png"];
    }
    
    if (fileNode.blSelected) {
        
        cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
        
    } else{
        
        cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
        
    }
    
    cell.ImageLayout.constant = 15;
    cell->thumbnailFileName = fileNode.name;
    
    if(fileList == movFileList) {
        cell.fileName.text = [fileNode.name stringByReplacingOccurrencesOfString:@".avi" withString:@""] ;
        
        cell.fileName.text = [cell.fileName.text stringByReplacingOccurrencesOfString:@".MOV" withString:@""] ;
    } else {
        cell.fileName.text = [fileNode.name stringByReplacingOccurrencesOfString:@".jpg" withString:@""] ;
        
        cell.fileName.text = [cell.fileName.text stringByReplacingOccurrencesOfString:@".JPG" withString:@""] ;
    }
    
    NSDictionary * fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[NSString stringWithFormat:@"%@/%@", directory, fileNode.name] error:nil] ;
    
    double fileSize = [fileAttributes fileSize] ;
    
    NSString *sizeString = @"0" ;
    if (fileSize < 1024) {
        
        sizeString = [NSString stringWithFormat:@"%.1f", fileSize] ;
    } else {
        fileSize /= 1024 ;
        if (fileSize < 1024) {
            
            sizeString = [NSString stringWithFormat:@"%.1fK", fileSize] ;
        } else {
            fileSize /= 1024 ;
            if (fileSize < 1024) {
                
                sizeString = [NSString stringWithFormat:@"%.1fM", fileSize] ;
            } else {
                fileSize /= 1024 ;
                if (fileSize < 1024) {
                    
                    sizeString = [NSString stringWithFormat:@"%.1fG", fileSize] ;
                } else {
                    
                }
            }
        }
    }
    
    cell.fileSize.text = sizeString ;
    
    NSDate *date = (NSDate*)[fileAttributes objectForKey: NSFileCreationDate];
    
    NSDateFormatter *form = [[NSDateFormatter alloc] init];
    [form setDateFormat:@"yyyy/MM/dd HH:mm"] ;
    cell.fileDate.text = [form stringFromDate:date];
    
    
    
    
    cell.filePath = [NSURL fileURLWithPath: [NSString stringWithFormat:@"%@/%@", directory, fileNode.name]] ;
    
    [AITFileCell fetchThumbnail:cell];
    //设置选中颜色
    UIColor *color = [[UIColor alloc]initWithRed:81.0/255 green:199.0/255 blue:244.0/255 alpha:1];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = color;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AITFileCell *cell = (AITFileCell*) [tableView cellForRowAtIndexPath:indexPath];
    
    AITFileNode *fileNode = [[AITFileNode alloc]init];
    fileNode = [fileList objectAtIndex:indexPath.row];
    fileNode.blSelected = YES;
    cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
  
    SectionZero++;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AITFileCell *cell = (AITFileCell*) [tableView cellForRowAtIndexPath:indexPath];
    
    AITFileNode *fileNode = [[AITFileNode alloc]init];
    fileNode = [fileList objectAtIndex:indexPath.row];
    
    fileNode.blSelected = NO;
    cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
    
    SectionZero--;
    
    
    if (SectionZero > 10000000 || SectionZero < 1) { //点击分享，然后取消计数器会

        SectionZero = 0;
    }
    
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0)
        return UITableViewCellEditingStyleNone;
    return UITableViewCellEditingStyleDelete;
}

#pragma mark - Get Date

- (NSArray *)getFileList{
    
    if (directory){
        
       NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:NULL];;
        
        movFileList = [[NSMutableArray alloc] init] ;
        jpegFileList = [[NSMutableArray alloc] init] ;
    
        
        for(int i = 0; i < [directoryContent count]; i++)
        {
            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
            
            
            if([[directoryContent objectAtIndex:i] rangeOfString:@"jpg"].location != NSNotFound ||
               [[directoryContent objectAtIndex:i] rangeOfString:@"JPG"].location != NSNotFound)
            {
                
                fileNode.name = [directoryContent objectAtIndex:i];
                fileNode.time = [self FileNodeTimer:fileNode.name];
                fileNode.blSelected = NO;                
                [jpegFileList addObject: fileNode] ;
                
            } else if([[directoryContent objectAtIndex:i] rangeOfString:@"txt"].location == NSNotFound)
            {
                
                fileNode.name = [directoryContent objectAtIndex:i];
                fileNode.time = [self FileNodeTimer:fileNode.name];
                fileNode.blSelected = NO;
                [movFileList addObject: fileNode] ;
            }
        }
        
        [self SortArray];
        if([self.fileTypeSwitch selectedSegmentIndex] == 0)
        {
            movFileList = [[movFileList sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            return movFileList;
        }else
        {
            jpegFileList = [[jpegFileList sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            return jpegFileList;
        }
        
        return directoryContent;
    }
    
    return [[NSArray alloc] init] ;
}

-(NSString *)FileNodeTimer:(NSString *)ThisTimer {
    
    
    NSString *timer = [NSString stringWithFormat:@"%@",ThisTimer];
    
    if ([timer rangeOfString:@"DVR"].location !=NSNotFound) {
        
        timer = [timer stringByReplacingOccurrencesOfString:@"DVR" withString:@""];
        
        timer = [timer stringByReplacingOccurrencesOfString:@"_" withString:@""];
        if (timer.length >= 12) {
            
            timer = [timer substringToIndex:12];
            
            NSString *year = [timer substringToIndex:4];
            timer = [timer substringFromIndex:4];
            NSString *moth = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *day = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *hour = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *part = [timer substringToIndex:2];
            
            return [NSString stringWithFormat:@"%@/%@/%@ %@:%@",year,moth,day,hour,part];
            
        }else{
            return nil;
        }
        
    }else if ([timer rangeOfString:@"SOS"].location !=NSNotFound){
        
        timer = [timer stringByReplacingOccurrencesOfString:@"SOS" withString:@""];
        
        timer = [timer stringByReplacingOccurrencesOfString:@"_" withString:@""];
        if (timer.length >= 12) {
            
            timer = [timer substringToIndex:12];
            
            NSString *year = [timer substringToIndex:4];
            timer = [timer substringFromIndex:4];
            NSString *moth = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *day = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *hour = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *part = [timer substringToIndex:2];
            
            return [NSString stringWithFormat:@"%@/%@/%@ %@:%@",year,moth,day,hour,part];
            
        }else{
            return nil;
        }
        
    }else if ([timer rangeOfString:@"IMG"].location !=NSNotFound){
        
        timer = [timer stringByReplacingOccurrencesOfString:@"IMG" withString:@""];
        
        timer = [timer stringByReplacingOccurrencesOfString:@"_" withString:@""];
        if (timer.length >= 12) {
            
            timer = [timer substringToIndex:12];
            
            NSString *year = [timer substringToIndex:4];
            timer = [timer substringFromIndex:4];
            NSString *moth = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *day = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *hour = [timer substringToIndex:2];
            timer = [timer substringFromIndex:2];
            NSString *part = [timer substringToIndex:2];
            
            return [NSString stringWithFormat:@"%@/%@/%@ %@:%@",year,moth,day,hour,part];
            
        }else{
            return nil;
        }
        
    }else{
        return nil;
    }
    
}

//冒泡排序,按时间
-(void)SortArray{
    
    for (int i = 1; i < movFileList.count; i++) {
        
        for (int j=(int)[movFileList count]-1; j>=i; j--) {
            
            AITFileNode *filenode1 = [movFileList objectAtIndex:j];
            AITFileNode *filenode2 = [movFileList objectAtIndex:j - 1];
            NSString *timer = [NSString stringWithFormat:@"%@",filenode1.time];
            NSString *timer1 = [NSString stringWithFormat:@"%@",filenode2.time];
            double one;
            double two;
           
                
            timer = [timer stringByReplacingOccurrencesOfString:@"/" withString:@""];
                
            timer = [timer stringByReplacingOccurrencesOfString:@" " withString:@""];
            timer = [timer stringByReplacingOccurrencesOfString:@":" withString:@""];
            one = [timer doubleValue];
            
            
            
                
            timer1 = [timer1 stringByReplacingOccurrencesOfString:@"/" withString:@""];
                
            timer1 = [timer1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            timer1 = [timer1 stringByReplacingOccurrencesOfString:@":" withString:@""];
            two = [timer1 doubleValue];
        
            
            if (one > two) {
                [movFileList exchangeObjectAtIndex:j-1 withObjectAtIndex:j];
            }
            
        }
        
    }
    
    for (int i = 1; i < jpegFileList.count; i++) {
        
        for (int j=(int)[jpegFileList count]-1; j>=i; j--) {
            
            AITFileNode *filenode1 = [jpegFileList objectAtIndex:j];
            AITFileNode *filenode2 = [jpegFileList objectAtIndex:j - 1];
            NSString *timer = [NSString stringWithFormat:@"%@",filenode1.time];
            NSString *timer1 = [NSString stringWithFormat:@"%@",filenode2.time];
            double one;
            double two;
          
                
            timer = [timer stringByReplacingOccurrencesOfString:@"/" withString:@""];
                
            timer = [timer stringByReplacingOccurrencesOfString:@" " withString:@""];
            timer = [timer stringByReplacingOccurrencesOfString:@":" withString:@""];
            one = [timer doubleValue];
           
        
                
            timer1 = [timer1 stringByReplacingOccurrencesOfString:@"/" withString:@""];
                
            timer1 = [timer1 stringByReplacingOccurrencesOfString:@" " withString:@""];
            timer1 = [timer1 stringByReplacingOccurrencesOfString:@":" withString:@""];
            two = [timer1 doubleValue];
            
            
            if (one > two) {
                [jpegFileList exchangeObjectAtIndex:j-1 withObjectAtIndex:j];
            }
            
        }
        
    }
}


- (UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize{
    
    UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
    [image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return reSizeImage;
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller
{
   
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller
{
   
    return self.view;
    
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller
{
   
    return  self.view.frame;
}


- (void)open{
    
    appDelegate.allowRotation = YES;
    
    QLPreviewController *myQlPreViewController = [[QLPreviewController alloc]init];
    
    myQlPreViewController.delegate = self;
    
    myQlPreViewController.dataSource = self;
    
    [myQlPreViewController setCurrentPreviewItemIndex:0];
    
    [self presentViewController:myQlPreViewController animated:YES completion:nil];
    
}

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller{
    
    return 1;
}



- (id <QLPreviewItem>)previewController: (QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    return openURL;
}



@end
