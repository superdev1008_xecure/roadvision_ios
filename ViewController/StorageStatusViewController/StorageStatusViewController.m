//
//  StorageStatusViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "StorageStatusViewController.h"
#import "AITCameraCommand.h"
#import "Toast+UIView.h"


typedef enum
{
    CAMERA_CMD_QUERY_StorageStatu,
    CAMERA_CMD_INVALID
    
} Camera_cmd_t;

@interface StorageStatusViewController (){
    
    Camera_cmd_t camera_cmd;
    
}

@end

@implementation StorageStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 30)];
    
    if (zhSP) {
        titleLabel.text = NSLocalizedString(@"Storage Status", nil);
    }else{
        titleLabel.text = NSLocalizedStringFromTable(@"Storage Status", @"DefLocalizable", nil);
        
    }
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    self.navigationItem.titleView = titleLabel;
    
    NSString *version = [[NSUserDefaults standardUserDefaults]stringForKey:@"HardwareVersion"];
    if (![version isEqualToString:@"Version = 1"]){
        
        camera_cmd = CAMERA_CMD_QUERY_StorageStatu;
        (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandQueryCameraSDFileUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
    }else{
        
        if (zhSP) {
            [self ShowAlertView:nil message:NSLocalizedString(@"Device does not support queries", nil) tag:2013];
        }else{
            
            [self ShowAlertView:nil message:NSLocalizedStringFromTable(@"Device does not support queries", @"DefLocalizable", nil) tag:2013];
            
        }
        
        
        
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    UIImage* imageBack = [UIImage imageNamed:@"BackImage.png"];
    CGRect frameimgBack = CGRectMake(100, 100, 15, 30);
    
    UIButton* backtBtn = [[UIButton alloc] initWithFrame:frameimgBack];
    [backtBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [backtBtn addTarget:self action:@selector(StorageStatusViewGoToMainView)
       forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]initWithCustomView:backtBtn];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    if (zhSP) {
        
        self.LabelCardUsage.text = NSLocalizedString(@"Memory Card Usage", nil);
        self.LabelCapacity.text = [NSString stringWithFormat:@"%@:",NSLocalizedString(@"Memory card capacity", nil)];
        self.LabelUsedCapacity.text = [NSString stringWithFormat:@"%@",NSLocalizedString(@"Used Capacity:", nil)];
    }else{
        
        self.LabelCardUsage.text = NSLocalizedStringFromTable(@"Memory Card Usage", @"DefLocalizable", nil);
        self.LabelCapacity.text = [NSString stringWithFormat:@"%@:",NSLocalizedStringFromTable(@"Memory card capacity", @"DefLocalizable", nil)];
        self.LabelUsedCapacity.text = [NSString stringWithFormat:@"%@",NSLocalizedStringFromTable(@"Used Capacity:", @"DefLocalizable", nil)];
        
    }
    
}

-(void)StorageStatusViewGoToMainView{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIAlertView

-(void)ShowAlertView:(NSString *)info message:(NSString *)message tag:(NSInteger)tag{
    
    UIAlertView *showalert = [[UIAlertView alloc]init];
    
    if (info.length > 0) {
        
        
        if (zhSP) {
            showalert = [[UIAlertView alloc]initWithTitle:info message:message delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
        }else{
            
            showalert = [[UIAlertView alloc]initWithTitle:info message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil)otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
            
        }
        
    }else{
        
        if (zhSP) {
            showalert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
        }else{
            showalert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
            
        }
        
        
    }
    
    if (tag > 0) {
        showalert.tag = tag;
    }
    [showalert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 2013) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - 命令回调函数

-(void) requestFinished:(NSString*) result{
    
    NSValue *centerValue = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height - 75)];
    
    if (camera_cmd == CAMERA_CMD_QUERY_StorageStatu) {
        
        
        if ([result rangeOfString:@"OK"].location != NSNotFound) {
            
            NSRange range = [result rangeOfString:@"Camera.Menu.SDCardStatus"];
            if(range.location != NSNotFound && [result rangeOfString:@"123456789"].location == NSNotFound)
            {
                NSString *lines = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

                NSString *str1 = [[NSString alloc]init];
                NSString *str2 = [[NSString alloc]init];
                NSRange one = [lines rangeOfString:@","];
                if (one.location != NSNotFound) {
                    str1 = [lines substringToIndex:one.location];
                    str2 = [lines substringFromIndex:one.location + 1];

                }

                self.LabelCapacity.text = [NSString stringWithFormat:@"%@  %@ %@",self.LabelCapacity.text,str1,@"G"];
                 self.LabelUsedCapacity.text = [NSString stringWithFormat:@"%@ %.1f%@",self.LabelUsedCapacity.text, [str2 doubleValue]/1024 ,@"G"];
                double num = 0;
                num =   ([str2 doubleValue]/1024)/[str1 integerValue] ;
 
                if (num == 0) {
                    
                    self.ImageUse.image = [UIImage imageNamed:@"StorageStatus0"];
                    
                }else if (num > 0.1 && num <= 0.25) {
                    
                    self.ImageUse.image = [UIImage imageNamed:@"StorageStatus25"];
                    
                }else if (num > 0.25 && num <= 0.5){
                    
                    self.ImageUse.image = [UIImage imageNamed:@"StorageStatus50"];
                    
                }else if (num > 0.5 && num <= 0.85){
                    
                    self.ImageUse.image = [UIImage imageNamed:@"StorageStatus75"];
                    
                }else if (num > 0.85){
                    
                    self.ImageUse.image = [UIImage imageNamed:@"StorageStatus100"];
                    
                }
                
            }
            
        }else{
            
            if (zhSP) {
                [self.view makeToast:NSLocalizedString(@"Memory card does not support queries", nil) duration:1.5 position:centerValue];
            }else{
                
                [self.view makeToast:NSLocalizedStringFromTable(@"Memory card does not support queries", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                
            }
            
        }
    }
}



@end
