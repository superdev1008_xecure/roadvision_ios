//
//  StorageStatusViewController.h
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StorageStatusViewController : UIViewController<UIAlertViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *LabelCardUsage;
@property (weak, nonatomic) IBOutlet UILabel *LabelCapacity;
@property (weak, nonatomic) IBOutlet UILabel *LabelUsedCapacity;

@property (weak, nonatomic) IBOutlet UIImageView *ImageUse;

@end
