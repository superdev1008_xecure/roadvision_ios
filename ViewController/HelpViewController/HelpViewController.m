//
//  HelpViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "HelpViewController.h"
#import "AITCameraCommand.h"

@interface HelpViewController (){
    
    UIWebView *WebHelp;
}

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 30)];
    self.Textproblem1.layoutManager.allowsNonContiguousLayout = NO;
    if (zhSP) {
        
        titleLabel.text= NSLocalizedString(@"Help", nil);
        self.Textproblem1.text = NSLocalizedString(@"Help String", nil);
        
    }else{
        
        titleLabel.text= NSLocalizedStringFromTable(@"Help", @"DefLocalizable", nil);
        self.Textproblem1.text = NSLocalizedStringFromTable(@"Help String", @"DefLocalizable", nil);;
        
    }
    
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    self.navigationItem.titleView = titleLabel;
    [self.Textproblem1 setContentOffset:CGPointZero];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    UIImage* imageBack = [UIImage imageNamed:@"BackImage.png"];
    CGRect frameimgBack = CGRectMake(100, 100, 15, 30);
    
    UIButton* backtBtn = [[UIButton alloc] initWithFrame:frameimgBack];
    [backtBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [backtBtn addTarget:self action:@selector(HelpViewGoToMainView)
       forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]initWithCustomView:backtBtn];
    self.navigationItem.leftBarButtonItem = btnBack;
    
    [self.Textproblem1 setContentOffset:CGPointZero];
    self.Textproblem1.hidden = YES;
    
    WebHelp = [[UIWebView alloc]initWithFrame:self.view.bounds];
    WebHelp.delegate = self;
    NSString *contentImg = nil;
    if (zhSP) {
        
        if (iPhone4 || iPhone5) {
            contentImg = [self htmlForJPGImage:[UIImage imageNamed:@"iPhone4_Zh.jpg"]];
        }else{
            contentImg = [self htmlForJPGImage:[UIImage imageNamed:@"iPhone6_Zh.jpg"]];
        }
    }else{
        
        if (iPhone4 || iPhone5) {
            contentImg = [self htmlForJPGImage:[UIImage imageNamed:@"iPhone4_En.jpg"]];
        }else{
            contentImg = [self htmlForJPGImage:[UIImage imageNamed:@"iPhone6_En.jpg"]];
        }
        
    }
    NSString *content = [NSString stringWithFormat:@"<html><body>%@</body></html>", contentImg];
    [WebHelp loadHTMLString:content baseURL:nil];

    WebHelp.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    WebHelp.scalesPageToFit=YES;
    WebHelp.multipleTouchEnabled=YES;
    WebHelp.userInteractionEnabled=YES;
    [self.view addSubview:WebHelp];
}

- (NSString *)htmlForJPGImage:(UIImage *)image{
    
    NSData *imageData = UIImageJPEGRepresentation(image,1.0f);
//    NSData *imageData = UIImagePNGRepresentation(image);
    NSString *imageSource = [NSString stringWithFormat:@"data:image/jpg;base64,%@",[imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    return [NSString stringWithFormat:@"<div align=center><img src='%@' /></div>", imageSource];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.Textproblem1 setContentOffset:CGPointZero];
}

-(void)HelpViewGoToMainView{
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
