//
//  VideoViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "VideoViewController.h"
#import "MDRadialProgressView.h"
#import "AppDelegate.h"
#import "Toast+UIView.h"
#import <QuartzCore/QuartzCore.h>
#import "AITCameraCommand.h"
#import "AITUtil.h"
#import "AppDelegate.h"
#import "VLCConstants.h"
#import "AITFileDownloader.h"

typedef enum
{
    CAMERA_CMD_SNAPSHOT,
    CAMERA_CMD_RECORD,
    CAMERA_QUERY_RECORDING,
    CAMERA_PRE_STREAMING,
    CAMERA_QUERY_MUTE,
    CAMERA_QUERY_STATUS,
    CAMERA_QUERY_TIMESTAMP,
    CAMERA_CMD_INVALID,
    CAMERA_CMD_SYNCHRONIZE_TIME,
    CAMERA_CMD_MUTE_ON,
    CAMERA_CMD_MUTE_OFF,
    CAMERA_CMD_STRAT_RECORD,
    CAMERA_CMD_STOP_RECORD,
    CAMERA_QUERY_CAMID,
    CAMERA_CMD_VIDEO,
    CAMERA_QUERY_REARVIDEO
    
} Camera_cmd_t;

static NSString *CAMERAID_CMD_FRONT = @"front";
static NSString *CAMERAID_CMD_REAR = @"rear";

@interface VideoViewController (){
    
    
    BOOL appRecording ;
    BOOL cameraRecording ;
    BOOL DirectionplayerView;
    BOOL TouchplayerView;
    BOOL RotationlplayerView;
    BOOL TimerSynchronize;
    BOOL JudeVersion;
    BOOL JudePattern;
    BOOL SwitchPreview;
    
    NSTimer *PlayeVideoTimer;
    //请求数据
    Camera_cmd_t camera_cmd;
    NSTimer *dateTimer;
    NSTimer *loadingTimer;
    VLCMediaPlayer *mediaPlayer ;
    NSDate *cameraDate;
    NSDate *startDate;
    NSString *PlayUrl;
    //旋转使用
    UITapGestureRecognizer *MSGTAGP;
    CGFloat ButtonViewHeight;
    
    UIImage *Video_ActivityVideo;
    UIImage *Video_NotActivityVideo;
    UIImage *Video_ActivityPhoto;
    UIImage *Video_NotActivityPhoto;
    
    //动画
    UIView *parentView;
    UIView *AnimationsView;
    
    AppDelegate *appDelegate;
    
    AITFileDownloader *downloader;//自动下载
    NSTimer *dlTimer;
    NSString *downUrl;
}

@end

@implementation VideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.BackButton setImage:[UIImage imageNamed:@"Video_CameraMode.png"] forState:UIControlStateHighlighted];
    
    
    if (zhSP) {
        self.LabelVideoTitle.text = NSLocalizedString(@"Video View", nil);
    }else{
        
        self.LabelVideoTitle.text = NSLocalizedStringFromTable(@"Video View", @"DefLocalizable", nil);
        
    }
    self.LabelCameraMode.font = [UIFont systemFontOfSize:16];
    self.LabelTimer.font = [UIFont systemFontOfSize:16];

    [self SettingVideoView];
    
}


-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    if (appDelegate == nil) {
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
    }
    appDelegate.allowRotation = NO;
    if (appDelegate.RearCameraLens) {
    
        self.FRVideoBTN.hidden = NO;
        
    }else{
        
        self.FRVideoBTN.hidden = YES;
    }
    self.LabelTimer.textColor = [UIColor redColor];
    self.LabelCameraMode.textColor = [UIColor redColor];
    [self LoaingCustomAnimationsView];
    if([[AITUtil getCameraAddress] isEqualToString:@"error"]){
        
        if (zhSP) {
            [self.view makeToast:NSLocalizedString(@"Ensure connect to camera", nil) duration:2.0 position:nil];
        }else{
            
            [self.view makeToast:NSLocalizedStringFromTable(@"Ensure connect to camera", @"DefLocalizable", nil) duration:2.0 position:nil];
            
        }
        
        [self CustomAnimationsSuccessImage];
        [self.BackButton sendActionsForControlEvents:UIControlEventTouchDown];
        [self.BackButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        [self.BackButton setUserInteractionEnabled:YES];
    }
    cameraRecording = NO;
    [self Communication:3];

    SwitchPreview = YES;
    
    PlayUrl = [[NSUserDefaults standardUserDefaults]stringForKey:@"VideoPlayUrl"];
    if (PlayUrl) {
        
        PlayeVideoTimer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(PalyVideo) userInfo:nil repeats:NO];
        
        
    }else{
        
    }
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    ButtonViewHeight = self.navigationView.frame.size.height + self.BottomView.frame.size.height;
    self.LabelCameraMode.font = [UIFont systemFontOfSize:15];
    self.LabelTimer.font = [UIFont systemFontOfSize:15];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    if (PlayeVideoTimer != nil){
        [PlayeVideoTimer invalidate];
        PlayeVideoTimer = nil;
    }
    
    if (dateTimer != nil){
        [dateTimer invalidate];
        dateTimer = nil;
    }
    
    if (loadingTimer != nil){
        [loadingTimer invalidate];
        loadingTimer = nil;
    }
    if ([mediaPlayer isPlaying]){
        
        [mediaPlayer stop];
        mediaPlayer = nil;
        camera_cmd = -1;
    }
}

-(void)PalyVideo{
    
    PlayUrl =  [PlayUrl stringByReplacingOccurrencesOfString:@"ip" withString:[AITUtil getCameraAddress]];
    
    VLCMedia *media = [VLCMedia mediaWithURL:[NSURL URLWithString:PlayUrl]];
    [mediaPlayer setMedia:media];
    [mediaPlayer play];
    
}

-(void)SettingVideoView{
    
    if (dateTimer == nil) {
        dateTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateDateTime:) userInfo:nil repeats:YES];
    }
    
    /* Settings VLC */
    mediaPlayer = [[VLCMediaPlayer alloc]init];
    
    (void)[mediaPlayer initWithOptions:@[[NSString stringWithFormat:@"--%@=%@", kVLCSettingNetworkCaching,@"400"],[NSString stringWithFormat:@"--%@=%@", kVLCSettingClockJitter, kVLCSettingClockJitterDefaultValue],]];
    
    [mediaPlayer setDelegate:(id<VLCMediaPlayerDelegate>)self];//
    [mediaPlayer setDrawable:self.playerView] ;
    mediaPlayer.videoAspectRatio =  NULL;
    mediaPlayer.videoCropGeometry = NULL;
    
    Video_ActivityPhoto = [UIImage imageNamed:@"Video_ActivityPhoto.png"];
    Video_ActivityVideo = [UIImage imageNamed:@"Video_ActivityVideo.png"];
    Video_NotActivityPhoto = [UIImage imageNamed:@"Video_NotActivityPhoto.png"];
    Video_NotActivityVideo = [UIImage imageNamed:@"Video_NotActivityVideo.png"];
    
    JudePattern = YES;

    
    if (zhSP) {
        self.LabelCameraMode.text = NSLocalizedString(@"Logger recording mode", nil);
    }else{
        
        self.LabelCameraMode.text = NSLocalizedStringFromTable(@"Logger recording mode", @"DefLocalizable", nil);
    }
        
    self.ImageVide.image = Video_ActivityVideo;
    self.ImagePhoto.image = Video_NotActivityPhoto;
}

#pragma mark - IBAction

- (IBAction)VideoBackToMainViewController:(id)sender {
    
    if ([mediaPlayer isPlaying]){
        
        [mediaPlayer stop];
        mediaPlayer = nil;
        camera_cmd = -1;
    }
    
    if (dateTimer != nil) {
        [dateTimer invalidate];
        dateTimer = nil;
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

- (IBAction)VideoBackMainTouchDown:(id)sender {
    
    if (!cameraRecording) {
        
        [self Communication:8];
        
    }
    
    if (PlayeVideoTimer != nil){
        [PlayeVideoTimer invalidate];
        PlayeVideoTimer = nil;
    }
    
    if (dateTimer != nil){
        [dateTimer invalidate];
        dateTimer = nil;
    }
    
    if (loadingTimer != nil){
        [loadingTimer invalidate];
        loadingTimer = nil;
    }
    
}

- (IBAction)LeftButtonTouchUp:(id)sender {
    
    JudePattern = YES;
    
    if (zhSP) {
        self.LabelCameraMode.text = NSLocalizedString(@"Logger recording mode", nil);
    }else{
        
        self.LabelCameraMode.text = NSLocalizedStringFromTable(@"Logger recording mode", @"DefLocalizable", nil);
        
    }
    self.ImageVide.image = Video_ActivityVideo;
    self.ImagePhoto.image = Video_NotActivityPhoto;

}

- (IBAction)MiddleButtonTouchUp:(id)sender {
    

    if (JudePattern) {
      
        if (cameraRecording) {
            [self Communication:9];
        }else{
            [self Communication:8];
        }
        
    }else{
        
        [self Communication:0];
    }
    self.BTNCommand.enabled = NO;
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(ButtonRestore) userInfo:nil repeats:NO];
}

- (IBAction)RightButtonTouchUp:(id)sender {
    
    JudePattern = NO;
    
    if (zhSP) {
        self.LabelCameraMode.text = NSLocalizedString(@"Logger camera mode", nil);
    }else{
        
        self.LabelCameraMode.text = NSLocalizedStringFromTable(@"Logger camera mode", @"DefLocalizable", nil);
        
    }
    self.ImageVide.image = Video_NotActivityVideo;
    self.ImagePhoto.image = Video_ActivityPhoto;
    
}

- (IBAction)SwitchVideoPreview:(id)sender {

    [self LoaingCustomAnimationsView];
   
    if ([mediaPlayer isPlaying]){
        [mediaPlayer stop];
        
    }
    self.BTNCommand.enabled = NO;
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(SwitchVideo) userInfo:nil repeats:NO];

}

-(void)SwitchVideo{
    
    [self Communication:11];
    [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(ButtonRestore) userInfo:nil repeats:NO];
    
}

-(void)ButtonRestore{
    
    self.BTNCommand.enabled = YES;
    self.BTNLeft.enabled = YES;
    self.BTNRight.enabled = YES;
    
}


#pragma mark - 获取视频预览地址

-(void)GetVideoPalyUrl{
    
    [self Communication:2];
}

#pragma mark - Communication

-(void)Communication:(NSInteger)Num{
    
    switch (Num) {
        case 0://抓拍
        {
            camera_cmd = CAMERA_CMD_SNAPSHOT;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraSnapshotUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 1://031 开启暂停录像
        {
            camera_cmd = CAMERA_CMD_RECORD;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 2://获取预览地址
        {
            camera_cmd = CAMERA_PRE_STREAMING;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandQueryPreviewStatusUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 3://查询设备
        {
            camera_cmd = CAMERA_QUERY_STATUS;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandQueryCameraStatusUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 4://查询固件时间
        {
            camera_cmd = CAMERA_QUERY_TIMESTAMP;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandTimeStampUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 5://同步时间到固件
        {
            camera_cmd = CAMERA_CMD_SYNCHRONIZE_TIME;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraTimeSettingsUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 6:
        {
            camera_cmd = CAMERA_CMD_MUTE_ON;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraMuteUrl:@"MuteOn"] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 7:
        {
            camera_cmd = CAMERA_CMD_MUTE_OFF;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraMuteUrl:@"MuteOff"] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 8:
        {
            camera_cmd = CAMERA_CMD_STRAT_RECORD;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStartRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 9:
        {
            camera_cmd = CAMERA_CMD_STOP_RECORD;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStopRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 11://点击切换镜头执行
        {
            if (!SwitchPreview) {
              
                camera_cmd = CAMERA_CMD_VIDEO;
                (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandSetCameraidUrl:CAMERAID_CMD_FRONT] Delegate:(id<AITCameraRequestDelegate>)self] ;
                
                
            }else{
        
                camera_cmd = CAMERA_CMD_VIDEO;
                (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandSetCameraidUrl:CAMERAID_CMD_REAR] Delegate:(id<AITCameraRequestDelegate>)self] ;
                
            }
            break;
        }
        default:
            break;
    }
}

-(void) requestFinished:(NSString*) result{
    
    NSValue* pointValue = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height - 70)];
    
    switch (camera_cmd)
    {
        case CAMERA_PRE_STREAMING://预览地址
        {
            int rtsp;
            NSString *liveurl = [[NSString alloc]init];
            NSDictionary* dict = [AITCameraCommand buildResultDictionary:result];
            if (dict == nil) {
                break;
            }
            rtsp        = [[dict objectForKey:[AITCameraCommand PROPERTY_CAMERA_RTSP]] intValue];
            
            if (rtsp == 1){
                liveurl = [NSString stringWithFormat:@"rtsp://%@%@", [AITUtil getCameraAddress], DEFAULT_RTSP_URL_AV1];
            }else if (rtsp == 2){
                liveurl = [NSString stringWithFormat:@"rtsp://%@%@", [AITUtil getCameraAddress], DEFAULT_RTSP_URL_V1];
            }else{
                
                liveurl = [NSString stringWithFormat:@"http://%@%@", [AITUtil getCameraAddress], DEFAULT_MJPEG_PUSH_URL];
            }
            VLCMedia *media = [VLCMedia mediaWithURL:[NSURL URLWithString:liveurl]];
            [mediaPlayer setMedia:media];
            [mediaPlayer play];
            
            break;
        }
        case CAMERA_CMD_SNAPSHOT://拍照
        {
      
            if([result rangeOfString:@"OK"].location != NSNotFound){
                
                [self.view makeToast:NSLocalizedString(@"Snapshot success", nil) duration:0.5 position:pointValue];
                NSRange PicName = [result rangeOfString:@"CameraLastPicName"];
                if (PicName.location != NSNotFound) {
                    
                    downUrl = [[result substringFromIndex:NSMaxRange(PicName)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    //截取到换行符的string
                    NSRange strRange = [downUrl rangeOfString:@"\n"];
                    downUrl = [downUrl substringToIndex:strRange.location];
                    downUrl = [downUrl stringByReplacingOccurrencesOfString:@"SD:\\" withString:@"/SD/"];
                    downUrl = [downUrl stringByReplacingOccurrencesOfString:@"\\" withString:@"/"];
                    
                    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(OpenDownImage) userInfo:nil repeats:NO];
                }
                
            }else if ([result rangeOfString:@"716"].location != NSNotFound){
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Please card", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Please card", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
            }else{
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Setting failure", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting failure", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
                
                
            }
            break;
         }
        case CAMERA_CMD_RECORD://下发启动、关闭录像
        {
                
            cameraRecording = !cameraRecording ;
            
            if (cameraRecording){
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Start recording", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Start recording", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
                
                
            } else{
                
                self.ImageRed.hidden = NO;
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Stop recording", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Stop recording", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
                
            }
           
            break;
        }
        case CAMERA_QUERY_RECORDING://判断设备是否在录像
        {
            cameraRecording = NO;
            
            if(result){
                NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
                
                if(range.location != NSNotFound){
                    
                    NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    if([camearRecordStatus rangeOfString:@"Recording"].location != NSNotFound){
                        
                        cameraRecording = YES;
                    }
                    
                }
            }
            
           
            break;
        }
        case CAMERA_QUERY_STATUS:
        {
            cameraRecording = NO;
            if(result){
                
                /* Check recording status */
                NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
                
                if(range.location != NSNotFound)
                {
                    NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    if([camearRecordStatus rangeOfString:@"Recording"].location != NSNotFound)
                        cameraRecording = YES;
                }
                
            }
            
            if (cameraRecording){
                
                self.LabelCameraMode.font = [UIFont systemFontOfSize:16];
                
            }else{
               
                
                self.ImageRed.hidden = NO;
            }
            
           
            break;
        }
        case CAMERA_QUERY_TIMESTAMP://固件时间
        {
            if([result rangeOfString:@"OK"].location != NSNotFound){
                
                NSString *camearTimeStampYear;
                NSString *camearTimeStampMonth;
                NSString *camearTimeStampDay;
                NSString *camearTimeStampHour;
                NSString *camearTimeStampMinute;
                NSString *camearTimeStampSecond;
                
                /* Get year */
                NSRange range = [result rangeOfString:CAMEAR_PREVIEW_TIMESTAMP_YEAR];
                if(range.location != NSNotFound){
                    
                    camearTimeStampYear = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                /* Get month */
                range = [result rangeOfString:CAMEAR_PREVIEW_TIMESTAMP_MONTH];
                if(range.location != NSNotFound){
                    
                    camearTimeStampMonth = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                /* Get day */
                range = [result rangeOfString:CAMEAR_PREVIEW_TIMESTAMP_DAY];
                if(range.location != NSNotFound){
                    
                    camearTimeStampDay = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                }
                /* Get hour */
                range = [result rangeOfString:CAMEAR_PREVIEW_TIMESTAMP_HOUR];
                if(range.location != NSNotFound){
                    
                    camearTimeStampHour = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                /* Get minute */
                range = [result rangeOfString:CAMEAR_PREVIEW_TIMESTAMP_MINUTE];
                if(range.location != NSNotFound){
                    
                    camearTimeStampMinute = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                /* Get second */
                range = [result rangeOfString:CAMEAR_PREVIEW_TIMESTAMP_SECOND];
                if(range.location != NSNotFound){
                    
                    camearTimeStampSecond = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                }
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
                cameraDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%04d/%02d/%02d %02d:%02d:%02d",
                                                            [camearTimeStampYear intValue],
                                                            [camearTimeStampMonth intValue],
                                                            [camearTimeStampDay intValue],
                                                            [camearTimeStampHour intValue],
                                                            [camearTimeStampMinute intValue],
                                                            [camearTimeStampSecond intValue]]];
                NSTimeInterval startDateIntevalSinceNow = [startDate timeIntervalSinceNow];
                NSTimeInterval cameraDateIntevalSinceNow = [cameraDate timeIntervalSinceNow];
                NSTimeInterval timeInterval = startDateIntevalSinceNow - cameraDateIntevalSinceNow;
                NSNumber* boolSynchronizeTime = [[NSUserDefaults standardUserDefaults] objectForKey:@"boolSynchronizeTime"];
                
                if (boolSynchronizeTime == NULL){
                    
                    NSUserDefaults* userdefaults = [NSUserDefaults standardUserDefaults];
                    [userdefaults setObject:[NSNumber numberWithBool:YES] forKey:@"boolSynchronizeTime"];
                    [userdefaults synchronize];
                }
                
                if ( fabs(timeInterval) > 60){
                    
                    TimerSynchronize = YES;
                    [self Communication:5];
                }
            }
            break;
        }
        case CAMERA_CMD_SYNCHRONIZE_TIME:
        {
            
                if([result rangeOfString:@"OK"].location != NSNotFound)
                {
                    if (zhSP) {
                        [self.view makeToast:NSLocalizedString(@"Synchronization time success", nil) duration:0.5 position:nil];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Synchronization time success", @"DefLocalizable", nil) duration:0.5 position:nil];
                        
                    }
                    
                }else{
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Synchronous time failure", nil) duration:0.5 position:nil];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Synchronous time failure", @"DefLocalizable", nil) duration:0.5 position:nil];
                    }
                    
                }
            
            
            break;
        }
        case CAMERA_CMD_STOP_RECORD://048停止录像
        {
            if([result rangeOfString:@"OK"].location != NSNotFound){
                
                cameraRecording = NO;
                self.ImageRed.hidden = NO;
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Stop recording", nil) duration:0.5 position:pointValue];
                }else{
                    [self.view makeToast:NSLocalizedStringFromTable(@"Stop recording", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
                
            }else if ([result rangeOfString:@"716"].location != NSNotFound){
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Please card", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Please card", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
            }else{
                
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Setting failure", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting failure", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
            }
            
            break;
        }
        case CAMERA_CMD_STRAT_RECORD://开启录像
        {
            if([result rangeOfString:@"OK"].location != NSNotFound){
                
                cameraRecording = YES;
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Start recording", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Start recording", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
                
            }else if ([result rangeOfString:@"716"].location != NSNotFound){
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Please card", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Please card", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                    
                }
                
            }else{
                
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Setting failure", nil) duration:0.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting failure", @"DefLocalizable", nil) duration:0.5 position:pointValue];
                }
                
            }
            
            break;
        }
        case CAMERA_CMD_VIDEO:
        {
            if([result rangeOfString:@"OK"].location != NSNotFound) {
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Setting succeeds!", nil) duration:1.5 position:pointValue];
                    
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting succeeds!", @"DefLocalizable", nil) duration:1.5 position:pointValue];
                    
                }
                
                SwitchPreview = !SwitchPreview;
                
            }else{
                
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Setting failure,Please check device.", nil) duration:1.5 position:pointValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Setting failure,Please check device.", @"DefLocalizable", nil) duration:1.5 position:pointValue];
                    
                    
            }
                
            }
            
            [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(PalyVideo) userInfo:nil repeats:NO];
            
            break;
        }
        default:
            break;
    }
    
    if(camera_cmd == CAMERA_CMD_SNAPSHOT || camera_cmd == CAMERA_CMD_RECORD || camera_cmd == CAMERA_QUERY_RECORDING || camera_cmd == CAMERA_QUERY_STATUS)
    {
        if (!result){
          
            if (zhSP) {
                [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil)];
            }else{
                
                [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil)];
                
            }
            
        }
    }
    
    camera_cmd = CAMERA_CMD_INVALID;
    
    
}

- (UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize{
    
    UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
    [image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return reSizeImage;
}

#pragma mark - 自动下载

-(void)OpenDownImage{
    
    if (downUrl.length > 10) {
        [self DownImgae:downUrl];
    }
    
    
}

-(void)DownImgae:(NSString *)Url{
    
    NSString *DownUrl = [NSString stringWithFormat:@"%@",Url];
    if(dlTimer == nil){
        
        dlTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(OnMainupdate:) userInfo:nil repeats:YES];
    }
    
    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://%@%@", [AITUtil getCameraAddress], [DownUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] ;
    
    NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] ;
    
    downloader = [[AITFileDownloader alloc] initWithUrl:url Path:[directory stringByAppendingPathComponent:[DownUrl substringFromIndex:[DownUrl rangeOfString:@"/" options:NSBackwardsSearch].location + 1]]];
    
    
}

- (void)OnMainupdate:(NSTimer *)timer{
    
    if(downloader->downloading == false){
        [downloader startDownload];
        
    }
    
    if (downloader->bodyLength > 0) {
        
        if (dlTimer) {
            [dlTimer invalidate];
            dlTimer = nil;
        }
        [self CustomAnimationsSuccessImage];
        NSValue* pointValue = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height - 70)];
        if (zhSP) {
            [self.view makeToast:NSLocalizedString(@"Photos downloaded", nil) duration:0.5 position:pointValue];
        }else{
            
            [self.view makeToast:NSLocalizedStringFromTable(@"Photos downloaded", @"DefLocalizable", nil) duration:0.5 position:pointValue];
            
        }
        
        
        
        
    }else if (downloader->offsetInFile == -1){
        
        if (dlTimer) {
            [dlTimer invalidate];
            dlTimer = nil;
        }
        [self DownImageFailure];
        
    }
}

-(void)DownImageFailure{
    
    [self CustomAnimationsSuccessImage];
    
    UIAlertView *Failure = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:NSLocalizedString(@"Automatic picture download failed", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
    if (!zhSP) {
        
        Failure = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:NSLocalizedStringFromTable(@"Automatic picture download failed", @"DefLocalizable", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
        
    }
    [Failure show];
    
}

#pragma mark - NSTimer

- (void)updateDateTime:(NSTimer *)timer{
    
#if 0
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSTimeZone *zone = [NSTimeZone localTimeZone];
    [formatter setTimeZone:zone];
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    
    [self.dateTime setText:[formatter stringFromDate:date]];
    
#else
    if(cameraDate == nil && camera_cmd != CAMERA_QUERY_TIMESTAMP){
        
        /* Get timestamp from DV */
        
        [self Communication:4];
        
        startDate = [NSDate date];
        
    }else{
        
        if (!TimerSynchronize) {
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSTimeZone *zone = [NSTimeZone localTimeZone];
            [formatter setTimeZone:zone];
            [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
            
            NSTimeInterval timeInterval = fabs([startDate timeIntervalSinceNow]);
            NSDate *date = [cameraDate dateByAddingTimeInterval:timeInterval];
            [self.LabelTimer setText:[formatter stringFromDate:date]];
        }else{
            
            NSDate *date = [NSDate date];
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            NSTimeZone *zone = [NSTimeZone localTimeZone];
            [formatter setTimeZone:zone];
            [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
            
            [self.LabelTimer setText:[formatter stringFromDate:date]];
            
        }
        
        
        
    }
#endif
    
    if(cameraRecording){
        self.ImageRed.hidden = !self.ImageRed.hidden;

    }
}

- (void)updateProgress:(NSTimer *)timer{
    
    static int progress = 2;

    if([mediaPlayer isPlaying] || progress == 21)
    {

        [loadingTimer invalidate];
        loadingTimer = nil;
        appDelegate.allowRotation = YES;
        [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(CustomAnimationsSuccessImage) userInfo:nil repeats:NO];
        progress = 2;
        return;
    }

    progress++;
    
   
}

#pragma mark - 动画

-(void)LoaingCustomAnimationsView{
    
    if (AnimationsView == nil) {
        AnimationsView = [[UIView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width /2 - 150 ,[[UIScreen mainScreen] bounds].size.height /2 - 200, 300, 300)];
    }
    
    AnimationsView.tag = 10013;
    
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 50, AnimationsView.frame.size.height/ 2 - 50, 100, 100)];
    UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake( AnimationsView.frame.size.width/ 2 - 75 , imageview.frame.size.height + imageview.frame.origin.y + 20, 150, 50)];
    message.backgroundColor = [UIColor clearColor];
    message.tag = 10012;
    message.font = [UIFont systemFontOfSize:20];
    message.textColor = [UIColor whiteColor];
    message.textAlignment = NSTextAlignmentCenter;
    

    if (zhSP) {
        
        message.text = NSLocalizedString(@"Loading...", nil);
    }else{
        
        message.text = NSLocalizedStringFromTable(@"Loading...", @"DefLocalizable", nil);
        
    }
    
    imageview.image = [UIImage imageNamed:@"Imageloading.png"];
    imageview = [self rotate360DegreeWithImageView:imageview];
    imageview.tag = 10011;
    
    [AnimationsView addSubview:imageview];
    [AnimationsView addSubview:message];
    AnimationsView.backgroundColor = [UIColor clearColor];
    AnimationsView.userInteractionEnabled = NO;
    
    //让页面暗下动画
    if (parentView == nil) {
        
        parentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height)];
    }
    
    parentView.backgroundColor = [UIColor blackColor];//[UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    parentView.tag = 10010;
    [self.view addSubview:parentView];
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         parentView.backgroundColor = [UIColor blackColor];// [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                     }
                     completion:NULL
     ];
    
    [self.view addSubview:AnimationsView];
    loadingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateProgress:) userInfo:nil repeats:YES];
    
}

-(void)CustomAnimationsSuccessImage{
    
    
    for (UIView *obj in [AnimationsView subviews]) {
        
        if (obj.tag == 10011 || obj.tag == 10012) {
            
            if (obj.tag == 10011) {
                
                UIImageView *a = (UIImageView *)obj;
                [a.layer removeAllAnimations];
               
            }
            [obj removeFromSuperview];
        }
        
    }
    
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(StopCustomAnimationsView) userInfo:nil repeats:NO];
}

-(void)StopCustomAnimationsView{
    
    for (UIView *obj in [self.view subviews]) {
        
        if (obj.tag == 10010 || obj.tag == 10013) {
            
            [obj removeFromSuperview];
        }
    }
    
    AnimationsView = nil;
    parentView = nil;
}

- (UIImageView *)rotate360DegreeWithImageView:(UIImageView *)imageView{
    
    CABasicAnimation *animation = [ CABasicAnimation
                                   animationWithKeyPath: @"transform" ];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    
    //围绕Z轴旋转，垂直与屏幕
    animation.toValue = [NSValue valueWithCATransform3D:
                         
                         CATransform3DMakeRotation(M_PI, 0.0, 0.0, 1.0) ];
    
    animation.duration = 1.0;
    //旋转效果累计，先转180度，接着再旋转180度，从而实现360旋转
    animation.cumulative = YES;
    animation.repeatCount = 100;
    
    //在图片边缘添加一个像素的透明区域，去图片锯齿
    CGRect imageRrect = CGRectMake(0, 0,imageView.frame.size.width, imageView.frame.size.height);
    UIGraphicsBeginImageContext(imageRrect.size);
    [imageView.image drawInRect:CGRectMake(1,1,imageView.frame.size.width-2,imageView.frame.size.height-2)];
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [imageView.layer addAnimation:animation forKey:nil];
    return imageView;
}

#pragma mark - 页面旋转

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    if (!(toInterfaceOrientation == UIDeviceOrientationLandscapeLeft ||  toInterfaceOrientation == UIDeviceOrientationLandscapeRight)) {
        
        DirectionplayerView = NO;
        self.BottomView.alpha = 1.f;
        self.navigationView.alpha = 1.f;

        
    }else{
        
        if (MSGTAGP == nil) {
            
            MSGTAGP = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(CustomTouchplayerViewView:)];
            [MSGTAGP setNumberOfTouchesRequired:1];
            MSGTAGP.cancelsTouchesInView = NO;
            [self.playerView addGestureRecognizer:MSGTAGP];
        }
        DirectionplayerView = YES;
        TouchplayerView = NO;
        self.BottomView.alpha = 0.f;
        self.navigationView.alpha = 0.f;

        self.playerView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        
    }
    
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    if (DirectionplayerView) {
        
        RotationlplayerView = YES;
        
    }else{
        
        RotationlplayerView = NO;
        self.playerView.frame = CGRectMake(0, self.navigationView.frame.size.height, [[UIScreen mainScreen] bounds].size.width, self.view.frame.size.height - ButtonViewHeight);
    }
    
    
}

-(void)CustomTouchplayerViewView:(UITapGestureRecognizer *)sender{
    
    if (RotationlplayerView) {
        
        if (TouchplayerView) {
            
            TouchplayerView = !TouchplayerView;
            //隐藏
            [UIView animateWithDuration:.3f
                                  delay:0.f
                                options:UIViewAnimationCurveEaseIn
                             animations:^{
                                 
                                 self.BottomView.alpha = 0.f;

                                 
                             }
                             completion:^(BOOL finished){
                             }];
            
        }else{
            
            TouchplayerView = !TouchplayerView;
            //显示
            [UIView animateWithDuration:.3f
                                  delay:0.f
                                options:UIViewAnimationCurveEaseIn
                             animations:^{
                                 
                                 self.BottomView.alpha = 1.f;

                             }
                             completion:^(BOOL finished){
                             }];
            
            
        }
        
    }
    
}

@end
