//
//  VideoViewController.h
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MobileVLCKit/VLCMediaPlayer.h"

@interface VideoViewController : UIViewController<UIAlertViewDelegate,VLCMediaPlayerDelegate,VLCMediaDelegate>


@property (weak, nonatomic) IBOutlet UIView *BottomView;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UILabel *LabelVideoTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ImageRed;

@property (weak, nonatomic) IBOutlet UIImageView *ImageVide;
@property (weak, nonatomic) IBOutlet UIImageView *ImageTouchCommand;
@property (weak, nonatomic) IBOutlet UIImageView *ImagePhoto;

@property (weak, nonatomic) IBOutlet UIButton *BTNLeft;

@property (weak, nonatomic) IBOutlet UIButton *BTNCommand;
@property (weak, nonatomic) IBOutlet UIButton *BTNRight;



@property (weak, nonatomic) IBOutlet UIButton *BackButton;
@property (weak, nonatomic) IBOutlet UIButton *FRVideoBTN;

- (IBAction)VideoBackToMainViewController:(id)sender;
- (IBAction)VideoBackMainTouchDown:(id)sender;

- (IBAction)LeftButtonTouchUp:(id)sender;
- (IBAction)MiddleButtonTouchUp:(id)sender;
- (IBAction)RightButtonTouchUp:(id)sender;
- (IBAction)SwitchVideoPreview:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *LabelCameraMode;
@property (weak, nonatomic) IBOutlet UILabel *LabelTimer;


@end
