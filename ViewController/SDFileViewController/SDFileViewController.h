//
//  SDFileViewController.h
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "CustomIOS7AlertView.h"

@interface SDFileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate>

@property (weak, nonatomic) IBOutlet UISegmentedControl *fileTypeSwitch;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SwitchFrontOrRear;
@property (weak, nonatomic) IBOutlet UIView *FrontOrRearView;


@property (weak, nonatomic) IBOutlet UIImageView *ImageFVideo;
@property (weak, nonatomic) IBOutlet UIImageView *ImageRVideo;
@property (weak, nonatomic) IBOutlet UIImageView *ImageSOSVideo;
@property (weak, nonatomic) IBOutlet UIImageView *ImagePhoto;
@property (weak, nonatomic) IBOutlet UIImageView *ImgeVideo;
@property (weak, nonatomic) IBOutlet UIButton *downloadBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;

@property (weak, nonatomic) IBOutlet UIImageView *SDBTNDown;
@property (weak, nonatomic) IBOutlet UIImageView *SDBTNDelete;
@property (weak, nonatomic) IBOutlet UIImageView *SDBTNOpen;
@property (weak, nonatomic) IBOutlet UIImageView *SDSelect;


@property (weak, nonatomic) IBOutlet UITableView *TableView;


- (IBAction)BottomLeftButton:(id)sender;
- (IBAction)BottomCenterButton:(id)sender;
- (IBAction)BottomRightButton:(id)sender;
- (IBAction)ViewSwitchFrontOrRear:(UISegmentedControl*)sender;
- (IBAction)fileTypeChanged:(UISegmentedControl*)sender;
- (IBAction)BottonSelect:(id)sender;


@end

@interface SDFileViewController ()
{
@public
    int dlCount;//正在下载的文件数量
    int CountNum;//倒计时
}
@end
