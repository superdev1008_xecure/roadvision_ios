//
//  AITFileDownloader.m
//  WiFiCameraViewer
//
//  Created by Clyde on 2013/11/17.
//  Copyright (c) 2013年 a-i-t. All rights reserved.
//

#import "AITFileDownloader.h"
#import "SDFileViewController.h"


@implementation AITFileDownloader

-(id)initWithUrl:(NSURL *) url Path: (NSString *)path
{
    self = [super init] ;
    
    if (self)
    {
        
        filePath = path ;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
        {
            [[NSFileManager defaultManager] removeItemAtPath: filePath error: nil] ;
        }
        [[NSFileManager defaultManager] createFileAtPath:path contents:nil attributes:nil] ;
    
        handle = [NSFileHandle fileHandleForUpdatingAtPath:path] ;

        if (handle)
        {
            NSURLRequest * request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30.0] ;
            
            connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO] ;
            
            downloading = false;
/*
            [connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
            
            [connection start] ;
*/
        }
    }
    return self ;
}

-(void)startDownload
{
    if (connection)
    {
        [connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        
        [connection start] ;
        
        downloading = true;
        
        
        
    }
}

-(void)stopDownload:(NSString *)path{
    
    if (connection)
    {
        
        //[connection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
        [connection cancel] ;
        
        downloading = false;
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        [[NSFileManager defaultManager] removeItemAtPath: path error: nil] ;
    }
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (handle)
    {
        offsetInFile = -1;
        [handle closeFile] ;
        handle = nil ;
        
        [[NSFileManager defaultManager] removeItemAtPath: filePath error: nil] ;
    }
}

#pragma mark - NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [handle truncateFileAtOffset: 0];
    bodyLength = response.expectedContentLength ;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
   
    [handle writeData:data];
    
    offsetInFile = handle.offsetInFile;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
   
    [handle closeFile] ;
    handle = nil ;
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    
    localNotification.fireDate = [NSDate date] ;

    localNotification.alertBody = NSLocalizedString(@"File Downloaded", nil) ;
    NSDictionary *infoDict = [NSDictionary dictionaryWithObjectsAndKeys:filePath, @"filePath", nil];
    localNotification.userInfo = infoDict ;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}


@end
