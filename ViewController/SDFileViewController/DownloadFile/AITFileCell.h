//
//  AITFileCell.h
//  WiFiCameraViewer
//
//  Created by Clyde on 2013/11/2.
//  Copyright (c) 2013年 a-i-t. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MobileVLCKit/VLCMediaThumbnailer.h"
#import "AITFileDownloader.h"

#define zhSP ([[[NSLocale preferredLanguages] objectAtIndex:0] hasPrefix:@"zh-Hans"])//-CN

@interface AITFileCell : UITableViewCell <VLCMediaThumbnailerDelegate>


@property (strong, nonatomic) IBOutlet UIImageView *fileIcon;
@property (strong, nonatomic) IBOutlet UILabel *fileName;
@property (strong, nonatomic) IBOutlet UILabel *fileDate;
@property (strong, nonatomic) IBOutlet UILabel *fileSize;
@property (weak, nonatomic) IBOutlet UIImageView *checkBox;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ImageLayout;

@property (strong, nonatomic) NSURL *filePath ;

+ (NSString *)reuseIdentifier ;

+ (void)fetchThumbnail: (AITFileCell *)cell ;
+ (void)cancelFetch: (AITFileCell *)cell ;
+ (void)startThumbnailFetching ;
+ (void)stopThumbnailFetching ;

@end

@interface AITFileCell()
{
    BOOL fetching ;

@public
    UIProgressView *dlProgress;
    UILabel *dlProgressLabel;
    NSString *thumbnailFileName;
}
@end
