//
//  AITFileCell.m
//  WiFiCameraViewer
//
//  Created by Clyde on 2013/11/2.
//  Copyright (c) 2013年 a-i-t. All rights reserved.
//

#import "AITFileCell.h"
#import "MobileVLCKit/VLCMedia.h"


@implementation AITFileCell

static bool thumbnailerRunning ;
static NSMutableArray *thumbnailQueue ;
static AITFileCell* fetchingCell ;

+ (void)initialize
{
    thumbnailerRunning = NO ;
    thumbnailQueue = nil ;
}

+ (void)startThumbnailFetching
{
    thumbnailQueue = [[NSMutableArray alloc] init] ;
}

+ (void)stopThumbnailFetching
{
    [thumbnailQueue removeAllObjects] ;
    thumbnailQueue = nil ;
}

+(UIImage*) generateThumbnailFromImage:(UIImage*)theImage
{
    UIImage * thumbnail;
    CGSize destinationSize = CGSizeMake(144,144);
    
    UIGraphicsBeginImageContext(destinationSize);
    [theImage drawInRect:CGRectMake(0,0,destinationSize.width, destinationSize.height)];
    thumbnail = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return thumbnail;
}

+ (void)fetchNext
{
    fetchingCell = nil ;
    
    if (thumbnailQueue)
    {
        AITFileCell *cell = [thumbnailQueue firstObject] ;
        
        if (cell)
        {
            thumbnailerRunning = YES ;
            fetchingCell = cell ;
            [thumbnailQueue removeObject:cell] ;
            
            //NSString *extension = [cell.fileName.text pathExtension] ;
            NSString *extension = [cell->thumbnailFileName pathExtension];
            
            if ([extension caseInsensitiveCompare:@"jpg"] == NSOrderedSame || [extension caseInsensitiveCompare:@"jpeg"] == NSOrderedSame)
            {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND,0), ^
                {
                    UIImage *oroginal = [UIImage imageWithData:[NSData dataWithContentsOfURL:cell.filePath]];
                    
                    UIImage *thumbnail = [AITFileCell generateThumbnailFromImage:oroginal] ;
                    
                    oroginal = nil ;
                    
                    dispatch_async(dispatch_get_main_queue(), ^
                    {
                        
                        if (cell->fetching)
                            cell.fileIcon.image = thumbnail ;
                        
                        cell->fetching = NO ;
                        [AITFileCell fetchNext] ;
                    });
                } );
            }else{
                
                dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND,0), ^{
                    
                    VLCMedia *media = [VLCMedia mediaWithURL: cell.filePath] ;
                    
                    VLCMediaThumbnailer *thumbnailer = [VLCMediaThumbnailer thumbnailerWithMedia: media andDelegate:cell] ;
                    
                    thumbnailer.thumbnailHeight = 144 ;
                    thumbnailer.thumbnailWidth = 144 ;
                    
                    [thumbnailer fetchThumbnail] ;
                });
            }
        } else {
            thumbnailerRunning = NO ;
        }
    } else {
        thumbnailerRunning = NO ;
    }
}

+ (void)cancelFetch: (AITFileCell *)cell
{
    [thumbnailQueue removeObject:cell] ;

    cell->fetching = NO ;
}

+ (void)fetchThumbnail: (AITFileCell *)cell
{
    cell->fetching = YES ;
//    cell.fileIcon.image = [UIImage imageNamed:@"logo.png"];
    
    if (thumbnailQueue)
    {
        if (![thumbnailQueue containsObject:cell])
        {
            [thumbnailQueue addObject:cell] ;
            
            if (!thumbnailerRunning)
            {
                [AITFileCell fetchNext] ;
            }
        }
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    //self.fileDate.frame = CGRectMake(100, 34, self.frame.size.width-100-80, 21);
    //[self.fileDate adjustsFontSizeToFitWidth] ;
}

+ (NSString *)reuseIdentifier {
    return @"AITFileCell";
}

- (void)mediaThumbnailerDidTimeOut:(VLCMediaThumbnailer *)mediaThumbnailer
{
    dispatch_async(dispatch_get_main_queue(), ^{

        [AITFileCell fetchNext] ;
    });
}

- (void)mediaThumbnailer:(VLCMediaThumbnailer *)mediaThumbnailer didFinishThumbnail:(CGImageRef)thumbnail
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (fetching)
            self.fileIcon.image = [[UIImage alloc] initWithCGImage:thumbnail];
        
        fetching = NO ;
        [AITFileCell fetchNext] ;
    });
}


@end
