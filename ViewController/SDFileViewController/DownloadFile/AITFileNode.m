//
//  AITFileNode.m
//  WiFiCameraViewer
//
//  Created by Clyde on 2013/11/17.
//  Copyright (c) 2013年 a-i-t. All rights reserved.
//

#import "AITFileNode.h"

@implementation AITFileNode

- (NSComparisonResult)compare:(AITFileNode*)obj
{
    NSRange range_SOS_self = [self.name rangeOfString:@"SOS_"];
    NSRange range_SOS_obj = [obj.name rangeOfString:@"SOS_"];
    NSRange range_DVR_self = [self.name rangeOfString:@"DVR_"];
    NSRange range_DVR_obj = [obj.name rangeOfString:@"DVR_"];
    NSRange range_PIC_self = [self.name rangeOfString:@"PIC_"];
    NSRange range_PIC_obj = [obj.name rangeOfString:@"PIC_"];
    NSString* comparedString_self = nil;
    NSString* comparedString_obj = nil;
    
    if (range_SOS_obj.location != NSNotFound)
    {
        comparedString_obj = [obj.name substringFromIndex:NSMaxRange(range_SOS_obj)];
    }else if(range_DVR_obj.location != NSNotFound)
    {
        comparedString_obj = [obj.name substringFromIndex:NSMaxRange(range_DVR_obj)];
    }else if(range_PIC_obj.location != NSNotFound)
    {
        comparedString_obj = [obj.name substringFromIndex:NSMaxRange(range_PIC_obj)];
    }
    
    if(range_DVR_self.location != NSNotFound)
    {
        comparedString_self = [self.name substringFromIndex:NSMaxRange(range_DVR_self)];
    }else if(range_SOS_self.location != NSNotFound)
    {
        comparedString_self = [self.name substringFromIndex:NSMaxRange(range_SOS_self)];
    }else if(range_PIC_self.location != NSNotFound)
    {
        comparedString_self = [self.name substringFromIndex:NSMaxRange(range_PIC_self)];
    }
    
    return [comparedString_obj compare:comparedString_self];
}

@end
