//
//  SDFileViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "SDFileViewController.h"
#import "AITFileNode.h"
#import "AITFileCell.h"
#import "AITFileDownloader.h"
#import "AITUtil.h"
#import "GDataXMLNode.h"
#import "VLCMovieViewController.h"
#import "AppDelegate.h"
#import "Toast+UIView.h"
#import "AITCameraCommand.h"
#import "MJRefresh.h"
#import <objc/message.h>

typedef enum
{
    CAMERA_CMD_FETCH_FILES,
    CAMERA_CMD_DELETE_FILES,
    CAMERA_CMD_OPEN_FILE_QUERY_RECORDING,
    CAMERA_CMD_DOWNLOAD_FILE_QUERY_RECORDING,
    CAMERA_CMD_RECORD,
    CAMERA_CMD_INVALID,
    CAMERA_CMD_QURY_RECORDING_VIEWDISAPEARED,
    CAMERA_CMD_FRONTVIDEO_FETCH_FILES,//3.0版本使用 第一次去文件
    CAMERA_CMD_REARVIDEO_FETCH_FILES,
    CAMERA_CMD_NEWIMAGE_FETCH_FILES,
    CAMERA_CMD_REARNEWIMAGE_FETCH_FILES,
    CAMERA_CMD_SOSFRONTVIDEO_FETCH_FILES,
    CAMERA_CMD_SOSREARVIDEO_FETCH_FILES,
    CAMERA_CMD_RECORD_STRAT,
    CAMERA_CMD_RECORD_STOP
} Camera_cmd_t;

static NSString *TAG_DCIM = @"DCIM" ;
static NSString *TAG_file = @"file" ;
static NSString *TAG_name = @"name" ;
static NSString *TAG_format = @"format";
static NSString *TAG_size = @"size" ;
static NSString *TAG_attr = @"attr" ;
static NSString *TAG_time = @"time" ;
static NSString *TAG_amount = @"amount";


@interface SDFileViewController (){
    
    NSMutableArray *fileNodes ;
    NSMutableArray *movFileNodes ;//前路视频
    NSMutableArray *movRearNodes;//后路视频
    
    NSMutableArray *movSOSFrontNodes;//加急文件前后路数据
    NSMutableArray *movSOSRearNodes;
    
    NSMutableArray *jpegFileNodes ;//图片
    NSMutableArray *jpegRearFileNodes ;
    
    NSMutableArray *DeleteArray;
    NSMutableArray *SelectFileName;//判断是否下载过该文件
    NSMutableArray *DownArray;
    NSMutableArray *DownJpegArray;
    NSMutableArray *OpenImageArray;
    
    
    BOOL boolPushVLCMovieViewControllerOrDocumentInteractionController;
    NSInteger fetchSize ;
    int fileDelCount;//要删除文件的数目
    int amount;
    bool fetchFirst ;//第一次获取文件
    bool fetchStopped ;//获取文件停止
    bool openAfterDownload;
    bool retFromOpen;//是否打开文件，若为yes，支持横竖屏
    bool recPaused;//录像是否停止
    BOOL _isLoading;
    
    Camera_cmd_t camera_cmd;
    
    NSTimer *dlTimer;
    NSTimer *videoTimer;
    
    VLCMovieViewController *viewController;
    //add Ted
    BOOL JudeLoadOrDown;
    BOOL JudePhoto;//判断选项卡是不是在照片
    BOOL JudeISDown;//判断是否正在下载
    BOOL JudeISDel;
    BOOL SelectAll;
    NSInteger JudeVideo;//判断要获取文件类型
    NSInteger FileNum;//记录已选择文件
    
    int JudeSction;//记录分组后有几个Sction
    int JudeDwonOrOpen;
    NSTimer *NoOperation;
    
    UILabel *label_video;
    UILabel *label_photo;
    
    //记录跳转播放界面前控件选择的单元
    
    NSInteger FileS;
    NSInteger FRNum;
    AppDelegate *appDelegate;
    
    UIImage *FVideoImage;
    UIImage *FVideoTouchImage;
    
    UIImage *RVideoImage;
    UIImage *RVideoTouchImage;
    
    UIImage *SDVideoImage;
    UIImage *SDVideoTouchImage;
    
    UIImage *SDSOSImage;
    UIImage *SDSOSTouchImage;
    
    UIImage *SDPhotoImage;
    UIImage *SDPhotoTouchImage;
    
    UIImage *SDFPhotoImage;
    UIImage *SDFPhotoTouchImage;
    
    UIImage *SDRPhotoImage;
    UIImage *SDRPhotoTouchImage;
    
    UIImage *SDSOSFVideoImage;
    UIImage *SDSOSFVideoTouchImage;
    
    UIImage *SDSOSRVideoImage;
    UIImage *SDSOSRVideoTouchImage;
    
    UIImageView *VideoImageView;
    UIImageView *PhotoImageView;
    
    //动画
    UIView *parentView;
    UIView *AnimationsView;
}

@end

@implementation SDFileViewController

-(NSUInteger) supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 30)];
    
    if (zhSP) {
        titleLabel.text = NSLocalizedString(@"SD Files", nil);
    }else{
        titleLabel.text = NSLocalizedStringFromTable(@"SD Files", @"DefLocalizable", nil);
        
    }
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    self.navigationItem.titleView = titleLabel;
    
    
    UINib *nib = [UINib nibWithNibName:@"AITFileCell" bundle:nil];
    
    [self.TableView registerNib:nib forCellReuseIdentifier:[AITFileCell reuseIdentifier]] ;

    [self.TableView setEditing:(self.TableView.editing) animated:YES];
    
    self.SwitchFrontOrRear.hidden = YES;
    
    FileS = 5;
    FRNum = 5;
    
    UIImage* imageBack = [UIImage imageNamed:@"BackImage.png"];
    CGRect frameimgBack = CGRectMake(100, 100, 15, 30);
    
    UIButton* backtBtn = [[UIButton alloc] initWithFrame:frameimgBack];
    [backtBtn setBackgroundImage:imageBack forState:UIControlStateNormal];
    [backtBtn addTarget:self action:@selector(SDViewBackMainViewTouchDown:)
       forControlEvents:UIControlEventTouchDown];
    [backtBtn addTarget:self action:@selector(SDViewBackMainView:)
       forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]initWithCustomView:backtBtn];
    self.navigationItem.leftBarButtonItem = btnBack;
 
    
    [self SettingSDTableViewReloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    if(appDelegate == nil){
        
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    SelectAll = YES;
    appDelegate.allowRotation = NO;
    if (!appDelegate.RearCameraLens) {
        
        //先隐藏，判断有后摄像头才显示
        if (!retFromOpen) {
            
            self.FrontOrRearView.hidden = YES;
            self.TableView.frame = CGRectMake(0, 60 , self.view.frame.size.width, self.view.frame.size.height - (85 + self.deleteBtn.frame.size.height));
            appDelegate.allowRotation = NO;
        }
        
    }else{
        
        self.FrontOrRearView.hidden = NO;
        self.SwitchFrontOrRear.hidden = NO;
        [self ShowSwitchFrontOrRearView];
    }
    
    SelectFileName = [[NSMutableArray alloc]init];
    
    if (NoOperation == nil) {
        
        CountNum = 60;
        NoOperation = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(CountNumberBackToMainView) userInfo:nil repeats:YES];
    }
    
     openAfterDownload = NO;
    [self QueryDownFileName];
    NSString *JudeWifi = [[NSUserDefaults standardUserDefaults]stringForKey:@"JudeWifi"];
    if ([JudeWifi isEqualToString:@"Wifi=NO"]) {
        
        if (zhSP) {
            
            [self ShowAlaertView:NSLocalizedString(@"Connect your mobile phone to CarDV by Wi-Fi",nil) Tag:1715];
            
        }else{
            
            [self ShowAlaertView:NSLocalizedStringFromTable(@"Connect your mobile phone to CarDV by Wi-Fi", @"DefLocalizable", nil) Tag:1715];
            
        }
        
        
    }else{
        
        JudeDwonOrOpen = 0;
        boolPushVLCMovieViewControllerOrDocumentInteractionController = NO;
        _isLoading = NO;
        JudeLoadOrDown = YES;
        JudeSction = 0;
        
        
            
            JudePhoto = NO;
            JudeVideo = 0;
            
            self.fileTypeSwitch.backgroundColor = [UIColor clearColor];
            self.fileTypeSwitch.tintColor = [UIColor clearColor];
        
        
        if(retFromOpen){
            
            retFromOpen = NO;
            self.TableView.userInteractionEnabled = YES;
            
            if (FileS != 5) {
                self.fileTypeSwitch.selectedSegmentIndex = FileS;
                
                if (FRNum != 5) {
                    
                    self.SwitchFrontOrRear.selectedSegmentIndex = FRNum;
                    
                    if (FileS == 0) {
                        
                        if (FRNum == 0) {
                            
                            self.ImageFVideo.image = FVideoTouchImage;
                            self.ImageRVideo.image = RVideoImage;
                            
                            movFileNodes = [[movFileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                            if (movFileNodes.count < 1) {
                                
                                fetchFirst = YES;
                                [self RequestData:1 JudeHUD:YES];
                                
                            }else{
                                fileNodes = movFileNodes;
                            }
                            
                        }else{
                            
                            self.ImageFVideo.image = FVideoImage;
                            self.ImageRVideo.image = RVideoTouchImage;
                            
                            movRearNodes = [[movRearNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                            if (movRearNodes.count < 1) {
                                
                                fetchFirst = YES;
                                [self RequestData:12 JudeHUD:YES];
                            }else{
                                
                                fileNodes = movRearNodes;
                            }
                        }
                        
                    }else if (FileS == 1){
                        
                        if (FRNum == 0) {
                            
                            self.ImageFVideo.image = SDSOSFVideoTouchImage;
                            self.ImageRVideo.image = SDSOSRVideoImage;
                            
                            movSOSFrontNodes = [[movSOSFrontNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                            if (movSOSFrontNodes.count < 1) {
                                
                                fetchFirst = YES;
                                [self RequestData:13 JudeHUD:YES];
                                
                            }else{
                                fileNodes = movSOSFrontNodes;
                            }
                            
                        }else{
                            
                            self.ImageFVideo.image = FVideoImage;
                            self.ImageRVideo.image = RVideoTouchImage;
                            
                            self.ImageFVideo.image = SDSOSFVideoImage;
                            self.ImageRVideo.image = SDSOSRVideoTouchImage;
                            
                            movSOSRearNodes = [[movSOSRearNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                            
                            
                            if (movSOSRearNodes.count < 1) {
                                
                                fetchFirst = YES;
                                [self RequestData:14 JudeHUD:YES];
                            }else{
                                
                                fileNodes = movSOSRearNodes;
                            }
                        }
                        
                    }else if (FileS == 2){
                        
                        if (FRNum == 0) {
                            
                            self.ImageFVideo.image = SDFPhotoTouchImage;
                            self.ImageRVideo.image = SDRPhotoImage;
                            
                            jpegFileNodes = [[jpegFileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                            if (jpegFileNodes.count < 1) {
                                
                                fetchFirst = YES;
                                [self RequestData:3 JudeHUD:YES];
                                
                            }else{
                                fileNodes = jpegFileNodes;
                            }
                            
                        }else{
                            
                            self.ImageFVideo.image = SDFPhotoImage;
                            self.ImageRVideo.image = SDRPhotoTouchImage;
                            
                            jpegRearFileNodes = [[jpegRearFileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                            
                            
                            if (jpegRearFileNodes.count < 1) {
                                
                                fetchFirst = YES;
                                [self RequestData:31 JudeHUD:YES];
                            }else{
                                
                                fileNodes = jpegRearFileNodes;
                            }
                        }
                        
                    }
                    
                    
                }
            }
            FileNum = 0;
            FileS = 5;
            FRNum = 5;
            
            AITFileCell *cell= nil ;
            AITFileNode *fileNode = nil ;
            
            for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
            {
                NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                [self.TableView deselectRowAtIndexPath:p animated:YES];
                
                cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                fileNode = [fileNodes objectAtIndex:p.row] ;
                fileNode.blSelected = NO;
                cell.checkBox.image =[UIImage imageNamed:@"checkBox.png"];
            }
            [DeleteArray removeAllObjects];
            return;
            
        }else{
            
            
            self.SwitchFrontOrRear.selectedSegmentIndex = 0;
            self.fileTypeSwitch.selectedSegmentIndex = 0;
            self.TableView.userInteractionEnabled = YES;
            
            if (movFileNodes.count > 0) {
                
                fileNodes = movFileNodes;
                [self.TableView reloadData];
                
            }else{
                
                [fileNodes removeAllObjects];
            }
            DeleteArray = [[NSMutableArray alloc]init];
            movFileNodes = [[NSMutableArray alloc] init] ;
            movRearNodes = [[NSMutableArray alloc]init];
            movSOSFrontNodes = [[NSMutableArray alloc]init];
            movSOSRearNodes = [[NSMutableArray alloc]init];
            jpegFileNodes = [[NSMutableArray alloc] init] ;
            jpegRearFileNodes = [[NSMutableArray alloc] init] ;
            
        }
        
        if(!dlCount){
            
            fetchSize = 16 ;
            fetchStopped = NO ;
            JudeISDel = NO;
            
            //如果是2版本以下在此加第一次加载数据
            fetchFirst = YES;
            [self RequestData:1 JudeHUD:YES];
            
        }
        
        [self SettingButtonBackImage];
        
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [self.navigationController setToolbarHidden:TRUE];

    [self stopDownload];
    [AITFileCell stopThumbnailFetching];
    fetchStopped = YES ;
    if (NoOperation != nil) {
        [NoOperation invalidate];
        NoOperation = nil;
    }
    if (!boolPushVLCMovieViewControllerOrDocumentInteractionController){
        
        [self RequestData:7 JudeHUD:NO];
        
    }
    
    
}

#pragma mark - leftBarButtonItem

-(void)SDViewBackMainView:(UIButton *)button{
    
    [button setBackgroundImage:[UIImage imageNamed:@"BackImage.png"] forState:UIControlStateNormal];
    
    if (dlCount > 0) {
        
        NSString *message = NSLocalizedString(@"Abort downloading?", nil);
        if (!zhSP) {
            
            message = NSLocalizedStringFromTable(@"Abort downloading?", @"DefLocalizable", nil);
            
        }
        
        [self ShowAlaertView:message Tag:12121];
        
    }else{
        
        if (OpenImageArray.count > 0) {
            [self DeleteOpenImage];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(void)SDViewBackMainViewTouchDown:(UIButton *)button{
    
    [button setBackgroundImage:[UIImage imageNamed:@"ButtonBack1.png"] forState:UIControlStateNormal];
    
}

- (void)setBackBarButtonEnable{
    
    AnimationsView = nil;
    self.navigationItem.leftBarButtonItem.enabled = YES;
}

-(void)StoploadingHUD{
    
    if (!JudeLoadOrDown) {
        
        [self TableViewSelectReloadData];
        [self CustomAnimationsSuccessImage];
    }
    
}


#pragma mark - SettingsButton

-(void)SettingButtonBackImage{
    
    self.FrontOrRearView.frame = CGRectMake(0,self.fileTypeSwitch.frame.origin.y + self.fileTypeSwitch.frame.size.height + 1, self.view.frame.size.width, 50);
    self.SwitchFrontOrRear.frame = CGRectMake(0, 0, self.FrontOrRearView.frame.size.width, self.FrontOrRearView.frame.size.height);
    
    self.ImageFVideo.frame = CGRectMake(0, 0, (self.view.frame.size.width / 2) - 1 , 50);
    self.ImageRVideo.frame = CGRectMake((self.view.frame.size.width / 2) + 1, 0, (self.view.frame.size.width / 2) - 1, 50);
    
    double imageW = self.view.frame.size.width / 3;
    self.ImgeVideo.frame = CGRectMake(0, 0, imageW - 1, 50);
    self.ImageSOSVideo.frame = CGRectMake(self.ImgeVideo.frame.size.width + 1, 0, imageW, 50);
    self.ImagePhoto.frame = CGRectMake(self.ImageSOSVideo.frame.origin.x + self.ImageSOSVideo.frame.size.width +1 , 0, self.view.frame.size.width - (self.ImgeVideo.frame.size.width + self.ImageSOSVideo.frame.size.width +2), 50);
    
    
    
    
    if (zhSP) {
        
        FVideoImage        = [UIImage imageNamed:@"FVideo.png"];
        FVideoTouchImage   = [UIImage imageNamed:@"FVideo1.png"];
        
        RVideoImage        = [UIImage imageNamed:@"RVideo.png"];
        RVideoTouchImage   = [UIImage imageNamed:@"RVideo1.png"];
        
        SDVideoImage       = [UIImage imageNamed:@"SDVideo.png"];
        SDVideoTouchImage  = [UIImage imageNamed:@"SDVideo1.png"];
        
        SDSOSImage         = [UIImage imageNamed:@"SOSVideo.png"];
        SDSOSTouchImage    = [UIImage imageNamed:@"SOSVideo1.png"];
        
        SDPhotoImage       = [UIImage imageNamed:@"SDPhoto.png"];
        SDPhotoTouchImage  = [UIImage imageNamed:@"SDPhoto1.png"];
        
        SDFPhotoImage      = [UIImage imageNamed:@"SDFPhoto.png"];
        SDFPhotoTouchImage = [UIImage imageNamed:@"SDFPhoto1.png"];
        
        SDRPhotoImage      = [UIImage imageNamed:@"SDRPhoto.png"];
        SDRPhotoTouchImage = [UIImage imageNamed:@"SDRPhoto1.png"];

        
        SDSOSFVideoImage   = [UIImage imageNamed:@"SDSOSFVideo.png"];
        SDSOSFVideoTouchImage = [UIImage imageNamed:@"SDSOSFVideo1.png"];
        
        SDSOSRVideoImage   = [UIImage imageNamed:@"SDSOSRVideo.png"];
        SDSOSRVideoTouchImage = [UIImage imageNamed:@"SDSOSRVideo1.png"];
        
        
        self.SDBTNDown.image = [UIImage imageNamed:@"SDFile_Down.png"] ;
        self.SDBTNDelete.image = [UIImage imageNamed:@"SDFile_Delete.png"];
        self.SDBTNOpen.image = [UIImage imageNamed:@"SDFile_Open.png"];
        self.SDSelect.image  = [UIImage imageNamed:@"SDFile_Select.png"];
        
    }else{
        
        FVideoImage        = [UIImage imageNamed:@"English_FDV.png"];
        FVideoTouchImage   = [UIImage imageNamed:@"English_FDV1.png.png"];
        
        RVideoImage        = [UIImage imageNamed:@"English_RDV.png"];
        RVideoTouchImage   = [UIImage imageNamed:@"English_RDV1.png"];
        
        SDVideoImage       = [UIImage imageNamed:@"English_SDVideo.png"];
        SDVideoTouchImage  = [UIImage imageNamed:@"English_SDVideo1.png"];
        
        SDSOSImage         = [UIImage imageNamed:@"English_SDSOS.png"];
        SDSOSTouchImage    = [UIImage imageNamed:@"English_SDSOS1.png"];
        
        SDPhotoImage       = [UIImage imageNamed:@"English_SDPhoto.png"];
        SDPhotoTouchImage  = [UIImage imageNamed:@"English_SDPhoto1.png"];
        
        SDFPhotoImage = [UIImage imageNamed:@"English_FPhoto.png"];
        SDFPhotoTouchImage = [UIImage imageNamed:@"English_FPhoto1.png"];
        
        SDRPhotoImage = [UIImage imageNamed:@"English_RPhoto.png"];
        SDRPhotoTouchImage = [UIImage imageNamed:@"English_RPhoto1.png"];
        
        SDSOSFVideoImage   = [UIImage imageNamed:@"English_SOSFVideo.png"];
        SDSOSFVideoTouchImage = [UIImage imageNamed:@"English_SOSFVideo1.png"];
        
        SDSOSRVideoImage   = [UIImage imageNamed:@"English_SOSRVideo.png"];
        SDSOSRVideoTouchImage = [UIImage imageNamed:@"English_SOSRVideo1.png"];
        
        self.SDBTNDown.image = [UIImage imageNamed:@"English_Download.png"] ;
        self.SDBTNDelete.image = [UIImage imageNamed:@"English_Delete.png"];
        self.SDBTNOpen.image = [UIImage imageNamed:@"English_SDOpen.png"];
        self.SDSelect.image  = [UIImage imageNamed:@"SDFile_SelectALL.png"];
        
    }
    
    self.ImgeVideo.image     = SDVideoTouchImage;
    self.ImageSOSVideo.image = SDSOSImage;
    self.ImagePhoto.image    = SDPhotoImage;
    self.ImageFVideo.image   = FVideoTouchImage;
    self.ImageRVideo.image   = RVideoImage;
    
    
}

#pragma mark - IBAction

- (IBAction)fileTypeChanged:(UISegmentedControl*)sender{
    
    CountNum = 60;
    [fileNodes removeAllObjects];
    [SelectFileName removeAllObjects];
    [DeleteArray removeAllObjects];
    [self.TableView reloadData];
    FileNum = 0;
    
    [self.TableView.mj_footer setRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
   
    self.SwitchFrontOrRear.selectedSegmentIndex = 0;
    self.ImageFVideo.image = FVideoTouchImage;
    self.ImageRVideo.image = RVideoImage;
    switch ([sender selectedSegmentIndex])
    {
        case 0:
        {
            self.ImgeVideo.image = SDVideoTouchImage;
            self.ImageSOSVideo.image = SDSOSImage;
            self.ImagePhoto.image = SDPhotoImage;
            
            JudePhoto = NO;
            if (movFileNodes.count < 1) {
                [self RequestData:1 JudeHUD:YES];
            }else{
                fileNodes = movFileNodes;
            }
            
            break;
        }
        case 1:
        {
            
            self.ImgeVideo.image = SDVideoImage;
            self.ImageSOSVideo.image = SDSOSTouchImage;
            self.ImagePhoto.image = SDPhotoImage;
            
            self.ImageFVideo.image = SDSOSFVideoTouchImage;
            self.ImageRVideo.image = SDSOSRVideoImage;
            
            JudePhoto = NO;
            if (movSOSFrontNodes.count < 1) {
                
                fetchFirst = YES;
                [self RequestData:13 JudeHUD:YES];
            }else{
                fileNodes = movSOSFrontNodes;
            }
            
            break;
        }
        case 2:
        {
            self.ImgeVideo.image = SDVideoImage;
            self.ImageSOSVideo.image = SDSOSImage;
            self.ImagePhoto.image = SDPhotoTouchImage;
            
            self.ImageFVideo.image = SDFPhotoTouchImage;
            self.ImageRVideo.image = SDRPhotoImage;
            
            JudePhoto  = YES;
            jpegFileNodes = [[jpegFileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
            
            if (jpegFileNodes.count < 1) {
                fetchFirst = YES;
                [self RequestData:3 JudeHUD:YES];
            }else{
                fileNodes = jpegFileNodes;
            }
            break;
        }
        default:
            break;
    }

    
    [self.TableView reloadData];
    
    /* Fred20150722: to refetech thumbnail after segment control switched */
    [AITFileCell stopThumbnailFetching];
    [AITFileCell startThumbnailFetching];
    
    if (fileNodes.count > 0 ) {
        
        for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
        {
         
            NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
            
            AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
            AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
            file.blSelected = FALSE;
            
            if(cell){
                
                cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                cell->dlProgress.hidden = YES;
            }
            
        }
    }
    
    [self.TableView reloadData];
    
    
}



- (IBAction)ViewSwitchFrontOrRear:(UISegmentedControl*)sender{
    
    CountNum = 60;
    [fileNodes removeAllObjects];
    [SelectFileName removeAllObjects];
    [DeleteArray removeAllObjects];
    [self.TableView reloadData];
    FileNum = 0;
    
    NSValue *centerValue = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height - 75)];
    
    [self.TableView.mj_footer setRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    if ([self.fileTypeSwitch selectedSegmentIndex] == 0) {
        
        switch (sender.selectedSegmentIndex) {
            case 0:
            {
                
                self.ImageFVideo.image = FVideoTouchImage;
                self.ImageRVideo.image = RVideoImage;
                
                movFileNodes = [[movFileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                if (movFileNodes.count < 1) {
                    
                    fetchFirst = YES;
                    [self RequestData:1 JudeHUD:YES];
                    
                }else{
                    fileNodes = movFileNodes;
                }
                
                
                break;
            }
            case 1:
            {
                self.ImageFVideo.image = FVideoImage;
                self.ImageRVideo.image = RVideoTouchImage;
                
                movRearNodes = [[movRearNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                if (appDelegate.RearCameraLens) {
                    
                    if (movRearNodes.count < 1) {
                        
                        fetchFirst = YES;
                        [self RequestData:12 JudeHUD:YES];
                    }else{
                        
                        fileNodes = movRearNodes;
                    }
                    
                }else{
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Please check the rear video.", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Please check the rear video.", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                }
                
                break;
            }
            default:
                break;
        }
        
    }else if ([self.fileTypeSwitch selectedSegmentIndex] == 1){
        
        switch (sender.selectedSegmentIndex) {
            case 0:
            {
                
                self.ImageFVideo.image = FVideoTouchImage;
                self.ImageRVideo.image = RVideoImage;
                
                self.ImageFVideo.image = SDSOSFVideoTouchImage;
                self.ImageRVideo.image = SDSOSRVideoImage;
                
                movSOSFrontNodes = [[movSOSFrontNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                if (movSOSFrontNodes.count < 1) {
                    
                    fetchFirst = YES;
                    [self RequestData:13 JudeHUD:YES];
                    
                }else{
                    fileNodes = movSOSFrontNodes;
                }
                
                break;
            }
            case 1:
            {
                self.ImageFVideo.image = FVideoImage;
                self.ImageRVideo.image = RVideoTouchImage;
                
                self.ImageFVideo.image = SDSOSFVideoImage;
                self.ImageRVideo.image = SDSOSRVideoTouchImage;
                
                movSOSRearNodes = [[movSOSRearNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                if (appDelegate.RearCameraLens) {
                    
                    if (movSOSRearNodes.count < 1) {
                        
                        fetchFirst = YES;
                        [self RequestData:14 JudeHUD:YES];
                    }else{
                        
                        fileNodes = movSOSRearNodes;
                    }
                    
                }else{
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Please check the rear video.", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Please check the rear video.", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                }
                
                break;
            }
            default:
                break;
        }
        
    }else if ([self.fileTypeSwitch selectedSegmentIndex] == 2){
        
        switch (sender.selectedSegmentIndex) {
            case 0:
            {
                
                self.ImageFVideo.image = SDFPhotoTouchImage;
                self.ImageRVideo.image = SDRPhotoImage;
                
                jpegFileNodes = [[jpegFileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                if (jpegFileNodes.count < 1) {
                    
                    fetchFirst = YES;
                    [self RequestData:3 JudeHUD:YES];
                    
                }else{
                    fileNodes = jpegFileNodes;
                }
                
                break;
            }
            case 1:
            {
                self.ImageFVideo.image = SDFPhotoImage;
                self.ImageRVideo.image = SDRPhotoTouchImage;
                
                jpegRearFileNodes = [[jpegRearFileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                if (appDelegate.RearCameraLens) {
                    
                    if (jpegRearFileNodes.count < 1) {
                        
                        fetchFirst = YES;
                        [self RequestData:31 JudeHUD:YES];
                    }else{
                        
                        fileNodes = jpegRearFileNodes;
                    }
                    
                }else{
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Please check the rear video.", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Please check the rear video.", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                }
                
                break;
            }
            default:
                break;
        }
        
    }
    
    [self.TableView reloadData];
    
    if (fileNodes.count > 0 ) {
        
        for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
        {
          
            NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
            AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
            AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
            file.blSelected = FALSE;
            
            if (cell) {
                cell.checkBox.image =  [UIImage imageNamed:@"checkBox.png"];
                cell->dlProgress.hidden = YES;
            }
            
            
            
        }
        
        [self.TableView reloadData];
    }
    
    
}

//下载
- (IBAction)BottomLeftButton:(id)sender {
    
    if (FileNum < 1) {
        
        NSString *message = NSLocalizedString(@"Please select the file to be downloaded", nil);
        if (!zhSP) {
            
            message = NSLocalizedStringFromTable(@"Please select the file to be downloaded", @"DefLocalizable", nil);
            
        }
        [self ShowAlaertView:message];
        
    }else{
        
        NSArray* visibleCells = [self.TableView visibleCells];
        NSInteger visibleCellCount = 0;
        for (AITFileCell* visibleCell in visibleCells)
        {
            AITFileNode *file = [fileNodes objectAtIndex:visibleCell.tag] ;
            if (file.blSelected) {
                visibleCellCount++;
            }
        }
        
        if( visibleCellCount == 0)
        {
            [self TableViewSelectReloadData];
            return;
        }
        
        
        JudeLoadOrDown = NO;
        JudeISDown = YES;
        JudeDwonOrOpen = 1;
        [self RequestData:51 JudeHUD:NO];
        
    }

    
}

//删除
- (IBAction)BottomCenterButton:(id)sender {
    
    NSString *message;
//    if (self.fileTypeSwitch.selectedSegmentIndex == 1) {
//        
//        message = NSLocalizedString(@"Lock files can not be deleted.", nil) ;
//        [self ShowAlaertView:message];
//        return;
//        
//    }else if (self.SwitchFrontOrRear.selectedSegmentIndex == 1) {
//        
//        message = NSLocalizedString(@"The file can‘t be deleted", nil) ;
//        [self ShowAlaertView:message];
//        return;
//        
//    }else
    if (FileNum < 1){
        
        if (zhSP) {
            message = NSLocalizedString(@"Please select the file to be deleted", nil);
        }else{
            
            message = NSLocalizedStringFromTable(@"Please select the file to be deleted", @"DefLocalizable", nil);
            
        }
        
        [self ShowAlaertView:message];
        return;
        
    }else{
        
        if (DeleteArray.count > 0) {
            
            if (zhSP) {
                 message = NSLocalizedString(@"Delete the selected file?", nil) ;
            }else{
                
                 message = NSLocalizedStringFromTable(@"Delete the selected file?", @"DefLocalizable", nil);
            }
           
            [self ShowAlaertView:message Tag:1313];
            
        }else{
            
            if (zhSP) {
                
                message = NSLocalizedString(@"Please select the file to be deleted", nil);
            }else{
                
                message =  NSLocalizedStringFromTable(@"Please select the file to be deleted", @"DefLocalizable", nil);
                
            }
            
            [self ShowAlaertView:message];
        }
        
        
    }
    
}

-(void)SDViewDeleteDate{
    
    
    JudeDwonOrOpen = 2;

    fileDelCount = 0;
    NSValue *SDViewdelete = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height - 75)];
    
    NSMutableArray *deleteArray = [[NSMutableArray alloc]init];
    [DeleteArray removeAllObjects];
    for (int i = 0; i < fileNodes.count; i++) {
        
        AITFileNode *file = [fileNodes objectAtIndex:i] ;
        if(file.blSelected == YES){
            [DeleteArray addObject:file];
        }
        
    }

    for (int i = 0; i < DeleteArray.count ; i++){
        
        AITFileNode *delefile = [DeleteArray objectAtIndex:i] ;

        
            fileDelCount++;
            NSString* stringname = [delefile.name stringByReplacingOccurrencesOfString: @"/" withString:@"$"];
            NSURL* url = [AITCameraCommand commandDelFileUrl:stringname];
 
            NSURLRequest * request = [NSURLRequest requestWithURL:url] ;
            
            NSURLResponse* urlResponse = nil;
            NSError* error = nil;
            NSData* resultData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
            if (error){
               
            }
            
            NSString *result = [[NSString alloc] initWithData:resultData encoding:NSUTF8StringEncoding] ;

            if(result && ([result rangeOfString:@"OK"].location != NSNotFound))
            {
              
                
                [deleteArray addObject:delefile];
                
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Delete success", nil) duration:1.0 position:SDViewdelete];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Delete success", @"DefLocalizable", nil) duration:1.0 position:SDViewdelete];
                    
                }
                
                
                
            }else{
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Delete fail", nil) duration:1.0 position:SDViewdelete];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Delete fail", @"DefLocalizable", nil) duration:1.0 position:SDViewdelete];
                    
                }
                
                
            }
        
    }
    
    for (AITFileNode *fileNode in deleteArray){
        
        [fileNodes removeObject:fileNode];
        if([movFileNodes containsObject:fileNode])
        {
            [movFileNodes removeObject:fileNode];

        }
        if([movRearNodes containsObject:fileNode])
        {
            [movRearNodes removeObject:fileNode];
            
        }
        if([movSOSFrontNodes containsObject:fileNode])
        {
            [movSOSFrontNodes removeObject:fileNode];
            
        }
        if([movSOSRearNodes containsObject:fileNode])
        {
            [movSOSRearNodes removeObject:fileNode];
            
        }
        
        if ([jpegFileNodes containsObject:fileNode])
        {
            [jpegFileNodes removeObject:fileNode];

        }
        if ([jpegRearFileNodes containsObject:fileNode])
        {
            [jpegRearFileNodes removeObject:fileNode];
            
        }
        
    }
    
    [self.TableView reloadData];
    
//    if(blFoundSOSFile){
//        
//        
//        UIAlertView *alv = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"The SOS file cannot be deleted", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] ;
//        
//        [alv show];
//    }
    
    [self CustomAnimationsSuccessImage];
    NSInteger one = self.fileTypeSwitch.selectedSegmentIndex;
    NSInteger Two = self.SwitchFrontOrRear.selectedSegmentIndex;
    //删除数组，重新请求数据
    switch (one) {
        case 0:
        {
            if (Two == 0) {
                [movRearNodes removeAllObjects];
            }else{
                [movFileNodes removeAllObjects];
            }
            break;
        }
        case 1:
        {
            if (Two == 0) {
                [movSOSRearNodes removeAllObjects];
            }else{
                
                [movSOSFrontNodes removeAllObjects];
            }
            break;
        }
        case 2:
        {
            if (Two == 0) {
                [jpegRearFileNodes removeAllObjects];
            }else{
                [jpegFileNodes removeAllObjects];
            }
            break;
        }
        default:
            break;
    }
    
    [DeleteArray removeAllObjects];
    [self TableViewSelectReloadData];
    JudeISDel = NO;
    
    
    [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(DeleteSuccess) userInfo:nil repeats:NO];
    
    
}

-(void)DeleteSuccess{
    
    if (fileNodes.count < 1) {
        
        if ([self.fileTypeSwitch selectedSegmentIndex] == 0) {
            
            switch (self.SwitchFrontOrRear. selectedSegmentIndex) {
                case 0:
                {
                    
                    fetchFirst = YES;
                    [self RequestData:1 JudeHUD:YES];
                        
                    break;
                }
                case 1:
                {
                    fetchFirst = YES;
                    [self RequestData:12 JudeHUD:YES];
                    break;
                }
                default:
                    break;
            }
            
        }else if ([self.fileTypeSwitch selectedSegmentIndex] == 1){
            
            switch (self.SwitchFrontOrRear. selectedSegmentIndex) {
                case 0:
                {
                    fetchFirst = YES;
                    [self RequestData:13 JudeHUD:YES];
                    break;
                }
                case 1:
                {
                    fetchFirst = YES;
                    [self RequestData:14 JudeHUD:YES];
                    break;
                }
                default:
                    break;
            }
            
        }else if ([self.fileTypeSwitch selectedSegmentIndex] == 2){
            
            switch (self.SwitchFrontOrRear. selectedSegmentIndex) {
                case 0:
                {
                    fetchFirst = YES;
                    [self RequestData:3 JudeHUD:YES];
                    break;
                }
                case 1:
                {
                    fetchFirst = YES;
                    [self RequestData:31 JudeHUD:YES];
                            
                    break;
                }
                default:
                    break;
            }
            
        }
        
    }
    
}


//打开
- (IBAction)BottomRightButton:(id)sender {
    
    NSArray* visibleCells = [self.TableView visibleCells];
    NSInteger visibleCellCount = 0;
    for (AITFileCell* visibleCell in visibleCells)
    {
        AITFileNode *file = [fileNodes objectAtIndex:visibleCell.tag] ;
        if (file.blSelected) {
            visibleCellCount++;
        }
    }
    
    if (FileNum < 1 || visibleCellCount == 0) {
        
        NSString *message = NSLocalizedString(@"Please select the file to be opened", nil);
        if (!zhSP) {
            
            message = NSLocalizedStringFromTable(@"Please select the file to be opened", @"DefLocalizable", nil);
            
        }
        
        [self ShowAlaertView:message];
        [self TableViewSelectReloadData];
        
    }else if (FileNum == 1){
        
        if(dlCount)
        {
            return;
        }
        self.TableView.userInteractionEnabled = NO;
        JudeDwonOrOpen = 3;
        [self RequestData:5 JudeHUD:NO];
        
        
        
    }else{
        
        NSString *message = [[NSString alloc]init];
        
        message = NSLocalizedString(@"You can only open one file", nil);
        if (!zhSP) {
            
            message = NSLocalizedStringFromTable(@"You can only open one file", @"DefLocalizable", nil);
           
        }
        [self ShowAlaertView:message];
        [self TableViewSelectReloadData];
        
    }

    
}

- (IBAction)BottonSelect:(id)sender {
    
    
    if (SelectAll) {
        
        if (fileNodes.count > 0) {
            
            SelectAll = !SelectAll;
            for (int i = 0; i < fileNodes.count; i++) {
                
                FileNum++;
                AITFileNode *file = [fileNodes objectAtIndex:i] ;
                file.blSelected = YES;
            }
            [self.TableView reloadData];
            
            for (int i = 0; i < fileNodes.count; i++) {
                
                NSIndexPath *ip=[NSIndexPath indexPathForRow:i inSection:0];
                [self.TableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];
                NSIndexPath *path=[NSIndexPath indexPathForItem:i inSection:0];
                [self tableView:self.TableView didSelectRowAtIndexPath:path];
            }
            NSIndexPath *p=[NSIndexPath indexPathForRow:0 inSection:0];
            [self.TableView selectRowAtIndexPath:p animated:YES scrollPosition:UITableViewScrollPositionBottom];
        }
        
        
    }else{
        
        if (fileNodes.count > 0) {
            
            SelectAll = !SelectAll;
            for (int i = 0; i < fileNodes.count; i++) {
                
                AITFileNode *file = [fileNodes objectAtIndex:i] ;
                file.blSelected = NO;
            }
            [self.TableView reloadData];
            
            for (int i = 0; i < fileNodes.count; i++) {
                
                
                NSIndexPath *path=[NSIndexPath indexPathForRow:i inSection:0];
                [self tableView:self.TableView didDeselectRowAtIndexPath:path];
            }
            FileNum = 0;
        }
    }
        
    
}

#pragma mark - Hidden Or Show FrontOrRearView


-(void)ShowSwitchFrontOrRearView{
    
    self.FrontOrRearView.hidden = NO;
    self.FrontOrRearView.frame = CGRectMake(0,self.fileTypeSwitch.frame.origin.y + self.fileTypeSwitch.frame.size.height + 1, self.view.frame.size.width, 50);
    self.SwitchFrontOrRear.frame = CGRectMake(0, 0, self.FrontOrRearView.frame.size.width, self.FrontOrRearView.frame.size.height);
    
    appDelegate.allowRotation = NO;
    
    self.SwitchFrontOrRear.selectedSegmentIndex = 0;
    self.ImageFVideo.image = FVideoTouchImage;
    self.ImageRVideo.image = RVideoImage;
    
    if (!JudePhoto && self.SwitchFrontOrRear.alpha == 0) {
        
        [UIView animateWithDuration:1.0
                         animations:^{
                             
                             self.SwitchFrontOrRear.alpha = 1;
                             self.FrontOrRearView.alpha = 1;
                             
                             self.TableView.frame = CGRectMake(0, self.FrontOrRearView.frame.origin.y + self.FrontOrRearView.frame.size.height + 20, self.view.frame.size.width, self.view.frame.size.height - (self.FrontOrRearView.frame.size.height + self.fileTypeSwitch.frame.size.height + self.deleteBtn.frame.size.height + 25));
                             
                             
                             
                         }];
    }
    
}

#pragma mark - Communication


-(void)RequestData:(NSInteger)num JudeHUD:(BOOL)JudeHUD{
    
    fetchSize = 0;
 
    if (JudeHUD) {
        JudeLoadOrDown = YES;
        [self LoaingCustomAnimationsView];
    }
    
    switch (num) {
        case 1://普通前路请求和刷新
        {
            
            //普通前路视频
            camera_cmd = CAMERA_CMD_FRONTVIDEO_FETCH_FILES;
            if (fetchFirst) {
                
                fetchSize = 0;
                (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandFirstSDFrontFileUrl] Delegate:(id<AITCameraRequestDelegate>)self];
            }else{
                
                fetchSize = 0;
                (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandSDFrontFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self];
            }
            break;
        }
        case 12://普通后路视频
        {
            camera_cmd = CAMERA_CMD_REARVIDEO_FETCH_FILES;
            fetchSize = 0;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandFirstSDRearFileUrl] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 13://第一次获取SOS前路
        {
            fetchFirst = YES;
            fetchSize = 0;
            camera_cmd = CAMERA_CMD_SOSFRONTVIDEO_FETCH_FILES;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandFirstSDSOSFrontFileUrl] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 14://第一次获取SOS后路
        {
            fetchFirst = YES;
            camera_cmd = CAMERA_CMD_SOSREARVIDEO_FETCH_FILES;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandFirstSDSOSRearFileUrl] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 3://请求图片
        {
            camera_cmd = CAMERA_CMD_NEWIMAGE_FETCH_FILES;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandFirstSDImageFileUrl] Delegate:(id<AITCameraRequestDelegate>)self];
            
            break;
        }
        case 31:
        {
            camera_cmd = CAMERA_CMD_REARNEWIMAGE_FETCH_FILES;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandFirstSDRearImageFileUrl] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 4://3.0版本刷新普通前路视频
        {
            _isLoading = YES;
            camera_cmd = CAMERA_CMD_FRONTVIDEO_FETCH_FILES;
            fetchFirst = NO;
            fetchSize = movFileNodes.count;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandSDFrontFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 41://3.0版本普通后路视频刷新
        {
            _isLoading = YES;
            camera_cmd = CAMERA_CMD_REARVIDEO_FETCH_FILES;
            fetchFirst = NO;
            fetchSize = movRearNodes.count;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandSDRearFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 42://3.0版本图片刷新
        {
            _isLoading = YES;
            fetchFirst = NO;
            camera_cmd = CAMERA_CMD_NEWIMAGE_FETCH_FILES;
            fetchSize = jpegFileNodes.count;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandSDImageFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self];
            
            break;
        }
        case 421:
        {
            _isLoading = YES;
            fetchFirst = NO;
            camera_cmd = CAMERA_CMD_REARNEWIMAGE_FETCH_FILES;
            fetchSize = jpegRearFileNodes.count;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandSDRearImageFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 43://SOS前路视频
        {
            _isLoading = YES;
            fetchFirst = NO;
            fetchSize = movSOSFrontNodes.count;
            camera_cmd = CAMERA_CMD_SOSFRONTVIDEO_FETCH_FILES;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandSDSOSFrontFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 44://SOS后路视频
        {
            _isLoading = YES;
            fetchFirst = NO;
            camera_cmd = CAMERA_CMD_SOSREARVIDEO_FETCH_FILES;
            (void)[[AITCameraCommand alloc]initWithUrl:[AITCameraCommand commandSDSOSRearFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 5://查询固件状态
        {
            camera_cmd = CAMERA_CMD_OPEN_FILE_QUERY_RECORDING;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandQueryCameraStatusUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 51:
        {
            camera_cmd = CAMERA_CMD_DOWNLOAD_FILE_QUERY_RECORDING;
            NSURL* url = [AITCameraCommand commandQueryCameraStatusUrl];
            (void)[[AITCameraCommand alloc] initWithUrl:url Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 6://031 开启暂停录像
        {
            camera_cmd = CAMERA_CMD_RECORD;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self];
            break;
        }
        case 7:
        {
            if (!openAfterDownload) {
                camera_cmd = CAMERA_CMD_QURY_RECORDING_VIEWDISAPEARED;
                NSURL* url = [AITCameraCommand commandQueryCameraStatusUrl];
                (void)[[AITCameraCommand alloc] initWithUrl:url Delegate:(id<AITCameraRequestDelegate>)self];
            }
            
            break;
        }
        case 8://048 开启录像
        {
            camera_cmd = CAMERA_CMD_RECORD_STRAT;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStartRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 9://暂停录像
        {
            camera_cmd = CAMERA_CMD_RECORD_STOP;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStopRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        default:
            break;
    }
    
}

-(void) requestFinished:(NSString*) result{
    
    BOOL JudeIFContinueDownload = NO;
    BOOL JudeDataIsNull = NO;
    NSValue *centerValue = [NSValue valueWithCGPoint:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height - 75)];
    switch (camera_cmd) {
        case CAMERA_CMD_FETCH_FILES:
        {
            
            if (result && [result rangeOfString:@"OK"].location == NSNotFound && result.length > 0)
            {
                if (fetchFirst)
                {
                    [movFileNodes removeAllObjects];
                    [movRearNodes removeAllObjects];
                    [jpegFileNodes removeAllObjects];
                    [SelectFileName removeAllObjects];
                    [self.TableView reloadData];
                    fetchFirst = NO ;
                }
                
                GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:[result substringToIndex:[result rangeOfString:@">" options:NSBackwardsSearch].location + 1] options:0 error:nil];
                
                GDataXMLElement *dcimElement = doc.rootElement ;

                
                if ([result rangeOfString:@"Not Found"].location != NSNotFound || dcimElement == NULL)
                {
                                        [self CustomAnimationsSuccessImage];
                    [self.TableView reloadData];
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Network error,some data con't be get", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Network error,some data con't be get", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                    _isLoading = NO;
                    return;
                }
               
                amount = 0 ;
                
                NSArray *dcimChildren = [dcimElement children] ;
                //根元素里面的子元素<file>
                
                for (GDataXMLElement *dcimChild in dcimChildren){
                    

                    @try
                    {
                        
                        if ([[dcimChild name] isEqualToString:TAG_file])
                        {
                            
                            
                            if([[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"mov"] == NSOrderedSame || [[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"MP4"] == NSOrderedSame){
                                fileNodes = movFileNodes;
                            }else{
                                fileNodes = jpegFileNodes;
                            }
                            
                            
                            //if(index == [fileNodes count]) {
                            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
                            
                            fileNode.name = [[self getFirstChild:dcimChild WithName:TAG_name] stringValue] ;
                            fileNode.format = [[self getFirstChild:dcimChild WithName:TAG_format] stringValue] ;
                            fileNode.size = (int)[[[self getFirstChild:dcimChild WithName:TAG_size] stringValue] integerValue];
                            fileNode.attr = [[self getFirstChild:dcimChild WithName:TAG_attr] stringValue] ;
                            fileNode.time = [[[self getFirstChild:dcimChild WithName:TAG_time] stringValue] substringWithRange:NSMakeRange(0, 16)] ;
                            
                            /* Append time to the file if time of the file is null */
                            if(fileNode.time == nil)
                                fileNode.time = @"1970-01-01 00:00";
                            
                            fileNode.blValid = TRUE;
                            
                            [fileNodes addObject: fileNode] ;
                            
                    } else if ([[dcimChild name] isEqualToString:TAG_amount])
                        {
                            amount = [[dcimChild stringValue] intValue] ;
                            if (amount == 0) {
                                JudeDataIsNull = YES;
                            }

                        }
                    }
                    
                    @catch (NSException *exception)
                    {

                    }
                }
                
                if(self.fileTypeSwitch.selectedSegmentIndex == 0){
                    fileNodes = movFileNodes;
                }else{
                    fileNodes = jpegFileNodes;
                }
                
                fileNodes = [[fileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                
                if (fileNodes.count < 5  && amount != 0)
                {
                   
                    _isLoading = YES;
                    JudeIFContinueDownload = YES;
                    camera_cmd = CAMERA_CMD_FETCH_FILES;
                    (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandListFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self] ;
                    
                }else{
                    /* Update table view */
                    for (AITFileNode *fileNode in fileNodes)
                    {
                        if (fileNode.blSelected)
                        {
                            fileNode.blSelected = NO;
                        }
                    }
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                        
                        
                    }
                    
                    [AITFileCell stopThumbnailFetching] ;
                    [AITFileCell startThumbnailFetching] ;
                    
                }
                
                
                [self.TableView reloadData];
                if (amount != fetchSize && dcimElement != NULL){
                   
                    fetchStopped = YES;
                    
                    if (zhSP) {
                        [self.view makeToast:NSLocalizedString(@"Data loading is completed", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Data loading is completed", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                    
                    
                }else if(amount == fetchSize){
               
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Drag up to refresh", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Drag up to refresh", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                }
            } else{
                
                if ([result rangeOfString:@"OK"].location == NSNotFound) {
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                    
                    fetchStopped = YES;
                }
                
            }
            _isLoading = NO;
            
            self.navigationItem.leftBarButtonItem.enabled = YES;
            
            break;
        }
        case CAMERA_CMD_DELETE_FILES:
        {    //Get file deletion response
            if(result && ([result rangeOfString:@"OK"].location != NSNotFound)){
                fileDelCount--;
                
                if(fileDelCount == 0)
                {
                    /* Fetch the file list */
                    fetchStopped = NO;
                    fetchFirst = YES ;
                    camera_cmd = CAMERA_CMD_FETCH_FILES;
                    (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandListFirstFileUrl:fetchSize] Delegate:(id<AITCameraRequestDelegate>)self] ;
                }
            }else{
                
                if (zhSP) {
                    [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:centerValue];
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                    
                }
                
            }
            break;
        }
        case CAMERA_CMD_OPEN_FILE_QUERY_RECORDING:
        {
            if(result){
                /* Check recording status */
                NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
                
                if(range.location != NSNotFound)
                {
                    NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    if([camearRecordStatus rangeOfString:@"Recording"].location != NSNotFound)
                    {
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Recording will be paused before opening the file", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Open", nil), nil] ;
                        
                        if (!zhSP) {
                            
                            av = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Recording will be paused before opening the file", @"DefLocalizable", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil) otherButtonTitles:NSLocalizedStringFromTable(@"Open", @"DefLocalizable", nil), nil] ;
                            
                            
                        }
                        [av setTag:1];
                        [av show];
                    }else{
                        
                        [self openFile];
                    }
                }
            }
            break;
        }
        case CAMERA_CMD_DOWNLOAD_FILE_QUERY_RECORDING:
        {
            if(result){
                /* Check recording status */
                NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
                
                if(range.location != NSNotFound)
                {
                    NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    if([camearRecordStatus rangeOfString:@"Recording"].location != NSNotFound){
                        
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Recording will be paused before downloading the file", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Download", nil), nil] ;
                        if (!zhSP) {
                            
                            av = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Recording will be paused before downloading the file", @"DefLocalizable", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil) otherButtonTitles:NSLocalizedStringFromTable(@"Download", @"DefLocalizable", nil), nil] ;
                            
                        }
                        
                        
                        [av setTag:2];
                        [av show];
                    } else{

                        [self downloadFile];
                    }
                }
            }else{
                
                
            }
            
            break;
        }
        case CAMERA_CMD_RECORD:
        {
            if([result rangeOfString:@"OK"].location != NSNotFound)
            {
              
            }
            break;
        }
        case CAMERA_CMD_QURY_RECORDING_VIEWDISAPEARED:
        {
            /* Check recording status */
            NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
            
            if(range.location != NSNotFound)
            {
                NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                if([camearRecordStatus rangeOfString:@"Recording"].location != NSNotFound)
                {
                   
                    recPaused = NO;
                    
                } else{
                    
                    recPaused = YES;
                    
                    /* Resume recording if it is pasued for downloading file */
                    if(recPaused)
                    {
                        recPaused = NO;
                        
                        [self RequestData:8 JudeHUD:NO];

                    }
                }
            }
            break;
        }
            //Add Ted 3.0代码
        case CAMERA_CMD_FRONTVIDEO_FETCH_FILES:
        {
            if (result && [result rangeOfString:@"OK"].location == NSNotFound && result.length > 0)
            {
                
                if (fetchFirst){
                    [movFileNodes removeAllObjects];
                    [SelectFileName removeAllObjects];
                    [self.TableView reloadData];
                }
                
                GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:[result substringToIndex:[result rangeOfString:@">" options:NSBackwardsSearch].location + 1] options:0 error:nil];
                
                GDataXMLElement *dcimElement = doc.rootElement ;
                
                if ([result rangeOfString:@"Not Found"].location != NSNotFound || dcimElement == NULL){
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Network error,some data con't be get", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Network error,some data con't be get", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                    }
                    
                    _isLoading = NO;
                    
                    [self.TableView reloadData];
                    return;
                }
                
                amount = 0 ;
                
                
                NSArray *dcimChildren = [dcimElement children] ;
                //根元素里面的子元素<file>
                
                for (GDataXMLElement *dcimChild in dcimChildren){
                    
                    //dcimChild <file>的子元素
                    //dcimChildren <file>元素
                    @try
                    {
                        
                        if ([[dcimChild name] isEqualToString:TAG_file])
                        {
                            
                            if([[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"mov"] == NSOrderedSame || [[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"MP4"] == NSOrderedSame)
                                fileNodes = movFileNodes;
                            
                            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
                            
                            fileNode.name = [[self getFirstChild:dcimChild WithName:TAG_name] stringValue] ;
                            fileNode.format = [[self getFirstChild:dcimChild WithName:TAG_format] stringValue] ;
                            fileNode.size = (int)[[[self getFirstChild:dcimChild WithName:TAG_size] stringValue] integerValue];
                            fileNode.attr = [[self getFirstChild:dcimChild WithName:TAG_attr] stringValue] ;
                            fileNode.time = [[[self getFirstChild:dcimChild WithName:TAG_time] stringValue] substringWithRange:NSMakeRange(0, 16)] ;                                /* Append time to the file if time of the file is null */
                            if(fileNode.time == nil)
                                fileNode.time = @"1970-01-01 00:00";
                            
                            fileNode.blValid = TRUE;
                            
                            [fileNodes addObject: fileNode] ;
                            

                            
                        } else if ([[dcimChild name] isEqualToString:TAG_amount])
                        {
                            amount = [[dcimChild stringValue] intValue] ;
                            if (amount == 0) {
                                JudeDataIsNull = YES;
                            }

                        }
                    }
                    
                    @catch (NSException *exception){
                        
                        if (amount == 0) {
                            JudeDataIsNull = YES;
                        }

                    }
                }
                fileNodes = movFileNodes;
                fileNodes = [[fileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                
                if (fileNodes.count > 0 && amount != 0)
                {
                    
                    if (fetchFirst && fileNodes.count < 16  && amount < 16) {
                        
                        fetchFirst = NO ;
                        JudeDataIsNull = YES;
                    }
                    /* Update table view */
                    
                    for (AITFileNode *fileNode in fileNodes)
                    {
                        if (fileNode.blSelected)
                        {
                            fileNode.blSelected = YES;
                        }else{
                            fileNode.blSelected = NO;
                        }
                    }
                    
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        
                        AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
                        
                        if (file.blSelected) {
                            
                            cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];

                            [self.TableView deselectRowAtIndexPath:p animated:NO];
                            
                        }else{
                            
                            cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                            [self.TableView deselectRowAtIndexPath:p animated:YES];
                        }
                        
                        
                    }
                    
                    [AITFileCell stopThumbnailFetching] ;
                    [AITFileCell startThumbnailFetching] ;
                    
                }
                
                
                [self.TableView reloadData];
                
            } else{
                
                if ([result rangeOfString:@"OK"].location == NSNotFound) {
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                    
                    fetchStopped = YES;
                    
                }
            }
            _isLoading = NO;
            
            self.navigationItem.leftBarButtonItem.enabled = YES;
            break;
        }
        case CAMERA_CMD_REARVIDEO_FETCH_FILES:
        {
            if (result && [result rangeOfString:@"OK"].location == NSNotFound && result.length > 0){
                
             
                
                if (fetchFirst){
                    [movRearNodes removeAllObjects];
                    [SelectFileName removeAllObjects];
                    [self.TableView reloadData];
                    
                }
                
                GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:[result substringToIndex:[result rangeOfString:@">" options:NSBackwardsSearch].location + 1] options:0 error:nil];
                
                GDataXMLElement *dcimElement = doc.rootElement ;
                
                if ([result rangeOfString:@"Not Found"].location != NSNotFound || dcimElement == NULL){
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Network error,some data con't be get", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Network error,some data con't be get", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                    [self CustomAnimationsSuccessImage];
                    [self.TableView reloadData];
                    _isLoading = NO;
                    return;
                }
                
           
                amount = 0 ;
                
                
                NSArray *dcimChildren = [dcimElement children] ;
                //根元素里面的子元素<file>
                
                for (GDataXMLElement *dcimChild in dcimChildren){
                    
                    //dcimChild <file>的子元素
                    //dcimChildren <file>元素
                    @try
                    {
                        
                        if ([[dcimChild name] isEqualToString:TAG_file])
                        {
                            
                            if([[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"mov"] == NSOrderedSame || [[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"MP4"] == NSOrderedSame)
                                fileNodes = movRearNodes;
                            
                            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
                            
                            fileNode.name = [[self getFirstChild:dcimChild WithName:TAG_name] stringValue] ;
                            fileNode.format = [[self getFirstChild:dcimChild WithName:TAG_format] stringValue] ;
                            fileNode.size = (int)[[[self getFirstChild:dcimChild WithName:TAG_size] stringValue] integerValue];
                            fileNode.attr = [[self getFirstChild:dcimChild WithName:TAG_attr] stringValue] ;
                            fileNode.time = [[[self getFirstChild:dcimChild WithName:TAG_time] stringValue] substringWithRange:NSMakeRange(0, 16)] ;                                /* Append time to the file if time of the file is null */
                            if(fileNode.time == nil)
                                fileNode.time = @"1970-01-01 00:00";
                            
                            fileNode.blValid = TRUE;
                            
                            [fileNodes addObject: fileNode] ;
                            
                            
                        } else if ([[dcimChild name] isEqualToString:TAG_amount])
                        {
                            
                            amount = [[dcimChild stringValue] intValue] ;
                            if (amount == 0) {
                                JudeDataIsNull = YES;
                            }

                        }
                    }
                    
                    @catch (NSException *exception){
                        
                        if (amount == 0) {
                            JudeDataIsNull = YES;
                        }

                    }
                }
                
                fileNodes = movRearNodes;
                fileNodes = [[fileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                
                if (fileNodes.count > 0 && amount != 0)
                {
                    if (fetchFirst && fileNodes.count < 16  && amount < 16) {
                        
                        fetchFirst = NO ;
                        JudeDataIsNull = YES;
                    }                    /* Update table view */
                    for (AITFileNode *fileNode in fileNodes)
                    {
                        if (fileNode.blSelected)
                        {
                            fileNode.blSelected = YES;
                        }else{
                            fileNode.blSelected = NO;
                        }
                        
                    }
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        
                        AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
                        
                        if (file.blSelected) {
                            
                            file.blSelected = YES;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
                            
                        }else{
                            
                            file.blSelected = NO;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                        }
                        
                    }
                    
                    [AITFileCell stopThumbnailFetching] ;
                    [AITFileCell startThumbnailFetching] ;
                    
                }
                [self.TableView reloadData];
                
            }else{
                
                if ([result rangeOfString:@"OK"].location == NSNotFound) {
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                    fetchStopped = YES;
                }
                
            }
            
            _isLoading = NO;
            self.navigationItem.leftBarButtonItem.enabled = YES;
            
            break;
        }
        case CAMERA_CMD_NEWIMAGE_FETCH_FILES:
        {
            if (result && [result rangeOfString:@"OK"].location == NSNotFound && result.length > 0){
                
                if (fetchFirst){
                    
                    [SelectFileName removeAllObjects];
                    [jpegFileNodes removeAllObjects];
                    [self.TableView reloadData];
                    
                }
                
                GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:[result substringToIndex:[result rangeOfString:@">" options:NSBackwardsSearch].location + 1] options:0 error:nil];
                
                GDataXMLElement *dcimElement = doc.rootElement ;
                
                if ([result rangeOfString:@"Not Found"].location != NSNotFound || dcimElement == NULL){
                    
                    [self CustomAnimationsSuccessImage];
                    [self.TableView reloadData];
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Network error,some data con't be get", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Network error,some data con't be get", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                    _isLoading = NO;
                    return;
                }
                
          
                amount = 0 ;
                
                
                NSArray *dcimChildren = [dcimElement children] ;
                //根元素里面的子元素<file>
                
                for (GDataXMLElement *dcimChild in dcimChildren)
                {
                    
                    @try
                    {
                        
                        if ([[dcimChild name] isEqualToString:TAG_file])
                        {
                            
                            fileNodes = jpegFileNodes;
                            
                            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
                            
                            fileNode.name = [[self getFirstChild:dcimChild WithName:TAG_name] stringValue] ;
                            fileNode.format = [[self getFirstChild:dcimChild WithName:TAG_format] stringValue] ;
                            fileNode.size = (int)[[[self getFirstChild:dcimChild WithName:TAG_size] stringValue] integerValue];
                            fileNode.attr = [[self getFirstChild:dcimChild WithName:TAG_attr] stringValue] ;
                            fileNode.time = [[[self getFirstChild:dcimChild WithName:TAG_time] stringValue] substringWithRange:NSMakeRange(0, 16)] ;                                /* Append time to the file if time of the file is null */
                            if(fileNode.time == nil)
                                fileNode.time = @"1970-01-01 00:00";
                            
                            fileNode.blValid = TRUE;
                            
                            [fileNodes addObject: fileNode] ;

                            
                        } else if ([[dcimChild name] isEqualToString:TAG_amount])
                        {
                            amount = [[dcimChild stringValue] intValue] ;
                            if (amount == 0) {
                                JudeDataIsNull = YES;
                            }

                        }
                    }
                    
                    @catch (NSException *exception){
                        
                        if (amount == 0) {
                            JudeDataIsNull = YES;
                        }

                    }
                }
                
                fileNodes = jpegFileNodes;
                
                fileNodes = [[fileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                
                if (fileNodes.count > 0 && amount != 0){
                    
                    if (fetchFirst && fileNodes.count < 16  && amount < 16) {
                        
                        fetchFirst = NO ;
                        JudeDataIsNull = YES;
                    }
                    /* Update table view */
                    for (AITFileNode *fileNode in fileNodes)
                    {
                        if (fileNode.blSelected)
                        {
                            fileNode.blSelected = YES;
                        }else{
                            fileNode.blSelected = NO;
                        }
                        
                    }
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        
                        AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
                        
                        if (file.blSelected) {
                            
                            file.blSelected = YES;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
                            
                            
                        }else{
                            
                            file.blSelected = NO;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                            
                        }
                        
                    }
                    
                    [AITFileCell stopThumbnailFetching] ;
                    [AITFileCell startThumbnailFetching] ;
                    
                }
                
                [self.TableView reloadData];
                
            }else{
                
                if ([result rangeOfString:@"OK"].location == NSNotFound) {
                    
                    if (zhSP) {
                        [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                    fetchStopped = YES;
                }
                
            }
            _isLoading = NO;
            self.navigationItem.leftBarButtonItem.enabled = YES;
            
            break;
        }
        case CAMERA_CMD_REARNEWIMAGE_FETCH_FILES:
        {
            if (result && [result rangeOfString:@"OK"].location == NSNotFound && result.length > 0){
                
                if (fetchFirst){
                    
                    [SelectFileName removeAllObjects];
                    [jpegRearFileNodes removeAllObjects];
                    [self.TableView reloadData];
                    
                }
                
                GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:[result substringToIndex:[result rangeOfString:@">" options:NSBackwardsSearch].location + 1] options:0 error:nil];
                
                GDataXMLElement *dcimElement = doc.rootElement ;
                
                if ([result rangeOfString:@"Not Found"].location != NSNotFound || dcimElement == NULL){
                    
                    [self CustomAnimationsSuccessImage];
                    [self.TableView reloadData];
                    
                    if (zhSP) {
                        [self.view makeToast:NSLocalizedString(@"Network error,some data con't be get", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Network error,some data con't be get", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                    _isLoading = NO;
                    return;
                }
                
                
                amount = 0 ;
                
                
                NSArray *dcimChildren = [dcimElement children] ;
                //根元素里面的子元素<file>
                
                for (GDataXMLElement *dcimChild in dcimChildren)
                {
                    
                    //dcimChild <file>的子元素
                    //dcimChildren <file>元素
                    @try
                    {
                        
                        if ([[dcimChild name] isEqualToString:TAG_file])
                        {
                            
                            fileNodes = jpegRearFileNodes;
                            
                            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
                            
                            fileNode.name = [[self getFirstChild:dcimChild WithName:TAG_name] stringValue] ;
                            fileNode.format = [[self getFirstChild:dcimChild WithName:TAG_format] stringValue] ;
                            fileNode.size = (int)[[[self getFirstChild:dcimChild WithName:TAG_size] stringValue] integerValue];
                            fileNode.attr = [[self getFirstChild:dcimChild WithName:TAG_attr] stringValue] ;
                            fileNode.time = [[[self getFirstChild:dcimChild WithName:TAG_time] stringValue] substringWithRange:NSMakeRange(0, 16)] ;                                /* Append time to the file if time of the file is null */
                            if(fileNode.time == nil)
                                fileNode.time = @"1970-01-01 00:00";
                            
                            fileNode.blValid = TRUE;
                            
                            [fileNodes addObject: fileNode] ;

                            
                        } else if ([[dcimChild name] isEqualToString:TAG_amount])
                        {
                            amount = [[dcimChild stringValue] intValue] ;
                            if (amount == 0) {
                                JudeDataIsNull = YES;
                            }

                        }
                    }
                    
                    @catch (NSException *exception){
                        
                        if (amount == 0) {
                            JudeDataIsNull = YES;
                        }

                    }
                }
                
                fileNodes = jpegRearFileNodes;
                
                fileNodes = [[fileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                
                if (fileNodes.count > 0 && amount != 0){
                    
                    if (fetchFirst && fileNodes.count < 16  && amount < 16) {
                        
                        fetchFirst = NO ;
                        JudeDataIsNull = YES;
                    }
                    /* Update table view */
                    for (AITFileNode *fileNode in fileNodes)
                    {
                        if (fileNode.blSelected)
                        {
                            fileNode.blSelected = YES;
                        }else{
                            fileNode.blSelected = NO;
                        }
                        
                    }
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        
                        AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
                        
                        if (file.blSelected) {
                            
                            file.blSelected = YES;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
                            
                            
                        }else{
                            
                            file.blSelected = NO;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                            
                        }
                        
                    }
                    
                    [AITFileCell stopThumbnailFetching] ;
                    [AITFileCell startThumbnailFetching] ;
                    
                }
                
                [self.TableView reloadData];
                
            }else{
                
                if ([result rangeOfString:@"OK"].location == NSNotFound) {
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                    fetchStopped = YES;
                }
                
            }
            _isLoading = NO;
            self.navigationItem.leftBarButtonItem.enabled = YES;
            break;
        }
        case CAMERA_CMD_SOSFRONTVIDEO_FETCH_FILES:
        {
            if (result && [result rangeOfString:@"OK"].location == NSNotFound && result.length > 0){
                
                
                if (fetchFirst){
                    
                    [SelectFileName removeAllObjects];
                    [movSOSFrontNodes removeAllObjects];
                    [self.TableView reloadData];
                }
                
                GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:[result substringToIndex:[result rangeOfString:@">" options:NSBackwardsSearch].location + 1] options:0 error:nil];
                
                GDataXMLElement *dcimElement = doc.rootElement ;
                
                if ([result rangeOfString:@"Not Found"].location != NSNotFound || dcimElement == NULL){
                    
                    if (zhSP) {
                        [self.view makeToast:NSLocalizedString(@"Network error,some data con't be get", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Network error,some data con't be get", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                    
                    _isLoading = NO;
                    [self CustomAnimationsSuccessImage];
                    [self.TableView reloadData];
                    return;
                }
                
                amount = 0 ;
                
                
                NSArray *dcimChildren = [dcimElement children] ;
                //根元素里面的子元素<file>
                
                for (GDataXMLElement *dcimChild in dcimChildren){
                    
                    //dcimChild <file>的子元素
                    //dcimChildren <file>元素
                    @try
                    {
                        
                        if ([[dcimChild name] isEqualToString:TAG_file])
                        {
                            
                            if([[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"mov"] == NSOrderedSame || [[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"MP4"] == NSOrderedSame)
                                fileNodes = movSOSFrontNodes;
                            
                            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
                            
                            fileNode.name = [[self getFirstChild:dcimChild WithName:TAG_name] stringValue] ;
                            fileNode.format = [[self getFirstChild:dcimChild WithName:TAG_format] stringValue] ;
                            fileNode.size = (int)[[[self getFirstChild:dcimChild WithName:TAG_size] stringValue] integerValue];
                            fileNode.attr = [[self getFirstChild:dcimChild WithName:TAG_attr] stringValue] ;
                            fileNode.time = [[[self getFirstChild:dcimChild WithName:TAG_time] stringValue] substringWithRange:NSMakeRange(0, 16)] ;                                /* Append time to the file if time of the file is null */
                            if(fileNode.time == nil)
                                fileNode.time = @"1970-01-01 00:00";
                            
                            fileNode.blValid = TRUE;
                            
                            [fileNodes addObject: fileNode] ;
                            
                            
                        } else if ([[dcimChild name] isEqualToString:TAG_amount])
                        {
                            amount = [[dcimChild stringValue] intValue] ;
                            if (amount == 0) {
                                JudeDataIsNull = YES;
                            }

                        }
                    }
                    
                    @catch (NSException *exception){
                        
                        if (amount == 0) {
                            JudeDataIsNull = YES;
                        }

                    }
                }
                
                fileNodes = movSOSFrontNodes;
                
                fileNodes = [[fileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                
                if (fileNodes.count > 0 && amount != 0)
                {
                    if (fetchFirst && fileNodes.count < 16  && amount < 16) {
                        
                        fetchFirst = NO ;
                        JudeDataIsNull = YES;
                    }
                    /* Update table view */
                    for (AITFileNode *fileNode in fileNodes)
                    {
                        if (fileNode.blSelected)
                        {
                            fileNode.blSelected = YES;
                        }else{
                            fileNode.blSelected = NO;
                        }
                        
                    }
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        
                        AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
                        
                        if (file.blSelected) {
                            
                            file.blSelected = YES;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
                            
                        }else{
                            
                            file.blSelected = NO;
                            cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                        
                        }
                        
                    }
                    
                    [AITFileCell stopThumbnailFetching] ;
                    [AITFileCell startThumbnailFetching] ;
                    
                }
                [self.TableView reloadData];
                
            } else{
                
                if ([result rangeOfString:@"OK"].location == NSNotFound) {
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                    fetchStopped = YES;
                }
                
            }
            _isLoading = NO;
            
            self.navigationItem.leftBarButtonItem.enabled = YES;
            break;
        }
        case CAMERA_CMD_SOSREARVIDEO_FETCH_FILES:
        {
            if (result && [result rangeOfString:@"OK"].location == NSNotFound && result.length > 0){
                
                if (fetchFirst){
                    [SelectFileName removeAllObjects];
                    [movSOSRearNodes removeAllObjects];
                    [self.TableView reloadData];
                    
                }
                
                GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:[result substringToIndex:[result rangeOfString:@">" options:NSBackwardsSearch].location + 1] options:0 error:nil];
                
                GDataXMLElement *dcimElement = doc.rootElement ;
                
                if ([result rangeOfString:@"Not Found"].location != NSNotFound || dcimElement == NULL){
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Network error,some data con't be get", nil) duration:1.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Network error,some data con't be get", @"DefLocalizable", nil) duration:1.5 position:centerValue];
                        
                    }
                    
                    _isLoading = NO;
                    [self CustomAnimationsSuccessImage];
                    [self.TableView reloadData];
                    return;
                }
                
                amount = 0 ;
                
                
                NSArray *dcimChildren = [dcimElement children] ;
                //根元素里面的子元素<file>
                
                for (GDataXMLElement *dcimChild in dcimChildren){
                    
                    //dcimChild <file>的子元素
                    //dcimChildren <file>元素
                    @try
                    {

                        if ([[dcimChild name] isEqualToString:TAG_file])
                        {
                            
                            if([[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"mov"] == NSOrderedSame || [[[self getFirstChild:dcimChild WithName:TAG_format] stringValue] caseInsensitiveCompare:@"MP4"] == NSOrderedSame)
                                fileNodes = movSOSRearNodes;
                            
                            AITFileNode *fileNode = [[AITFileNode alloc] init] ;
                            
                            fileNode.name = [[self getFirstChild:dcimChild WithName:TAG_name] stringValue] ;
                            fileNode.format = [[self getFirstChild:dcimChild WithName:TAG_format] stringValue] ;
                            fileNode.size = (int)[[[self getFirstChild:dcimChild WithName:TAG_size] stringValue] integerValue];
                            fileNode.attr = [[self getFirstChild:dcimChild WithName:TAG_attr] stringValue] ;
                            fileNode.time = [[[self getFirstChild:dcimChild WithName:TAG_time] stringValue] substringWithRange:NSMakeRange(0, 16)] ;                                /* Append time to the file if time of the file is null */
                            if(fileNode.time == nil)
                                fileNode.time = @"1970-01-01 00:00";
                            
                            fileNode.blValid = TRUE;
                            
                            [fileNodes addObject: fileNode] ;
                            
                            
                        } else if ([[dcimChild name] isEqualToString:TAG_amount])
                        {
                            amount = [[dcimChild stringValue] intValue] ;
                            if (amount == 0) {
                                JudeDataIsNull = YES;
                            }

                        }
                    }
                    
                    @catch (NSException *exception){
                        
                        if (amount == 0) {
                            JudeDataIsNull = YES;
                        }

                    }
                }
                
                fileNodes = movSOSRearNodes;
                fileNodes = [[fileNodes sortedArrayUsingSelector:@selector(compare:)] mutableCopy];
                
                if (fileNodes.count > 0 && amount != 0)
                {
                    if (fetchFirst && fileNodes.count < 16  && amount < 16) {
                        
                        fetchFirst = NO ;
                        JudeDataIsNull = YES;
                    }
                    /* Update table view */
                    for (AITFileNode *fileNode in fileNodes)
                    {
                        if (fileNode.blSelected)
                        {
                            fileNode.blSelected = YES;
                        }else{
                            fileNode.blSelected = NO;
                        }
                        
                    }
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        
                        AITFileNode *file = [fileNodes objectAtIndex:p.row] ;
                        
                        if (file.blSelected) {
                            
                            file.blSelected = YES;
                             cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
                            
                            
                        }else{
                            
                            file.blSelected = NO;
                             cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                            
                        }
                        
                    }
                    
                    [AITFileCell stopThumbnailFetching] ;
                    [AITFileCell startThumbnailFetching] ;
                    
                }
                [self.TableView reloadData];
                
            }else{
                
                if ([result rangeOfString:@"OK"].location == NSNotFound) {
                    
                    if (zhSP) {
                        
                        [self.view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:centerValue];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Cannot connect to the camera!", @"DefLocalizable", nil) duration:0.5 position:centerValue];
                        
                    }
                    
                    fetchStopped = YES;
                }
                
            }
            _isLoading = NO;
            
            self.navigationItem.leftBarButtonItem.enabled = YES;
            break;
            
        }
        case CAMERA_CMD_RECORD_STOP:
        {
            if([result rangeOfString:@"OK"].location != NSNotFound)
            {

            }
            break;
        }
        case CAMERA_CMD_RECORD_STRAT:
        {
            if([result rangeOfString:@"OK"].location != NSNotFound)
            {

            }
            break;
        }
        
        default:
            break;
    }
    
    [self.TableView reloadData];
    if (AnimationsView != nil && JudeLoadOrDown) {
        
        if (!JudeIFContinueDownload) {
            
            [self CustomAnimationsSuccessImage];
        }
        
    }
    
    //判断加载数据是否为空
    if (JudeDataIsNull) {
        
        [self.TableView.mj_footer endRefreshingWithNoMoreData];
        
    }else{
        
        [self.TableView.mj_footer endRefreshing];
    }
    
    CountNum = 60;
}

#pragma mark - alertView

-(void)ShowAlaertView:(NSString *)Message{
    
    UIAlertView *showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
    if (!zhSP) {
        
        showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
    }
    [showAlert show];
}

-(void)ShowAlaertView:(NSString *)Message Tag:(NSInteger)tag{
    
    UIAlertView *showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:Message delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
    if (!zhSP) {
        
        showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:Message delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil) otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
        
    }
    if (tag != 0) {
        showAlert.tag = tag;
        if (tag == 444) {
            
            
            if (zhSP) {
                showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Info", nil) message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Confim", nil), nil];
            }else{
                
                showAlert = [[UIAlertView alloc]initWithTitle:NSLocalizedStringFromTable(@"Info", @"DefLocalizable", nil) message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedStringFromTable(@"Confim", @"DefLocalizable", nil), nil];
                
            }
            showAlert.tag = tag;
        }
    }
    
    [showAlert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 1715) {
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else if (alertView.tag == 12121){
        
        if (buttonIndex == 1) {
            
            [self stopDownload];
            if (OpenImageArray.count > 0) {
                [self DeleteOpenImage];
            }
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }else if (alertView.tag == 1313){
        
        if (buttonIndex == 1) {
            
            JudeISDel = YES;
            [self LoaingCustomAnimationsView];
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(SDViewDeleteDate) userInfo:nil repeats:NO];
            
        }else{
            [self TableViewSelectReloadData];
        }
        
    }else if (alertView.tag == 444){
      
        return;
    }else{
        
        switch (buttonIndex){
            case 0:
            {
                JudeLoadOrDown = YES;
                JudeISDown = NO;
                [self TableViewSelectReloadData];
                self.TableView.userInteractionEnabled = YES;
                if (AnimationsView) {
                    [self CustomAnimationsSuccessImage];
                }
                
            }
                break;
            case 1:
            {
                recPaused = YES;
                [self RequestData:9 JudeHUD:NO];
                
                /* Opening file */
                if([alertView tag] == 1)
                {
                    [self openFile];
                    
                    /* Deselect the file and uncheck the check box */
                    AITFileCell *cell = nil ;
                    AITFileNode *fileNode = nil ;
                    
                    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
                    {
                        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
                        [self.TableView deselectRowAtIndexPath:p animated:YES];
                        
                        cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
                        fileNode = [fileNodes objectAtIndex:p.row] ;
                        fileNode.blSelected = NO;
                        cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                        
                     
                    }
                } else if([alertView tag] == 2)// Downloading file
                {
                    
                    [self downloadFile];
                }
            }
                break;
            default:
                break;
        }
        
    }
    
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    NSInteger tem = 0;
    
    tem = fileNodes.count;
    return tem;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CountNum = 60;
    AITFileCell *cell = [tableView dequeueReusableCellWithIdentifier:[AITFileCell reuseIdentifier]];
    
    if (cell == nil) {
        cell = [[AITFileCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[AITFileCell reuseIdentifier]];
    }
    
    
    
    AITFileNode *file = [fileNodes objectAtIndex:indexPath.row] ;
    
    if (file == nil)
    {
        return cell;
    }
    
    if(file.blSelected){
        
        cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
    }else{
        
        cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
    }
    
    
    cell->thumbnailFileName = file.name;
    
    if(fileNodes == movFileNodes){
        
        cell.fileName.text = [file.name stringByReplacingOccurrencesOfString:@".MOV" withString:@""] ;
        
    }else{
        
        cell.fileName.text = [file.name stringByReplacingOccurrencesOfString:@".jpg" withString:@""] ;
        cell.fileName.text = [cell.fileName.text stringByReplacingOccurrencesOfString:@".JPG" withString:@""] ;
    }
    
    cell.fileName.text = [cell.fileName.text substringFromIndex:[file.name rangeOfString:@"/" options:NSBackwardsSearch].location + 1] ;
    
    double fileSize = file.size ;
    
    NSString *sizeString = @"0" ;
    
    if (fileSize < 1024){
        
        sizeString = [NSString stringWithFormat:@"%.1f", fileSize] ;
    }else{
        fileSize /= 1024 ;
        if (fileSize < 1024)
        {
            
            sizeString = [NSString stringWithFormat:@"%.1fK", fileSize] ;
        } else
        {
            fileSize /= 1024 ;
            if (fileSize < 1024)
            {
                
                sizeString = [NSString stringWithFormat:@"%.1fM", fileSize] ;
            } else
            {
                fileSize /= 1024 ;
                if (fileSize < 1024)
                {
                    
                    sizeString = [NSString stringWithFormat:@"%.1fG", fileSize] ;
                } else
                {
                    
                }
            }
        }
    }
    
    cell.fileSize.text = sizeString ;
    
    cell.fileDate.text = [file.time stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
    
    // Configure the cell...
    //cell.fileDate.text = file.time ;
    
    cell.filePath = [NSURL URLWithString: [NSString stringWithFormat:@"http://%@%@", [AITUtil getCameraAddress], [file.name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] ;
    
    if (self.fileTypeSwitch.selectedSegmentIndex == 0 || self.fileTypeSwitch.selectedSegmentIndex == 1) {
        
        cell.ImageLayout.constant = 10;
        cell.fileIcon.image = [UIImage imageNamed:@"logo.png"];
        
    }else{
        
        if (self.fileTypeSwitch.selectedSegmentIndex == 0) {
            
            cell.ImageLayout.constant = 15;
            cell.fileIcon.image = [UIImage imageNamed:@"logo.png"];
            
        }else{
            
            cell.ImageLayout.constant = 10;
            cell.fileIcon.image = [UIImage imageNamed:@"logoImage.png"];
        }
        
    }
    
    // Setup progress bar for each cell
    if(cell->dlProgress == nil){
        cell->dlProgress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
        cell->dlProgress.frame = CGRectMake(cell.ImageLayout.constant + 2, cell.frame.size.height-7, 60, 9);
        cell->dlProgress.tag =indexPath.row*10+1;
        cell->dlProgress.progress = 0.0;
        CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 3.0f);
         cell->dlProgress.transform = transform;
        [cell.contentView addSubview:cell->dlProgress];
        
    }
    
    if(file->downloader){
        [cell->dlProgress setHidden:NO];
    }else{
        [cell->dlProgress setHidden:YES];
    }
    
    
    [AITFileCell fetchThumbnail:cell] ;
    
    cell.tag = indexPath.row;
    
    UIColor *color = [[UIColor alloc]initWithRed:81.0/255 green:199.0/255 blue:244.0/255 alpha:1];
    cell.selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
    cell.selectedBackgroundView.backgroundColor = color;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    AITFileCell *cell = (AITFileCell*) [tableView cellForRowAtIndexPath:indexPath];
//        AITFileNode *file = [[AITFileNode alloc]init];
    
    AITFileNode *file = [fileNodes objectAtIndex:indexPath.row] ;
    
    file.blSelected = YES;
    cell.checkBox.image = [UIImage imageNamed:@"checkBoxMarked.png"];
    
    FileNum++;
    [DeleteArray addObject:file];
    CountNum = 60;
    NSString *array = nil;
    for (int i = 0; i < DeleteArray.count; i++) {
        AITFileNode *Delete = [[AITFileNode alloc]init];
        Delete = [DeleteArray objectAtIndex:i];
        if ([Delete.name isEqualToString:file.name]) {
            array = Delete.name;
            break;
        }
    }
    
    NSString *FileName = [[NSString alloc]initWithFormat:@"%@",cell.fileName.text];
    [SelectFileName addObject:FileName];

    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    AITFileCell *cell = (AITFileCell*) [tableView cellForRowAtIndexPath:indexPath];
    AITFileNode *file = [fileNodes objectAtIndex:indexPath.row] ;
    
    file.blSelected = NO;
    cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
    FileNum--;
    
    for (int i = 0; i < DeleteArray.count; i++) {
        
        AITFileNode *file1 = [[AITFileNode alloc]init];
        file1 = [DeleteArray objectAtIndex:i];
        if ([file1.name isEqualToString:file.name]) {

            [DeleteArray removeObjectAtIndex:i];
            break;
        }
        
    }
    if ([SelectFileName count] > 0) {
        
        NSString *DelName = [[NSString alloc]initWithFormat:@"%@",cell.fileName.text];
        for (int i = 0 ; i < [SelectFileName count]; i++) {
            NSString *SaveName = [SelectFileName objectAtIndex:i];
            if ([DelName isEqualToString:SaveName]) {
                [SelectFileName removeObjectAtIndex:i];
            }
        }
    }

}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewCellEditingStyleNone;
}

#pragma mark - TableViewSelectReloadData

-(void)TableViewSelectReloadData{
    
    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++){
        
        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
        AITFileNode *AITFNode = [[AITFileNode alloc]init];
        AITFNode = [fileNodes objectAtIndex:p.row];
        AITFNode.blSelected = NO;
        [self.TableView deselectRowAtIndexPath:p animated:YES];
    }

    [self.TableView reloadData];
    FileNum = 0;
}

#pragma mark -  Setting MJRefresh

-(void)SettingSDTableViewReloadData{
    

    MJRefreshAutoGifFooter *footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
    
    // 设置文字
    if (zhSP) {
        /*
         MJRefreshStateIdle 闲置
         MJRefreshStatePulling 松开就可以进入的状态
         MJRefreshStateRefreshing 正在刷新的状态
         MJRefreshStateNoMoreData //没有数据
         MJRefreshStateWillRefresh 即将刷新的状态
         */
        [footer setTitle:@"上拉可以加载更多数据" forState:MJRefreshStateIdle];
        [footer setTitle:@"加载更多数据" forState:MJRefreshStatePulling];
        [footer setTitle:@"正在加载..." forState:MJRefreshStateRefreshing];
        [footer setTitle:@"没有更多了" forState:MJRefreshStateNoMoreData];
        
    }else{
        
        [footer setTitle:@"Drag up to refresh" forState:MJRefreshStateIdle];
        [footer setTitle:@"Loading more" forState:MJRefreshStatePulling];
        [footer setTitle:@"Loading..." forState:MJRefreshStateRefreshing];
        [footer setTitle:@"No more data" forState:MJRefreshStateNoMoreData];
    }
    // 设置字体
    footer.stateLabel.font = [UIFont systemFontOfSize:17];
    // 设置颜色
    footer.stateLabel.textColor = [UIColor grayColor];
    self.TableView.mj_footer = footer;
}

-(void)footerRereshing{
    
    CountNum = 60;
    if (dlCount)
    {
        return;
    }
    
    _isLoading = YES;
    
    BOOL filetyepe;
    if ([self.SwitchFrontOrRear selectedSegmentIndex] == 0 ) {
        filetyepe = YES;
    }else {
        filetyepe = NO;
    }
    fetchFirst = NO;
    switch (self.fileTypeSwitch.selectedSegmentIndex) {
        case 0:
        {
            _isLoading = YES;
            if (filetyepe){
                
                [self RequestData:4 JudeHUD:YES];
            }else{
                
                [self RequestData:41 JudeHUD:YES];
            }
            break;
        }
        case 1:
        {
            if (filetyepe) {
                [self RequestData:43 JudeHUD:YES];
            }else{
                [self RequestData:44 JudeHUD:YES];
            }
            
            break;
        }
        case 2:
        {
            if (filetyepe) {
                [self RequestData:42 JudeHUD:YES];
            }else{
                [self RequestData:421 JudeHUD:YES];
            }
            
            break;
        }
        default:
            break;
    }
    
    
}

#pragma mark - 判断是否已下载

//对比文件名，已下载返回NO，未下载返回YES

-(BOOL)JudeFileDown:(NSString *)newName{

    
    if ([DownArray count] < 1 ) {
        
        return YES;
        
    }else if (DownJpegArray.count < 1 ){
        
        return YES;
        
    }else{
        
        BOOL JudeReturn = YES;
        
        if (DownJpegArray.count > 0 && self.fileTypeSwitch.selectedSegmentIndex == 2) {
            
            for (int i = 0;  i < [DownJpegArray count]; i++){
                
                NSString *oldName = [NSString stringWithFormat:@"%@",[DownJpegArray objectAtIndex:i]];
                if ([newName rangeOfString:oldName].location !=NSNotFound ) {
                    
                    BOOL ShowAlert = YES;
                    
                    if (OpenImageArray.count > 0) {
                        
                        for (int i = 0 ; i < OpenImageArray.count ; i++) {
                            
                            NSString *ForDelete = [OpenImageArray objectAtIndex:i];
                            NSRange range = [ForDelete rangeOfString:@"IMG"];
                            NSString *P = [ForDelete substringToIndex:range.location];
                            ForDelete = [ForDelete stringByReplacingOccurrencesOfString:P withString:@""];
                            if ([newName rangeOfString:ForDelete].location !=NSNotFound ){
                                
                                ShowAlert = NO;
                                JudeReturn = YES;
                                [OpenImageArray removeObjectAtIndex:i];
                                [self QueryDownFileName];
                                
                            }else{
                                
                                JudeReturn =  NO;
                            }
                            
                        }
                    }
                    
                    if (ShowAlert) {
                        
                        NSString *message = [NSString stringWithFormat:@"%@ %@",newName,NSLocalizedString(@"The file is downloaded, please phone Archie", nil)];
                        if (!zhSP) {
                            
                            message = [NSString stringWithFormat:@"%@ %@",newName,NSLocalizedStringFromTable(@"The file is downloaded, please phone Archie", @"DefLocalizable", nil)];
                            
                        }
                        
                        [self ShowAlaertView:message Tag:444];
                        JudeReturn = NO;
                        break;
                    }
                }
            }
        }
        
        if (DownArray.count > 0 && self.fileTypeSwitch.selectedSegmentIndex != 2) {
            
            for (int i = 0;  i < [DownArray count]; i++) {
                
                NSString *oldName = [NSString stringWithFormat:@"%@",[DownArray objectAtIndex:i]];
                
                if ([newName rangeOfString:oldName].location !=NSNotFound ) {
                    
                    NSString *message = [NSString stringWithFormat:@"%@ %@",newName,NSLocalizedString(@"The file is downloaded, please phone Archie", nil)];
                    if (!zhSP) {
                        
                        message = [NSString stringWithFormat:@"%@ %@",newName,NSLocalizedStringFromTable(@"The file is downloaded, please phone Archie", @"DefLocalizable", nil)];
                        
                    }
                    
                    [self ShowAlaertView:message Tag:444];
                    JudeReturn = NO;
                    break;
                }
            }
        }
        
        
        return JudeReturn;
    }
    
}

-(void)QueryDownFileName{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *directory = [[NSString alloc]init];
    if([paths count] > 0)
    {
        directory = [paths objectAtIndex:0] ;
    }
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:NULL];
    
    DownArray = [[NSMutableArray alloc]init];
    DownJpegArray = [[NSMutableArray alloc]init];
    
    for(int i = 0; i < [directoryContent count]; i++){
        
        NSString *fileName = [NSString stringWithFormat:@"%@",[directoryContent objectAtIndex:i]];
        
        if([[directoryContent objectAtIndex:i] rangeOfString:@"jpg"].location != NSNotFound ||
           [[directoryContent objectAtIndex:i] rangeOfString:@"JPG"].location != NSNotFound){
            
            [DownJpegArray addObject:fileName];
            
        }
        if([[directoryContent objectAtIndex:i] rangeOfString:@"txt"].location == NSNotFound){
            
            [DownArray addObject:fileName];
        }
        
    }
}

#pragma mark - 删除预览下载的图片

-(void)DeleteOpenImage{
    
    if (OpenImageArray.count > 0) {
        
        NSString *deleflath = nil;
        for (int i = 0; i < OpenImageArray.count; i++ ) {
            deleflath = [OpenImageArray objectAtIndex:i];
            
            [[NSFileManager defaultManager] removeItemAtPath:deleflath error:nil] ;
        }
        
    }
}

#pragma mark - 动画

-(void)LoaingCustomAnimationsView{
    
    self.TableView.userInteractionEnabled = NO;
    if (AnimationsView == nil) {
        AnimationsView = [[UIView alloc]initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width /2 - 150 ,[[UIScreen mainScreen] bounds].size.height /2 - 200, 300, 300)];
    }
    
    AnimationsView.tag = 10013;
    
    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 50, AnimationsView.frame.size.height/ 2 - 50, 100, 100)];
    UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake( AnimationsView.frame.size.width/ 2 - 75 , imageview.frame.size.height + imageview.frame.origin.y + 20, 150, 50)];
    message.backgroundColor = [UIColor clearColor];
    message.tag = 10012;
    message.font = [UIFont systemFontOfSize:20];
    message.textColor = [UIColor whiteColor];
    message.textAlignment = NSTextAlignmentCenter;
    
    if (JudeLoadOrDown) {//YES是加载
        
        if (zhSP) {
            
            message.text = NSLocalizedString(@"Loading...", nil);

        }else{
            message.text = NSLocalizedStringFromTable(@"Loading...", @"DefLocalizable", nil);

        }
        
    }else{//下载
        
        
        if (zhSP) {
            message.text = NSLocalizedString(@"Downloading...", nil);
        }else{
            message.text = NSLocalizedStringFromTable(@"Downloading...", @"DefLocalizable", nil);
            
        }
        
    }
    if (JudeISDel) {
        
        
        if (zhSP) {
            message.text = NSLocalizedString(@"Deleting...", nil);
        }else{
            message.text = NSLocalizedStringFromTable(@"Deleting...", @"DefLocalizable", nil);
        }
        
    }
    
    
    imageview.image = [UIImage imageNamed:@"Imageloading.png"];
    imageview = [self rotate360DegreeWithImageView:imageview];
    imageview.tag = 10011;
    
    [AnimationsView addSubview:imageview];
    [AnimationsView addSubview:message];
    AnimationsView.backgroundColor = [UIColor clearColor];
    AnimationsView.userInteractionEnabled = NO;
    
    //让页面暗下动画
    if (parentView == nil) {
        
        parentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width , [[UIScreen mainScreen] bounds].size.height)];
    }
    
    parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    parentView.tag = 10010;
    [self.view addSubview:parentView];
    
    [UIView animateWithDuration:0.2f delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         parentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
                     }
                     completion:NULL
     ];

    [self.view addSubview:AnimationsView];
    
}

-(void)CustomAnimationsSuccessImage{
    
    if (AnimationsView) {
        
        UIImageView *SuccessImage = [[UIImageView alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 50, AnimationsView.frame.size.height/ 2 - 50, 100, 100)];
        SuccessImage.image = [UIImage imageNamed:@"ImageloadingSuccess.png"];
        CGRect imageview;
        for (UIView *obj in [AnimationsView subviews]) {
            
            if (obj.tag == 10011 || obj.tag == 10012) {
                
                if (obj.tag == 10011) {
                    UIImageView *a = (UIImageView *)obj;
                    [a.layer removeAllAnimations];
                    imageview = a.frame;
                }
                [obj removeFromSuperview];
            }
            
        }
        UILabel *message = [[UILabel alloc]initWithFrame:CGRectMake(AnimationsView.frame.size.width/ 2 - 75 , imageview.size.height + imageview.origin.y + 20, 150, 50)];
        message.backgroundColor = [UIColor clearColor];
        message.font = [UIFont systemFontOfSize:20];
        message.textColor = [UIColor whiteColor];
        message.textAlignment = NSTextAlignmentCenter;
        if (zhSP) {
            message.text = NSLocalizedString(@"Consummation", nil);
        }else{
            
            message.text = NSLocalizedStringFromTable(@"Consummation", @"DefLocalizable", nil);
            
        }
        
        message.tag = 10012;
        SuccessImage.tag = 10011;
        
        [AnimationsView addSubview:message];
        [AnimationsView addSubview:SuccessImage];
        
        [NSTimer scheduledTimerWithTimeInterval:1.4 target:self selector:@selector(StopCustomAnimationsView) userInfo:nil repeats:NO];
    }
    
}

-(void)StopCustomAnimationsView{
    
    self.TableView.userInteractionEnabled = YES;
    for (UIView *del in [AnimationsView subviews]) {
        
        if (del.tag == 10011 || del.tag == 10012) {

            [del removeFromSuperview];
        }
        
    }
    
    for (UIView *obj in [self.view subviews]) {
        
        if (obj.tag == 10010 || obj.tag == 10013) {
            
            [obj removeFromSuperview];
        }
    }

    AnimationsView = nil;
}

- (UIImageView *)rotate360DegreeWithImageView:(UIImageView *)imageView{
    
    CABasicAnimation *animation = [ CABasicAnimation
                                   animationWithKeyPath: @"transform" ];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    
    //围绕Z轴旋转，垂直与屏幕
    animation.toValue = [NSValue valueWithCATransform3D:
                         
                         CATransform3DMakeRotation(M_PI, 0.0, 0.0, 1.0) ];
    
    animation.duration = 1.0;
    //旋转效果累计，先转180度，接着再旋转180度，从而实现360旋转
    animation.cumulative = YES;
    animation.repeatCount = 600;
    
    //在图片边缘添加一个像素的透明区域，去图片锯齿
    CGRect imageRrect = CGRectMake(0, 0,imageView.frame.size.width, imageView.frame.size.height);
    UIGraphicsBeginImageContext(imageRrect.size);
    [imageView.image drawInRect:CGRectMake(1,1,imageView.frame.size.width-2,imageView.frame.size.height-2)];
    imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [imageView.layer addAnimation:animation forKey:nil];
    return imageView;
}

#pragma mark - Time BackToMainView

-(void)CountNumberBackToMainView{
    
    CountNum--;

    if (CountNum < 1) {
        
        if (dlCount == 0) {
            
            [NoOperation invalidate];
            NoOperation = nil;
            [self.navigationController popViewControllerAnimated:NO];
            
        }else{
            
            CountNum = 60;

        }
    }
    
}

#pragma mark - GetDate

-(GDataXMLElement *) getFirstChild:(GDataXMLElement *) element WithName: (NSString*) name{
    
    NSArray *elements = [element elementsForName:name] ;
    
    if (elements.count > 0)
    {
        
        return (GDataXMLElement *) [elements objectAtIndex:0];
    }
    
    return nil ;
}

- (UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize{
    
    UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
    [image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return reSizeImage;
}

- (void)openFile{
    
    /* do nothing if file downloadings in progress */
    if(dlCount)
        return;
    
    retFromOpen = YES;
    AITFileCell *cell= nil ;
    AITFileNode *fileNode = nil ;
    FileS = self.fileTypeSwitch.selectedSegmentIndex;
    FRNum = self.SwitchFrontOrRear.selectedSegmentIndex;

    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
    {
        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
        cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
        fileNode = [fileNodes objectAtIndex:p.row] ;
        
        if(cell && fileNode.blSelected){
            
            break;
        }else{
            cell = nil;
        }
    }
    
    if(cell == nil)
        return;
    
    //Open the file
#if 0
    NSURL *url = cell.filePath ;
    
    //[[UIApplication sharedApplication] openURL:url];
    if (url != nil) {
        
        MPMoviePlayerViewController *moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
        [self presentMoviePlayerViewControllerAnimated:moviePlayer];
        
        [moviePlayer.moviePlayer play];
        
    }
    
#else
    
    NSString *filrUrl = [cell.filePath absoluteString];
    if (([filrUrl rangeOfString:@".JPG"].location != NSNotFound) || ([filrUrl rangeOfString:@".JPEG"].location != NSNotFound) || (JudeDwonOrOpen == 1)){
        
        self.TableView.userInteractionEnabled = NO;
        openAfterDownload = YES;
        
        NSURL *url = cell.filePath ;
        
        NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] ;
        
        
        cell->dlProgress.hidden = FALSE;
        cell->dlProgress.progress = 0;
        
        if(dlTimer == nil){
            
            JudeLoadOrDown = YES;
            dlTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(SDupdateUI:) userInfo:nil repeats:YES];
        }
        
        
        if(fileNode->downloader == nil)
        {
            // Count downloading items
            dlCount++;
            
            // Prepare for downloading
            fileNode->downloader = [[AITFileDownloader alloc] initWithUrl:url Path:[directory stringByAppendingPathComponent:[fileNode.name substringFromIndex:[fileNode.name rangeOfString:@"/" options:NSBackwardsSearch].location + 1]]];
        }
    }else{
        
        appDelegate.allowRotation = YES;
        
        viewController = [[VLCMovieViewController alloc] initWithNibName:@"VLCMovieViewController" bundle:nil] ;
        viewController.url = cell.filePath ;
        viewController.edgesForExtendedLayout = UIRectEdgeNone;
        
        self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Back", nil) style: UIBarButtonItemStyleDone target:nil action:nil] ;
        if (!zhSP) {
            
            self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedStringFromTable(@"Back", @"DefLocalizable", nil) style: UIBarButtonItemStyleDone target:nil action:nil] ;
            
        }
        
        
        videoTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(videoLoading:) userInfo:nil repeats:YES];
        
        
        boolPushVLCMovieViewControllerOrDocumentInteractionController = YES;
        [self.navigationController pushViewController:viewController animated:YES];
    }
#endif
}

- (void)videoLoading:(NSTimer *)timer{
    
    if([viewController isPlaying]){
        
        [videoTimer invalidate];
        videoTimer = nil;
        
        /* Hide activity indicator */
        [self CustomAnimationsSuccessImage];
        
    }
}

- (void)downloadFile{
    
    
    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++){
        
        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
        AITFileNode *fileNode = [fileNodes objectAtIndex:p.row] ;
        
        if (fileNode.blSelected && [self JudeFileDown:fileNode.name]){
            
            NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"http://%@%@", [AITUtil getCameraAddress], [fileNode.name stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]] ;
            NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] ;
            
            [self.TableView deselectRowAtIndexPath:p animated:YES];
            if (cell) {
                
                cell->dlProgress.hidden = FALSE;
                cell->dlProgress.progress = 0;
            }
            
            
            if(dlTimer == nil){
                
                JudeLoadOrDown = NO;
                [self LoaingCustomAnimationsView];
                dlTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(SDupdateUI:) userInfo:nil repeats:YES];
            }
            
            if(fileNode->downloader == nil){
                
                // Count downloading items
                dlCount++;
                
                // Start downloading
                fileNode->downloader = [[AITFileDownloader alloc] initWithUrl:url Path:[directory stringByAppendingPathComponent:[fileNode.name substringFromIndex:[fileNode.name rangeOfString:@"/" options:NSBackwardsSearch].location + 1]]];
            }
        }
    }
    
    if(dlCount == 0 || dlCount < 1){
        JudeISDown = NO;
    }
}

- (void)stopDownload{
    
    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++)
    {
        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p];
        AITFileNode *fileNode = [fileNodes objectAtIndex:p.row] ;
        NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] ;
        
        if(cell){
            
            cell->dlProgress.hidden = YES;
            cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
        }
        
        if(fileNode->downloader != nil){
            
            // Count downloading items
            dlCount--;
            
            // Stop downloading
            [fileNode->downloader stopDownload:[directory stringByAppendingPathComponent:[fileNode.name substringFromIndex:[fileNode.name rangeOfString:@"/" options:NSBackwardsSearch].location + 1]]];
            fileNode->downloader = nil;
            fileNode->downloader = nil;
            fileNode.blSelected = NO;
            
        }
    }
}

- (void)SDupdateUI:(NSTimer *)timer{
    
    if (AnimationsView == nil || AnimationsView.hidden == YES) {
        [self LoaingCustomAnimationsView];
    }
    
    self.TableView.userInteractionEnabled = NO;
    for (int i = 0; i < [self.TableView numberOfRowsInSection:0]; i++){
        
        
        NSIndexPath *p = [NSIndexPath indexPathForRow:i inSection:0];
        
        AITFileCell *cell = (AITFileCell*)[self.TableView cellForRowAtIndexPath:p] ;
        
        //        if(cell){
        
        AITFileNode *fileNode = [fileNodes objectAtIndex:p.row] ;
        
        AITFileDownloader *downloader = fileNode->downloader;
        
        if(downloader != nil){
            
            /* start downloading */
            if(downloader->downloading == false){
                [downloader startDownload];
                
            }
            
        
            
            /* download failed */
            if(downloader->offsetInFile == -1){
                
                fileNode->downloader = nil;
                dlCount--;
                
                
                if (dlCount < 1) {
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:fileNode.name message:NSLocalizedString(@"Download failed!", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil)  otherButtonTitles:nil, nil];
                    if (!zhSP) {
                        
                        alert = [[UIAlertView alloc] initWithTitle:fileNode.name message:NSLocalizedStringFromTable(@"Download failed!", @"DefLocalizable", nil) delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"OK", @"DefLocalizable", nil)  otherButtonTitles:nil, nil];
                        
                    }
                    
                    [alert show] ;
                    
                    cell->dlProgress.hidden = TRUE;
                    self.TableView.userInteractionEnabled = YES;
                    
                    fileNode.blSelected = NO;
                    cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];
                    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(StoploadingHUD) userInfo:nil repeats:NO];
                    
                }else{
                    
                    if (zhSP) {
                    
                        [self.view makeToast:NSLocalizedString(@"Download failed!", nil) duration:1.5 position:nil];
                    }else{
                        
                        [self.view makeToast:NSLocalizedStringFromTable(@"Download failed!", @"DefLocalizable", nil) duration:1.5 position:nil];
                        
                    }

                }
                
                CountNum = 60;
                
            } else if (downloader->bodyLength > 0){
                
                /* download succeeded */
                
                if(downloader->offsetInFile == downloader->bodyLength)
                {
                    
                    if (cell) {
                        
                        cell->dlProgress.progress = 1;
                        cell->dlProgress.hidden = YES;
                        cell.checkBox.image = [UIImage imageNamed:@"checkBox.png"];

                    }
                    fileNode->downloader = nil;
                    fileNode.blSelected = NO;
                    dlCount--;
                    
                    
                    if(openAfterDownload){
                        
                        if (OpenImageArray == nil) {
                            OpenImageArray = [[NSMutableArray alloc]init];
                        }
                        NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] ;
                        NSString *filePath = [NSString stringWithFormat:@"%@/%@", directory, [fileNode.name substringFromIndex:[fileNode.name rangeOfString:@"/" options:NSBackwardsSearch].location + 1]];
                        NSURL *url = [NSURL fileURLWithPath: filePath] ;
                        BOOL IfAdd = YES;
                        if ([DownJpegArray count] > 0) {
                            for (int i = 0;  i < [DownJpegArray count]; i++){
                                NSString *addname = [DownJpegArray objectAtIndex:i];
                                if ([fileNode.name rangeOfString:addname].location !=NSNotFound ){
                                  
                                    IfAdd = NO;
                                    break;
                                }
                            }
                        }
                        if (IfAdd) {
                      
                            [OpenImageArray addObject:filePath];
                        }
                        
                        boolPushVLCMovieViewControllerOrDocumentInteractionController = YES;
                        UIDocumentInteractionController *documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
                        
                        appDelegate.allowRotation = YES;
                        [documentInteractionController setDelegate:(id<UIDocumentInteractionControllerDelegate>)self];
                        [documentInteractionController presentPreviewAnimated:YES] ;
                        
                        self.TableView.userInteractionEnabled = YES;
                        
                    }
                    CountNum = 60;
                    
                }else{
                    /* downloading */
                 
                    if (cell) {
                        cell->dlProgress.progress = (float) downloader->offsetInFile/downloader->bodyLength;
                    }
                    
                }
            }
            break;
        }

        
    }
    
    if(dlCount == 0 || dlCount < 1){
        
        [dlTimer invalidate];
        dlTimer = nil;
        JudeISDown = NO;
        FileNum = 0;
        dlCount = 0;
        [SelectFileName removeAllObjects];
        if (!openAfterDownload) {
            
            self.TableView.userInteractionEnabled = YES;
            
        }
        openAfterDownload = NO;
        if (AnimationsView != nil) {
            
            [self TableViewSelectReloadData];
            [self CustomAnimationsSuccessImage];
            
        }
        [self QueryDownFileName];
        JudeLoadOrDown = YES;
        
        
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {

            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        
        
        
    }
    
}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller
{
    return self;
}

- (UIView*)documentInteractionControllerViewForPreview:(UIDocumentInteractionController*)controller{
    
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController*)controller{
    
    return self.view.frame;
}

//点击预览窗口的“Done”(完成)按钮时调用
- (void)documentInteractionControllerDidEndPreview:(UIDocumentInteractionController*)_controller
{

}

@end
