//
//  MainViewController.m
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import "MainViewController.h"
#import "VideoViewController.h"
#import "SettingsViewController.h"
#import "HelpViewController.h"
#import "PhoneFileViewController.h"
#import "SDFileViewController.h"
#import "StorageStatusViewController.h"
#import "AITCameraCommand.h"
#import "AppDelegate.h"
#import "AITUtil.h"
#import "Toast+UIView.h"

typedef enum
{
    CAMERA_CMD_SET_RAND,
    CAMERA_CMD_GET_RAND,
    CAMERA_CMD_QUERY_RECORDING,
    CAMERA_CMD_RECORD,
    CAMERA_CMD_GET_FW_VER,
    CAMERA_CMD_INVALID,
    CAMERA_CMD_START_RECORDING,
    CAMERA_PRE_STREAMING,
    CAMERA_OPEN_VIDEO,
    CAMERA_START_VIDEO,
    CAMERA_STOP_VIDEO,
    CAMERA_CMD_QUERY_SETTINGVIEW
    
} Camera_cmd_t;

@interface MainViewController (){
    
    NSArray *tableData;
    MBProgressHUD *_MainViewHUD;
    Camera_cmd_t camera_cmd;
    u_int32_t randA;//加密变量
    u_int32_t randB;
    
    BOOL IS_NORecording;
    BOOL GoToSettingView;
    NSInteger JudeNumView;//记录从哪个view回来
    AppDelegate *appDelegate;
    CustomIOSAlertView *noWifiAlert;
    
    
}

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CloseAerltView) name:@"MainViewControllerCloseAerltView" object:nil];
    return self;
}

-(NSUInteger) supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (zhSP) {
        
        self.LabelHelp.text = NSLocalizedString(@"Help", nil);
        self.LabelPhoneFile.text = NSLocalizedString(@"Phone Files", nil);
        self.LabelSDFile.text = NSLocalizedString(@"SD Files", nil);
        self.LabelSetting.text = NSLocalizedString(@"Settings", nil);
        self.LabelStorageStatus.text = NSLocalizedString(@"Storage Status", nil);
        self.LabelVideo.text = NSLocalizedString(@"Video View", nil);
        
    }else{
        
        self.LabelHelp.text = NSLocalizedStringFromTable(@"Help", @"DefLocalizable", nil);
        self.LabelPhoneFile.text = NSLocalizedStringFromTable(@"Phone Files", @"DefLocalizable", nil);
        self.LabelSDFile.text = NSLocalizedStringFromTable(@"SD Files", @"DefLocalizable", nil);
        self.LabelSetting.text = NSLocalizedStringFromTable(@"Settings", @"DefLocalizable", nil);
        self.LabelStorageStatus.text = NSLocalizedStringFromTable(@"Storage Status", @"DefLocalizable", nil);
        self.LabelVideo.text = NSLocalizedStringFromTable(@"Video View", @"DefLocalizable", nil);
        self.ImageWord.image = [UIImage imageNamed:@"Main_EnglishWords.png"];
        
    }
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.navigationController.navigationBar.shadowImage = [UIImage alloc ];
    self.navigationController.navigationBarHidden = YES;
    

    
    appDelegate.allowRotation = NO;
    GoToSettingView = NO;
    
    if (JudeNumView != 0) {
        if (JudeNumView == 3) {
            
            [self GetCommand:1];
            
        }
    }
    JudeNumView = 0;
    static BOOL boolFirstIntoFunctionListView = YES;
    if (boolFirstIntoFunctionListView){
        
        NSString *WifiIsYN ;
        if (!appDelegate.Encryption){
            
            WifiIsYN = [NSString stringWithFormat:@"Wifi=NO"];
            [[NSUserDefaults standardUserDefaults]setObject:WifiIsYN forKey:@"JudeWifi"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            noWifiAlert = [[CustomIOSAlertView alloc] init] ;
            
            UIView *messageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 180)];
            if (zhSP) {
                
                [noWifiAlert setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"OK", nil), nil]];
                
            }else{
                
                [noWifiAlert setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedStringFromTable(@"OK", @"DefLocalizable", nil), nil]];
               
            }
            
            
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 300, 50)];
            [label setFont:[UIFont systemFontOfSize:18]];
            
            label.textAlignment = NSTextAlignmentCenter;
            
            if (zhSP) {
                
                label.text = NSLocalizedString(@"Please check Wi-Fi connection", nil);
                tableData = [NSArray arrayWithObjects:NSLocalizedString(@"Please check CarDV is on", nil), NSLocalizedString(@"Activate Wi-Fi connection",nil), NSLocalizedString(@"Connect your mobile phone to CarDV by Wi-Fi",nil), nil];
            }else{
                
                label.text = NSLocalizedStringFromTable(@"Please check Wi-Fi connection", @"DefLocalizable", nil);
                tableData = [NSArray arrayWithObjects:NSLocalizedStringFromTable(@"Please check CarDV is on", @"DefLocalizable", nil), NSLocalizedStringFromTable(@"Activate Wi-Fi connection", @"DefLocalizable", nil), NSLocalizedStringFromTable(@"Connect your mobile phone to CarDV by Wi-Fi", @"DefLocalizable", nil), nil];
                
            }
            
            [messageView addSubview:label];
            
            UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 50, 300, 130) style:UITableViewStylePlain];
            tableView.scrollEnabled = NO;
            
            
            tableView.delegate = (id<UITableViewDelegate>)self;
            tableView.dataSource = (id<UITableViewDataSource>)self;
            
            [messageView addSubview:tableView];
            [noWifiAlert setContainerView:messageView] ;
            [noWifiAlert setUseMotionEffects:true] ;
            [noWifiAlert show] ;

        }
        
        boolFirstIntoFunctionListView = NO;
    }
    
}

-(void)HiddenMainViewHUD{
    
    if ( _MainViewHUD != nil) {
        [_MainViewHUD hide:YES];
        _MainViewHUD.hidden = YES;
        _MainViewHUD = nil;
    }
}


- (NSString *)filePathOfDocument:(NSString *)filename{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDir = [paths objectAtIndex:0];
    
    return [docDir stringByAppendingPathComponent:filename];
}


-(void)CloseAerltView{
    
    if (noWifiAlert != nil) {
        [noWifiAlert close];
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MainViewControllerCloseAerltView" object:nil];
}

-(void)LoadingMainViewHUD{
    
    _MainViewHUD =[[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:_MainViewHUD];
    _MainViewHUD.color = [UIColor grayColor];
    _MainViewHUD.delegate = self;
    if (zhSP) {
        _MainViewHUD.labelText = NSLocalizedString(@"Loading...", nil);
    }else{
        _MainViewHUD.labelText = NSLocalizedStringFromTable(@"Loading...", @"DefLocalizable", nil);
        
    }
    
    _MainViewHUD.labelColor = [UIColor whiteColor];
    [_MainViewHUD show:YES];
}

#pragma mark - 发送命令函数

-(void)GetCommand:(NSInteger)Num{
    
    switch (Num) {
            
        case 0://查询设备状态
        {
            camera_cmd = CAMERA_CMD_QUERY_RECORDING;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandQueryCameraStatusUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 1://非031用 开启录像
        {
            camera_cmd = CAMERA_START_VIDEO;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStartRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        case 2://停止录像
        {
            camera_cmd = CAMERA_STOP_VIDEO;
            (void)[[AITCameraCommand alloc] initWithUrl:[AITCameraCommand commandCameraStopRecordUrl] Delegate:(id<AITCameraRequestDelegate>)self] ;
            break;
        }
        default:
            break;
    }
    
}

#pragma mark - 命令回调函数

-(void) requestFinished:(NSString*) result{
    
    BOOL blShowControlView = YES;
    
    switch (camera_cmd) {
            
        case CAMERA_CMD_QUERY_RECORDING:
        {
            if(result)
            {
                /* Check recording status */
                NSRange range = [result rangeOfString:CAMEAR_PREVIEW_MJPEG_STATUS_RECORD];
                
                if(range.location != NSNotFound)
                {
                    NSString *camearRecordStatus = [[result substringFromIndex:NSMaxRange(range)+1] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                    if([camearRecordStatus rangeOfString:@"Recording"].location != NSNotFound){
                        
                        blShowControlView = NO;
                        UIAlertView *av = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Recording will be stopped before entering setting", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Enter", nil), nil] ;
                        if (!zhSP) {
                            
                            av = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTable(@"Recording will be stopped before entering setting", @"DefLocalizable", nil) message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTable(@"Cancel", @"DefLocalizable", nil) otherButtonTitles:NSLocalizedStringFromTable(@"Enter", @"DefLocalizable", nil), nil] ;
                            
                        }
                        
                        [av setTag:1];
                        [av show];
                    }else{
                        
                        blShowControlView = YES;
                        
                    }
                }
            }
            break;
        }
        case CAMERA_START_VIDEO:
        {
            if([result rangeOfString:@"OK"].location != NSNotFound) {
                
                if (zhSP) {
                    
                    [self.view makeToast:NSLocalizedString(@"Video has been opened.", nil) duration:1.0 position:nil];
                    
                }else{
                    
                    [self.view makeToast:NSLocalizedStringFromTable(@"Video has been opened.", @"DefLocalizable", nil) duration:1.0 position:nil];
                    
                }
                
                
            }
            break;
        }
        case CAMERA_STOP_VIDEO:
        {
            if([result rangeOfString:@"OK"].location != NSNotFound) {
                blShowControlView = YES;
                
            }
            break;
        }
            
        default:
            break;
    }
    
    if(blShowControlView && GoToSettingView){//跳转到设置界面
        
        if (_MainViewHUD != nil) {
            [_MainViewHUD setHidden:YES];
        }
        GoToSettingView = NO;
        [self PushViewController:2];
        
    }
    
}


#pragma mark - AlertView
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex)
    {
        case 0:
        {
            
            [_MainViewHUD setHidden:YES];
            break;
        }
        case 1:
        {
            if([alertView tag] == 1 )
            {
                /* Entering or returning from setting page */
               
                [self GetCommand:2];
                
            } else if([alertView tag] == 4){

            } else
                break;
        }
        default:
            break;
    }
}


#pragma mark - Button IBAction
//预览
- (IBAction)TouchVideoView:(id)sender {
    
    JudeNumView = 1;
    [self PushViewController:0];
}
//SD档案
- (IBAction)TouchSDView:(id)sender{
 
    JudeNumView = 2;
    [self PushViewController:1];
    
}
//设置界面
- (IBAction)TouchSettingsView:(id)sender{
    
    JudeNumView = 3;
    int valu = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (valu >= 10|| ([AITUtil isWiFiEnabled] && valu < 10)) {
        
        GoToSettingView = YES;
        [self GetCommand:0];
        
    }else{
        
        if (zhSP) {
            
            [self.view makeToast:NSLocalizedString(@"Connect your mobile phone to CarDV by Wi-Fi",nil) duration:1.0 position:nil];
            
        }else{
            
            [self.view makeToast:NSLocalizedStringFromTable(@"Connect your mobile phone to CarDV by Wi-Fi", @"DefLocalizable", nil) duration:1.0 position:nil];
            
        }
        
    }
    
    
}
//本地文档
- (IBAction)TouchPhoneFileoView:(id)sender{
    
    JudeNumView = 4;
    [self PushViewController:3];
}
//存储状态
- (IBAction)TouchStorageStatusView:(id)sender{
    
    JudeNumView = 5;
    [self PushViewController:4];
}
//帮助页面
- (IBAction)TouchHelpView:(id)sender{

    JudeNumView = 6;
    [self PushViewController:5];
}

-(void)PushViewController:(int)Num{
    
    NSString *JudeWIFI = [[NSUserDefaults standardUserDefaults]stringForKey:@"JudeWifi"];
    BOOL JudeNOPush = YES;
    if ([JudeWIFI isEqualToString:@"Wifi=NO"]) {
        
        if (zhSP) {
            
            [self.view makeToast:NSLocalizedString(@"Connect your mobile phone to CarDV by Wi-Fi",nil) duration:1.0 position:nil];
        }else{
            
            [self.view makeToast:NSLocalizedStringFromTable(@"Connect your mobile phone to CarDV by Wi-Fi", @"DefLocalizable", nil) duration:1.0 position:nil];
            
        }
        
        JudeNOPush = NO;
    }
    UIViewController *pusview = [[UIViewController alloc]init];
    switch (Num) {
        case 0:
        {
            if (JudeNOPush){
                
                pusview = [[VideoViewController alloc]initWithNibName:@"VideoViewController" bundle:nil];
            }
            break;
        }
        case 1:
        {
            if (JudeNOPush){
                
               pusview = [[SDFileViewController alloc]initWithNibName:@"SDFileViewController" bundle:nil];
            }
            
            break;
        }
        case 2:
        {
            if (JudeNOPush) {
                pusview = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
            }
            
            break;
        }
        case 3:
        {
            pusview = [[PhoneFileViewController alloc]initWithNibName:@"PhoneFileViewController" bundle:nil];
            break;
        }
        case 4:
        {
            if (JudeNOPush) {
                pusview = [[StorageStatusViewController alloc]initWithNibName:@"StorageStatusViewController" bundle:nil];
            }
            
            break;
        }
        case 5:
        {
            pusview = [[HelpViewController alloc]initWithNibName:@"HelpViewController" bundle:nil];
            break;
        }
        default:
            break;
    }
    
    if (pusview) {
        
        if (JudeNOPush) {
            [self.navigationController pushViewController:pusview animated:YES];
        }else{
            
            if (Num == 5 || Num == 3) {
                
                [self.navigationController pushViewController:pusview animated:YES];
            }
        }
        
    }
    
}

#pragma mark - Table view data source  IOS7AlertView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    // Return the number of sections.
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.textLabel.numberOfLines = 0;
    
    return cell;
}

-(UITableViewCellAccessoryType)tableView:(UITableView *)tableView
        accessoryTypeForRowWithIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewCellAccessoryNone;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    return cell.frame.size.height;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 0.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES] ;
    
    switch(indexPath.row)
    {
        case 0:
            break;
        case 1:

            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"]];
            [noWifiAlert close];
            break;
        case 2:
            break;
        default:
            break;
    }
}


@end
