//
//  MainViewController.h
//  AccompaniedAllTheWay
//
//  Created by macvision on 16/1/20.
//  Copyright © 2016年 AccompaniedAllTheWay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "MBProgressHUD.h"

@interface MainViewController : UIViewController<UIAlertViewDelegate,MBProgressHUDDelegate>



@property (weak, nonatomic) IBOutlet UILabel *LabelVideo;
@property (weak, nonatomic) IBOutlet UILabel *LabelSetting;
@property (weak, nonatomic) IBOutlet UILabel *LabelHelp;
@property (weak, nonatomic) IBOutlet UILabel *LabelStorageStatus;
@property (weak, nonatomic) IBOutlet UILabel *LabelSDFile;
@property (weak, nonatomic) IBOutlet UILabel *LabelPhoneFile;

@property (weak, nonatomic) IBOutlet UIImageView *ImageWord;


- (IBAction)TouchVideoView:(id)sender;//预览
- (IBAction)TouchSDView:(id)sender;//SD档案
- (IBAction)TouchSettingsView:(id)sender;//设置界面
- (IBAction)TouchPhoneFileoView:(id)sender;//本地文档
- (IBAction)TouchStorageStatusView:(id)sender;//存储状态
- (IBAction)TouchHelpView:(id)sender;//帮助页面


@end
