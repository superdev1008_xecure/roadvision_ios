//
//  MDRadialProgressViewController.h
//  WiFiCameraViewer
//
//  Created by Fred Chen on 2014/7/20.
//  Copyright (c) 2014年 a-i-t. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDRadialProgressViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelLoading;
@end
