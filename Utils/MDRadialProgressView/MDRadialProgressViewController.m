//
//  MDRadialProgressViewController.m
//  WiFiCameraViewer
//
//  Created by Fred Chen on 2014/7/20.
//  Copyright (c) 2014年 a-i-t. All rights reserved.
//

#import "MDRadialProgressViewController.h"
#import "MDRadialProgressView.h"

@interface MDRadialProgressViewController ()
{
    NSTimer *timer;
}
@end

@implementation MDRadialProgressViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateUI:) userInfo:nil repeats:YES];

}

- (void)updateUI:(NSTimer *)timer
{
    static int progress = 0;
    CGRect frame = CGRectMake(180, 10, 30, 30);
    MDRadialProgressView *radialView = [[MDRadialProgressView alloc] initWithFrame:frame];
    
    radialView.progressTotal = 10;
    radialView.progressCurrent =  progress % 10;
	radialView.completedColor = [UIColor darkGrayColor];
	radialView.incompletedColor = [UIColor colorWithRed:0.1f green:0.1f blue:0.1f alpha:0.1f];
    radialView.thickness = 8;
    radialView.backgroundColor = [UIColor whiteColor];
    radialView.sliceDividerHidden = NO;
    radialView.sliceDividerColor = [UIColor whiteColor];
    radialView.sliceDividerThickness = 1;
    progress++;
    
	[self.view addSubview:radialView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [timer invalidate];
    timer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
