
#import "AppXMlTemplate.h"

@implementation AppXMlTemplate 


-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.XMlTitle  forKey:@"XMlTitle"];
    [aCoder encodeObject:self.XMlGet    forKey:@"XMlGet"];
    [aCoder encodeObject:self.XMlSet    forKey:@"XMlSet"];
    [aCoder encodeObject:self.XMlSwitch forKey:@"XMlSwitch"];
    [aCoder encodeObject:self.XMlID     forKey:@"XMlID"];
    [aCoder encodeObject:self.XMlText   forKey:@"XMlText"];
    [aCoder encodeObject:self.XMlArray   forKey:@"XMlArray"];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        
        self.XMlTitle  = [aDecoder decodeObjectForKey:@"XMlTitle"];
        self.XMlGet    = [aDecoder decodeObjectForKey:@"XMlGet"];
        self.XMlSet    = [aDecoder decodeObjectForKey:@"XMlSet"];
        self.XMlSwitch = [aDecoder decodeObjectForKey:@"XMlSwitch"];
        self.XMlID     = [aDecoder decodeObjectForKey:@"XMlID"];
        self.XMlText   = [aDecoder decodeObjectForKey:@"XMlText"];
        self.XMlArray   = [aDecoder decodeObjectForKey:@"XMlArray"];
        
        
    }
    return self;
}


@end
