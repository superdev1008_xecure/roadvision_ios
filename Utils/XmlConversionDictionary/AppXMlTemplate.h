
#import <Foundation/Foundation.h>

@interface AppXMlTemplate : NSObject<NSCoding>

@property (nonatomic ,strong) NSString *XMlTitle;
@property (nonatomic ,strong) NSString *XMlGet;
@property (nonatomic ,strong) NSString *XMlSet;
@property (nonatomic ,strong) NSString *XMlSwitch;
@property (nonatomic ,strong) NSString *XMlCurrentValue;
//设置页面显示的值
@property (nonatomic ,strong) NSMutableDictionary *XMlID;
@property (nonatomic ,strong) NSMutableDictionary *XMlText;
@property (nonatomic ,strong) NSMutableArray *XMlArray;
//记录存入顺序,弹窗用
@end
