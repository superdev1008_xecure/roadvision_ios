//
//  AITCameraCommand.h
//  WiFiCameraViewer
//
//  Created by Clyde on 2013/11/8.
//  Copyright (c) 2013年 a-i-t. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AITCameraRequest.h"

#define zhSP ([[[NSLocale preferredLanguages] objectAtIndex:0] hasPrefix:@"zh-Hans"])//-CN
//macro redefined  isEqualToString
//#define isZhHant [[[NSLocale preferredLanguages] firstObject] hasPrefix:@"zh-Hans"]

#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)

#define IS_IPHONE4 (([[UIScreen mainScreen] bounds].size.height == 480) ? YES : NO)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height == 568) ? YES : NO)
#define IS_IPhone6 (667 == [[UIScreen mainScreen] bounds].size.height ? YES : NO)
#define IS_IPhone6plus (736 == [[UIScreen mainScreen] bounds].size.height ? YES : NO)

static NSString *DEFAULT_MJPEG_PUSH_URL = @"/cgi-bin/liveMJPEG" ;
static NSString *DEFAULT_RTSP_URL_AV1   = @"/liveRTSP/av1" ;
static NSString *DEFAULT_RTSP_URL_V1    = @"/liveRTSP/v1" ;
static NSString *DEFAULT_RTSP_URL_AV2    = @"/liveRTSP/av2" ;
static NSString *DEFAULT_RTSP_URL_AV4    = @"/liveRTSP/av4" ;
static NSString *CAMEAR_PREVIEW_MJPEG_STATUS_RECORD = @"Camera.Preview.MJPEG.status.record" ;
static NSString *CAMEAR_PREVIEW_MJPEG_STATUS_MUTE = @"Camera.Preview.MJPEG.status.mute" ;
static NSString *CAMEAR_PREVIEW_MJPEG_STATUS = @"Camera.Preview.MJPEG.status" ;

static NSString *CAMEAR_PREVIEW_TIMESTAMP_YEAR = @"Camera.Preview.MJPEG.TimeStamp.year" ;
static NSString *CAMEAR_PREVIEW_TIMESTAMP_MONTH = @"Camera.Preview.MJPEG.TimeStamp.month" ;
static NSString *CAMEAR_PREVIEW_TIMESTAMP_DAY = @"Camera.Preview.MJPEG.TimeStamp.day" ;
static NSString *CAMEAR_PREVIEW_TIMESTAMP_HOUR = @"Camera.Preview.MJPEG.TimeStamp.hour" ;
static NSString *CAMEAR_PREVIEW_TIMESTAMP_MINUTE = @"Camera.Preview.MJPEG.TimeStamp.minute" ;
static NSString *CAMEAR_PREVIEW_TIMESTAMP_SECOND = @"Camera.Preview.MJPEG.TimeStamp.second" ;

@interface AITCameraCommand : NSObject <AITCameraRequestDelegate>

+ (NSURL*) commandUpdateUrl: (NSString *) ssid EncryptionKey: (NSString *) encryptionKey ;
+ (NSURL*) commandGetMenuSettingsValuesUrl ;
+ (NSURL*) commandSetVideoClipTimeUrl: (NSString *) value ;
+ (NSURL*) commandSetVideoResUrl: (NSString *) resolution ;
+ (NSURL*) commandSetImageResUrl: (NSString *) resolution ;
+ (NSURL*) commandSetMotionDetUrl: (NSString *) enable ;
+ (NSURL*) commandSetFlickerUrl: (NSString *) freq ;
+ (NSURL*) commandSetAWBUrl: (NSString *) value ;
+ (NSURL*) commandSetEVUrl: (NSString *) value ;
+ (NSURL*) commandCameraTimeSettingsUrl ;
+ (NSURL*) commandFindCameraUrl ;
+ (NSURL*) commandCameraSnapshotUrl;
+ (NSURL*) commandCameraRecordUrl;
+ (NSURL*) commandQueryCameraStatusUrl;
+ (NSURL*) commandQueryCameraRecordUrl;
+ (NSURL*) commandQueryCameraMuteUrl;
+ (NSURL*) commandCameraMuteUrl: (NSString *) enable;
+ (NSURL*) commandTimeStampUrl;
+ (NSURL*) commandReactivateUrl ;
+ (NSURL*) commandWifiInfoUrl ;
+ (NSURL*) commandListFileUrl: (NSInteger) count ;
+ (NSURL*) commandListFirstFileUrl: (NSInteger) count ;
+ (NSURL*) commandDelFileUrl: (NSString *) fileName;
+ (NSURL*) commandSetRandUrl: (u_int32_t) rand ;
+ (NSURL*) commandGetRandUrl;
+ (NSURL*) commandCameraFormatSDCardUrl;
+ (NSURL*) commandSetMenuGsensorLockSUrl: (NSString *) value;
+ (NSURL*) commandGetParkingMonitoringUrl;
+ (NSURL*) commandSetParkingMonitoringUrl: (NSString *) value;
+ (NSURL*) commandGetTVOutUrl;
+ (NSURL*) commandSetTVOutUrl: (NSString *) value;
+ (NSURL*) commandGetLDWSUrl;
+ (NSURL*) commandSetLDWSUrl: (NSString *) value;
+ (NSDictionary*) buildResultDictionary:(NSString*)result;

//Add Ted 3.0版本接口

+ (NSURL*) commandOldCameraMuteUrl;
+ (NSURL*) commandOldCameraMuteUrl: (NSString *) enable;

+ (NSURL*) commandRearPreviewUrl;
+ (NSURL*) commandFrontPreviewUrl;
+ (NSURL*) commandQueryPreviewStatusUrl;
+ (NSURL*) commandQueryCameraSDFileUrl;
+ (NSURL*) commandCameraResetSettingUrl;
+ (NSURL*) commandQueryRearVideoUrl;
//SD
+ (NSURL*) commandFirstSDRearFileUrl;
+ (NSURL*) commandFirstSDFrontFileUrl;
+ (NSURL*) commandFirstSDImageFileUrl;
+ (NSURL*) commandFirstSDRearImageFileUrl;
+ (NSURL*) commandSDRearFileUrl: (NSInteger) count;
+ (NSURL*) commandSDFrontFileUrl: (NSInteger) count;
+ (NSURL*) commandSDImageFileUrl: (NSInteger) count;
+ (NSURL*) commandSDSOSFrontFileUrl: (NSInteger) count;
+ (NSURL*) commandSDSOSRearFileUrl: (NSInteger) count;
+ (NSURL*) commandSDRearImageFileUrl: (NSInteger) count;
+ (NSURL*) commandFirstSDSOSRearFileUrl;
+ (NSURL*) commandFirstSDSOSFrontFileUrl;

+ (NSURL*) commandSetTVOutNTSCUrl: (NSString *) value;
+ (NSURL*) commandSetTVOutPALUrl: (NSString *) value;
+ (NSURL*) commandQueryCameraMemoryCardUrl;

+ (NSURL*) commandCameraStartRecordUrl;
+ (NSURL*) commandCameraStopRecordUrl;
+ (NSURL*) commandSetMenuFCWSUrl: (NSString *) value;
+ (NSURL*) commandSetMenuLDWSUrl: (NSString *) value;
+ (NSURL*) commandSetMenuMTDSUrl: (NSString *) value;
+ (NSURL*) commandSetCameraidUrl: (NSString *) camid;
+ (NSURL*) commandGetCamMenu;
+(NSURL*)commandSettingsViewUrl:(NSString *)property value:(NSString *) value;
//Add Ted

+ (NSString *) PROPERTY_CAMERA_RTSP;
+ (NSString *) PROPERTY_SSID ;
+ (NSString *) PROPERTY_ENCRYPTION_KEY ;
+ (NSURL*) commandCameraTextUrl;


- (id) initWithUrl: (NSURL*) url View: (UIView *)aView ;
- (id) initWithUrl: (NSURL*) url Delegate:(id<AITCameraRequestDelegate>)delegate ;

@end
