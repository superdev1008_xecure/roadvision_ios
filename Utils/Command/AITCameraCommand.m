//
//  AITCameraCommand.m
//  WiFiCameraViewer
//
//  Created by Clyde on 2013/11/8.
//  Copyright (c) 2013年 a-i-t. All rights reserved.
//

#import "AITCameraCommand.h"
#import "AITUtil.h"
#import "Toast+UIView.h"

@interface AITCameraCommand()
{
    AITCameraRequest *request ;
    UIView *view ;
}
@end


@implementation AITCameraCommand

static NSString *CGI_PATH = @"/cgi-bin/Config.cgi" ;
static NSString *ACTION_SET = @"set" ;
static NSString *ACTION_GET = @"get" ;
static NSString *ACTION_DEL = @"del" ;
static NSString *ACTION_LS = @"dir" ;

static NSString *ACTION_reardir = @"reardir" ;
static NSString *PROPERTY_Event = @"Event" ;
static NSString *PROPERTY_Photo = @"Photo" ;
/*
 action=dir前路
 action=reardir后路
 property=DCIM录像
 property=Event加锁
 property=Photo
 http://192.72.1.1/cgi-bin/Config.cgi?action=dir&property=Photo&format=all&count=16&from=0
 http://192.72.1.1/cgi-bin/Config.cgi?action=reardir&property=Photo&format=all&count=16&from=0
 */

static NSString *PROPERTY = @"property";
static NSString *PROPERTY_NET = @"Net" ;
static NSString *PROPERTY_SSID = @"Net.WIFI_AP.SSID" ;
static NSString *PROPERTY_ENCRYPTION_KEY = @"Net.WIFI_AP.CryptoKey" ;
static NSString *PROPERTY_DCIM = @"DCIM" ;
static NSString *PROPERTY_VIDEO = @"Video" ;
static NSString *PROPERTY_MUTE = @"SoundIndicator";
static NSString *OldQueryPROPERTY_MUTE = @"Camera.Preview.MJPEG.status.customer";
static NSString *OLDPROPERTY_MUTE = @"Video";
static NSString *PROPERTY_QUERY_RECORD = @"Camera.Preview.MJPEG.status.record" ;
static NSString *PROPERTY_QUERY_STATUS = @"Camera.Preview.MJPEG.status" ;
static NSString *PROPERTY_TIMESTAMP = @"Camera.Preview.MJPEG.TimeStamp" ;
static NSString *PROPERTY_QUERY_SDLIFE= @"Camera.Menu.SDCardStatus";

static NSString *PROPERTY_DEFAULTVALUE = @"Camera.Menu.*" ;//DefaultValue
static NSString *PROPERTY_VIDEOCLIP_TIME = @"VideoClipTime" ;
static NSString *PROPERTY_VIDEORES = @"Videores" ;
static NSString *PROPERTY_IMAGERES = @"Imageres" ;
static NSString *PROPERTY_EV = @"EV" ;
static NSString *PROPERTY_MTD = @"MTD" ;
static NSString *PROPERTY_FLICKER = @"Flicker";
static NSString *PROPERTY_AWB = @"AWB";
static NSString *PROPERTY_TIMESETTING = @"TimeSettings";
static NSString *PROPERTY_FORMATSDCARD = @"SD0";//Camera.Cruise.Seq2.Count
static NSString *PROPERTY_ResetSetting = @"FactoryReset";
static NSString *PROPERTY_PARKING_MONITORING = @"Camera.Menu.ParkGsensorLevel";
static NSString *PROPERTY_TVOUT = @"Camera.Cruise.ActiveSeq";
static NSString *PROPERTY_TVOUT_TVSystem = @"TVSystem";
static NSString *PROPERTY_LDWS = @"Camera.Cruise.AutoPan.Speed.Pan";

static NSString *VALUE = @"value" ;
static NSString *COMMAND_FIND_CAMERA = @"findme" ;
static NSString *COMMAND_RESET = @"reset" ;
static NSString *COMMAND_CAPTURE = @"capture" ;
static NSString *COMMAND_RECORD  = @"record";
static NSString *COMMAND_RECORD_START  = @"record_start";
static NSString *COMMAND_RECORD_STOP   = @"record_stop";
static NSString *COMMAND_FORMATSDCARD = @"format";

/*
 format=jpeg      申请图片
 format=Fnormal   申请前路普通录像
 format=Rnormal   申请后路普通录像
 format=Fsos      申请前路加锁录像
 format=Rsos      申请后路加锁录像
 */
static NSString *FORMAT = @"format" ;
static NSString *FORMAT_AVI = @"avi" ;
static NSString *FORMAT_JPEG = @"jpeg" ;
static NSString *FORMAT_ALL = @"all" ;
static NSString *FORMAT_Front = @"Fnormal";
static NSString *FORMAT_Rear = @"Rnormal";
static NSString *FORMAT_Fsos = @"Fsos";
static NSString *FORMAT_Rsos = @"Rsos";

static NSString *COUNT = @"count" ;
//static int COUNT_MAX = 32 ;
//static int COUNT_MIN = 1 ;

static NSString *FROM = @"from" ;

static NSString *PROPERTY_RAND = @"Camera.Cruise.Seq1.Count" ;

//Add Ted
static NSString *PROPERTY_CAMERA_RTSP = @"Camera.Preview.RTSP.av";
static NSString *PROPERTY_QUERY_PREVIEW_STATUS = @"Camera.Preview.RTSP.av";
static NSString *ACTION_SETCAMID = @"setcamid";
static NSString *PROPERTY_SWITCH_PREVIEW = @"Camera.Preview.Source.1.Camid" ;
static NSString *COMMAND_REAR_PREVIEW = @"rear" ;
static NSString *COMMAND_FRONT_PREVIEW = @"front";

static NSString *COMMAND_MemoryCard = @"Camera.Preview.MJPEG.WarningMSG";
static NSString *COMMAND_QueryRearVideo = @"Camera.Menu.IsStreaming";

static NSString *COMMAND_QueryTEXT = @"Camera.CustCmd.TPMS.SysConfig";
static NSString *PROPERTY_Menu_FCWS = @"Camera.Menu.FCWS";
static NSString *PROPERTY_Menu_MTD =  @"MTD";
static NSString *PROPERTY_Menu_LDWS = @"Camera.Menu.LDWS";
static NSString *PROPERTY_Menu_GsensorLock= @"Camera.Menu.GsensorLockLevel";

+ (NSString *) PROPERTY_SSID
{
    return PROPERTY_SSID ;
}

+ (NSString *) PROPERTY_ENCRYPTION_KEY
{
    return PROPERTY_ENCRYPTION_KEY ;
}

+ (NSString *) buildKeyValuePair:(NSString*) key Value:(NSString *) value
{
    if(value != nil)
        return [NSString stringWithFormat:@"%@=%@", key, value] ;
    else
        return [NSString stringWithFormat:@"%@", key] ;
}

+ (NSString *) buildProperty:(NSString *) property Value: (NSString *) value
{
    return [NSString stringWithFormat:@"%@&%@", [AITCameraCommand buildKeyValuePair:PROPERTY Value:property], [AITCameraCommand buildKeyValuePair:VALUE Value:value]] ;
}

+ (NSString *) buildProperty:(NSString *) property
{
    return [NSString stringWithFormat:@"%@", [AITCameraCommand buildKeyValuePair:PROPERTY Value:property]] ;
}

+ (NSString *) buildArgumentList: (NSArray *) arguments
{
    NSString *argumentList = @"" ;
    
    for (NSString *argument in arguments)
    {
    
        if (argument)
        {
        
            argumentList = [argumentList stringByAppendingFormat: @"&%@", argument] ;
        }
    }
    return argumentList ;
}

+ (NSURL*) buildRequestUrl: (NSString *) path Action: (NSString *) action ArgumentList: (NSString *) argumentList
{
    NSString *cameraIp = [AITUtil getCameraAddress] ;

    NSString *url = [NSString stringWithFormat:@"http://%@%@?action=%@%@", cameraIp, path, action, argumentList] ;
    
    return [[NSURL alloc] initWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] ;
}

+ (NSURL*) commandUpdateUrl: (NSString *) ssid EncryptionKey: (NSString *) encryptionKey
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_SSID Value: ssid]] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_ENCRYPTION_KEY Value: encryptionKey]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandGetMenuSettingsValuesUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DEFAULTVALUE]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetVideoClipTimeUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_VIDEOCLIP_TIME Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetVideoResUrl: (NSString *) resolution
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_VIDEORES Value: resolution]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetImageResUrl: (NSString *) resolution
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_IMAGERES Value: resolution]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetMotionDetUrl: (NSString *) enable
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_MTD Value: enable]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetFlickerUrl: (NSString *) freq
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_FLICKER Value: freq]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetAWBUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_AWB Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetEVUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_EV Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandCameraTimeSettingsUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    //Get current date
    NSDate *date = [NSDate date];
    
    // format it
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"yyyy$MM$dd$HH$mm$ss"];
    
    // convert it to a string
    NSString *dateString = [dateFormat stringFromDate:date];
    
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_TIMESETTING Value: dateString]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}



+ (NSURL*) commandGetParkingMonitoringUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_PARKING_MONITORING]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetParkingMonitoringUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_PARKING_MONITORING Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandGetTVOutUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_TVOUT]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetTVOutUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_TVOUT_TVSystem Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetTVOutNTSCUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_TVOUT_TVSystem Value: value]] ;//PROPERTY_TVOUT_TVSystem
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetTVOutPALUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_TVOUT_TVSystem Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandGetLDWSUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;

    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_LDWS]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetLDWSUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_LDWS Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}


+ (NSURL*) commandCameraFormatSDCardUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_FORMATSDCARD Value: COMMAND_FORMATSDCARD]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandFindCameraUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_NET Value: COMMAND_FIND_CAMERA]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandCameraSnapshotUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_VIDEO Value: COMMAND_CAPTURE]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandCameraRecordUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_VIDEO Value: COMMAND_RECORD]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//开启录像
+ (NSURL*) commandCameraStartRecordUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_VIDEO Value: COMMAND_RECORD_START]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

//停止录像
+ (NSURL*) commandCameraStopRecordUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_VIDEO Value: COMMAND_RECORD_STOP]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandQueryCameraStatusUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_QUERY_STATUS]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandQueryCameraRecordUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_QUERY_RECORD]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandQueryCameraMuteUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:OldQueryPROPERTY_MUTE]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandCameraMuteUrl: (NSString *) enable
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_MUTE Value: enable]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandTimeStampUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_TIMESTAMP]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandReactivateUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_NET Value: COMMAND_RESET]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandWifiInfoUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_SSID]] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_ENCRYPTION_KEY]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandListFileUrl: (NSInteger) count
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DCIM]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
//    count = count > COUNT_MAX ? COUNT_MAX : count ;
//    count = count < COUNT_MIN ? COUNT_MIN : count ;
    
    int num = 16;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%d", num]]] ;
//    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:[NSString stringWithFormat:@"%ld", (long)count]]];
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandListFirstFileUrl: (NSInteger) count
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DCIM]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
//    count = count > COUNT_MAX ? COUNT_MAX : count ;
//    count = count < COUNT_MIN ? COUNT_MIN : count ;
    
    count = 20;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandDelFileUrl: (NSString *) fileName
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    //[arguments addObject: [AITCameraCommand buildProperty:[NSString stringWithFormat:@"%@%@", PROPERTY_DCIM_DEL, fileName]]] ;
    [arguments addObject: [AITCameraCommand buildProperty:[NSString stringWithFormat:@"%@", fileName]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_DEL ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetRandUrl: (u_int32_t) rand
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_RAND Value: [NSString stringWithFormat:@"%u", rand]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandGetRandUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_RAND]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

- (id) initWithUrl: (NSURL*) url View: (UIView *)aView
{
    
    self = [super init] ;
    
    if (self) {
        view = aView ;
        
        request = [[AITCameraRequest alloc] initWithUrl:url Delegate:self] ;
    }
    return self ;
}

-(void) requestFinished:(NSString*) result
{

    if (result) {
        //[view makeToast:NSLocalizedString(@"Command Succeeded", nil)];
    } else {
        [view makeToast:NSLocalizedString(@"Cannot connect to the camera!", nil) duration:0.5 position:nil];
    }

}

- (id) initWithUrl: (NSURL*) url Delegate:(id<AITCameraRequestDelegate>)delegate
{
    self = [super init] ;
    
    if (self) {
        request = [[AITCameraRequest alloc] initWithUrl:url Delegate:delegate] ;
    }
    return self ;
}

+ (NSDictionary*) buildResultDictionary:(NSString*)result
{
    NSMutableArray *keyArray;
    NSMutableArray *valArray;
    NSArray *lines;
    
    keyArray = [[NSMutableArray alloc] init];
    valArray = [[NSMutableArray alloc] init];
    lines = [result componentsSeparatedByString:@"\n"];
    for (NSString *line in lines) {
        NSArray *state = [line componentsSeparatedByString:@"="];
        if ([state count] != 2)
            continue;
        [keyArray addObject:[[state objectAtIndex:0] copy]];
        [valArray addObject:[[state objectAtIndex:1] copy]];
    }
    if ([keyArray count] == 0)
        return nil;
    return [NSDictionary dictionaryWithObjects:valArray forKeys:keyArray];
}

+ (NSString *) PROPERTY_CAMERA_RTSP
{
    return PROPERTY_CAMERA_RTSP;
}

#pragma mark -  3.0版本新功能代码 add Ted
//适配视频预览格式
+ (NSURL*) commandQueryPreviewStatusUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_QUERY_PREVIEW_STATUS]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

//视频预览切换前后镜头
+ (NSURL*) commandRearPreviewUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_SWITCH_PREVIEW Value: COMMAND_REAR_PREVIEW]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SETCAMID ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandFrontPreviewUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_SWITCH_PREVIEW Value: COMMAND_FRONT_PREVIEW]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SETCAMID ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}


//获取SD文档接口
/*
 
 action   区分前后路
 property 区分视频照片
 format   不变 (等于@"all")
 count    请求多少数据
 from     已请求到数据个数
 
 action=dir     前路
 action=reardir 后路
 property=DCIM  录像
 property=Event 加锁
 property=Photo 图片
 
 */

//第一次获取文件
//前路视频
+ (NSURL*) commandFirstSDFrontFileUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DCIM]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int count = 16;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//后路视频
+ (NSURL*) commandFirstSDRearFileUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DCIM]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int count = 16;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_reardir ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//图片前路
+ (NSURL*) commandFirstSDImageFileUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Photo]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int count = 16;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//图片后路
+ (NSURL*) commandFirstSDRearImageFileUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Photo]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int count = 16;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_reardir ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//SOS前路
+ (NSURL*) commandFirstSDSOSFrontFileUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Event]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int count = 16;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//SOS后路
+ (NSURL*) commandFirstSDSOSRearFileUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Event]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int count = 16;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_reardir ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

//上拉刷新
//前路视频
+ (NSURL*) commandSDFrontFileUrl: (NSInteger) count{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DCIM]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int num = 16;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%d", num]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;

}
//后路视频
+ (NSURL*) commandSDRearFileUrl: (NSInteger) count{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DCIM]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int num = 16;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%d", num]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_reardir ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//SOS前路
+ (NSURL*) commandSDSOSFrontFileUrl: (NSInteger) count{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Event]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int num = 16;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%d", num]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;

}
//SOS后路
+ (NSURL*) commandSDSOSRearFileUrl: (NSInteger) count{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Event]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int num = 16;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%d", num]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_reardir ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//前路图片
+ (NSURL*) commandSDImageFileUrl: (NSInteger) count{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Photo]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int num = 16;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%d", num]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}
//后路图片
+ (NSURL*) commandSDRearImageFileUrl: (NSInteger) count{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Photo]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:FORMAT_ALL]] ;
    
    int num = 16;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%d", num]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_reardir ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}


//查询SD卡寿命
+ (NSURL*) commandQueryCameraSDFileUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_QUERY_SDLIFE]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

//查询卡异常
+ (NSURL*) commandQueryCameraMemoryCardUrl{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:COMMAND_MemoryCard]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

//恢复出厂设置

+ (NSURL*) commandCameraResetSettingUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_ResetSetting Value: @"Camera"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

//老版本设置录音设置
+ (NSURL*) commandOldCameraMuteUrl: (NSString *) enable
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:OLDPROPERTY_MUTE Value: enable]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandOldCameraMuteUrl
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:OldQueryPROPERTY_MUTE]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandQueryRearVideoUrl
{//QUERY_REARVIDEO
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:COMMAND_QueryRearVideo]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_GET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetMenuFCWSUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Menu_FCWS Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetMenuLDWSUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Menu_LDWS Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandSetMenuMTDSUrl: (NSString *) value
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Menu_MTD Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*)commandSetMenuGsensorLockSUrl: (NSString *) value{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_Menu_GsensorLock Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

//视频预览切换前后镜头

+ (NSURL*) commandSetCameraidUrl: (NSString *) camid
{
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_SWITCH_PREVIEW Value:camid]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SETCAMID ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}

+ (NSURL*) commandCameraTextUrl
{
        
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    
    [arguments addObject: [AITCameraCommand buildProperty:PROPERTY_DCIM]] ;
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FORMAT Value:@"all"]] ;
    
    int count = 16;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:COUNT Value:[NSString stringWithFormat:@"%ld", (long)count]]] ;
    
    [arguments addObject: [AITCameraCommand buildKeyValuePair:FROM Value:@"0"]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_LS ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
    
}

#pragma mark -  获取XML
+ (NSURL*) commandGetCamMenu
{
    NSString *cameraIp = [AITUtil getCameraAddress] ;
    NSString *cammenu  = @"/cdv/cammenu.xml";
    
    NSString *url = [NSString stringWithFormat:@"http://%@%@", cameraIp, cammenu] ;
    return [[NSURL alloc] initWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}

+(NSURL*)commandSettingsViewUrl:(NSString *)property value:(NSString *) value{
    
    NSMutableArray * arguments = [[NSMutableArray alloc] init] ;
    [arguments addObject: [AITCameraCommand buildProperty:property Value: value]] ;
    
    return [AITCameraCommand buildRequestUrl:CGI_PATH Action: ACTION_SET ArgumentList: [AITCameraCommand buildArgumentList:arguments]] ;
}



@end
